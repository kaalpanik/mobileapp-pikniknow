import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:ui';


class Loader {
  OverlayState overlayState = null;
  OverlayEntry overlayEntry = null;
  BuildContext context = null;

  static final Loader _singleton = new Loader._private();

  static Loader getInstance() {
    return _singleton;
  }

  Loader._private();

  showOverlay(BuildContext context) async {
    if (overlayState == null) {
      this.context = context;
      overlayState = Overlay.of(context);
      overlayEntry = OverlayEntry(
          builder: (context) => GestureDetector(
                onTap: () {},
                child: Container(
                  height: 100,
                  width: 100,
                  color: Colors.transparent,
                  child: Center(child: new ClipRRect(
                    borderRadius: BorderRadius.circular(20),
              child: new BackdropFilter(
                filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                child: new Container(
                  width: 80,
                  height: 80,
                  decoration: new BoxDecoration(
                    color: Colors.black.withOpacity(0.6),
                    borderRadius: BorderRadius.circular(20)
                  ),
                  child: new Stack(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: new Container(
                        height: 60,
                        width: 60,
                        child:CupertinoTheme( data: CupertinoTheme.of(context).copyWith(brightness: Brightness.dark), child: CupertinoActivityIndicator(radius: 17,), ),
                      ),
                      ),
                    ],
                  ),
                ),
              )
            )),
                ),
                
              ));
      overlayState.insert(overlayEntry);
    }
  }

  dismissOverlay() {
    if (overlayEntry != null) {
      overlayEntry.remove();
      overlayState = null;
      overlayEntry = null;
    }
  }

  
}
