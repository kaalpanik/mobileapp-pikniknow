import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:ui';
import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';


String formattedDate(String from, String to, String date){
    var fromFormat = DateFormat(from);
    var toFormat = DateFormat(to);
    var outputDate = toFormat.format(fromFormat.parse(date));
    return outputDate;
  }

int getDateDifference(String givenDate) {
  final birthday = DateFormat("yyyy-MM-dd").parse(givenDate);
  final date2 = DateTime.now();
  final difference = ((date2.difference(birthday).inDays) / 365).floor();
  log(difference);
  return difference;
}

String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
}




  Widget borderContainer(BuildContext context,String text){
    return ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.blueGrey,
                  width: 0.8
                ),
                borderRadius: BorderRadius.circular(5)
              ),
              child: Center(
                child: Row(
                  children: <Widget>[
                    Expanded(child: Padding(
                      padding: EdgeInsets.fromLTRB(10,5,10,5),
                      child: new Text(text, textAlign: TextAlign.left, style: TextStyle(fontFamily: "Poppins",fontSize: 18,color: Color(0XFF2C4C65),fontWeight: FontWeight.w500),),
                    )),
                  ],
                ),
              )),
            );
  }

  class InputDoneView extends StatefulWidget{
    @override
  State<StatefulWidget> createState() {
    
    return InputDoneViewState();
  }
  }



  class InputDoneViewState extends State<InputDoneView>{
    bool hide = false;
    @override
  void initState() {
    
    super.initState();
    hide = false;
  }
    @override
  Widget build(BuildContext context) {
    
    return Container(
      width: double.infinity,
      child: _content(context),
    );
  }

    Widget _content(BuildContext context){
      if(hide){
        return new SizedBox(height: 0,);
      }
      else{
        return Container(

        height: 40,
        width: double.infinity,
        color: Colors.transparent,
        child: Container(
          child: ClipRRect(
            borderRadius: BorderRadius.circular(1),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10.0,sigmaY: 10.0),
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                  color: Colors.grey.shade500.withOpacity(0.2),
                ),
                child: Align(
                  alignment: Alignment.topRight,
                  child: new Padding(
                    padding: const EdgeInsets.only(top: 4.0,bottom: 4.0),
                    child: CupertinoButton(
                      padding: EdgeInsets.only(right: 24.0,top: 8.0,bottom: 8.0),
                      onPressed: (){
                        setState(() {
                          hide = true;
                        });
                        FocusScope.of(context).requestFocus(new FocusNode());
                      },
                      child: Text("Done",
                        style: TextStyle(color: CupertinoColors.activeBlue,fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
      }
    }
  }
  OverlayEntry overlaykeyboardEntry;

  showKeyboardOverlay(BuildContext context){
      
      if(overlaykeyboardEntry != null) return;
      OverlayState overlayState = Overlay.of(context);
      overlaykeyboardEntry = OverlayEntry(builder: (context){
        return Positioned(
          bottom: MediaQuery.of(context).viewInsets.bottom,
          right: 0,
          left: 0,
          child: InputDoneView(),
        );
      });
      overlayState.insert(overlaykeyboardEntry);
    }

  removerKeyvoardOverlay() {
    if(overlaykeyboardEntry != null){
      overlaykeyboardEntry.remove();
      overlaykeyboardEntry = null;
    }
  }

Future<void> launchURL(String url) async {
  var urltoLaunch = url;
  if (await canLaunch(urltoLaunch)) {
    await launch(urltoLaunch);
  } else {
    throw 'Could not launch $url';
  }
}


class RadioTile extends StatefulWidget {
  final value;
  final groupValue;
  final title;
  final color;
  final Function() onPressed;

  const RadioTile({Key key, this.color,this.title, this.onPressed,this.value,this.groupValue})
      : super(key: key);

  @override
  _RadioTileState createState() => _RadioTileState();
}

class _RadioTileState extends State<RadioTile> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: widget.onPressed,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            AbsorbPointer(
              child: new Radio(
                onChanged: (val){
                },
                activeColor: widget.color,
                groupValue: widget.groupValue,
                value: widget.value,
              ),
            ),
            new SizedBox(width: 0,),
            new Text(widget.title,style:TextStyle(fontSize: 18))
          ],
        ),
      );
  }
}

class CheckBoxTile extends StatefulWidget {
  final value;
  final title;
  final color;
  final Function() onPressed;

  const CheckBoxTile({Key key, this.color,this.title, this.onPressed,this.value})
      : super(key: key);

  @override
  _CheckBoxTileState createState() => _CheckBoxTileState();
}

class _CheckBoxTileState extends State<CheckBoxTile> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: widget.onPressed,
        child: Row(
          children: <Widget>[
            AbsorbPointer(
              child: new Checkbox(
                onChanged: (val){
                },
                activeColor: widget.color,
                value: widget.value,
              ),
            ),
            new SizedBox(width: 0,),
            new Text(widget.title,style:TextStyle(fontSize: 18))
          ],
        ),
      );
  }
}


Future<String> getFromUserDefault(String key) async{
  var value = "";
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if(prefs.getString(key) != null){
    value = prefs.getString(key);
  }
  return value;
}

setIntoDefault(String key, String value) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString(key, value);
}

removeFromDefault(String key)async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.remove(key);
}


class NumberTile extends StatefulWidget {
  final value;
  double size;
  Color color = Colors.white;
  final Function() onPressed;

   NumberTile({Key key, this.onPressed,this.value,this.size,this.color})
      : super(key: key);

  @override
  _NumberTileState createState() => _NumberTileState();
}

class _NumberTileState extends State<NumberTile> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: widget.onPressed,
        child: new Container(
          padding: EdgeInsets.all(10),
          child: Center(child: Text(widget.value,textScaleFactor: 1,style: TextStyle(color: widget.color,fontSize: widget.size,fontWeight: FontWeight.bold),)),
        ),
      );
  }
}

class SpritePainter extends CustomPainter {
  final Animation<double> _animation;

  SpritePainter(this._animation) : super(repaint: _animation);

  void circle(Canvas canvas, Rect rect, double value) {
    double opacity = (1.0 - (value / 4.0)).clamp(0.0, 1.0);
    Color color = new Color.fromRGBO(0, 117, 194, opacity);

    double size = rect.width / 2;
    double area = size * size;
    double radius = sqrt(area * value / 4);

    final Paint paint = new Paint()..color = color;
    canvas.drawCircle(rect.center, radius, paint);
  }

  @override
  void paint(Canvas canvas, Size size) {
    Rect rect = new Rect.fromLTRB(0.0, 0.0, size.width, size.height);

    for (int wave = 3; wave >= 0; wave--) {
      circle(canvas, rect, wave + _animation.value);
    }
  }

  @override
  bool shouldRepaint(SpritePainter oldDelegate) {
    return true;
  }
}

class RequestPainter extends CustomPainter {
  final Animation<double> _animation;

  RequestPainter(this._animation) : super(repaint: _animation);

  void circle(Canvas canvas, Rect rect, double value) {
    double opacity = (1.0 - (value / 4.0)).clamp(0.0, 1.0);
    Color color = Colors.white.withOpacity(opacity);

    double size = rect.width / 2;
    double area = size * size;
    double radius = sqrt(area * value / 4);

    final Paint paint = new Paint()..color = color;
    canvas.drawCircle(rect.center, radius, paint);
  }

  @override
  void paint(Canvas canvas, Size size) {
    Rect rect = new Rect.fromLTRB(0.0, 0.0, size.width, size.height);

    for (int wave = 3; wave >= 0; wave--) {
      circle(canvas, rect, wave + _animation.value);
    }
  }

  @override
  bool shouldRepaint(SpritePainter oldDelegate) {
    return true;
  }
}



  



  




