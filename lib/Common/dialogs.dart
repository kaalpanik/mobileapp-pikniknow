import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:rflutter_alert/rflutter_alert.dart';


class Dialogs {

  static final Dialogs _singleton = new Dialogs._private();

  static Dialogs getInstance() {

    return _singleton;
  }

  Dialogs._private();
  


  showErrorAlert(BuildContext context,String title, String message, bool isError){
    Alert(
      context: context,
      type: isError?AlertType.error:AlertType.success,
      title: title,
      desc: message,
      style: AlertStyle(
        isOverlayTapDismiss: false,
      ),
      closeFunction: (){},
      closeIcon: Icon(Icons.close,size:0,color:Colors.transparent),
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: AppDefault_Green,
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();
  }

    showInfoAlert(BuildContext context,String title, String message, bool isError){
    Alert(
      context: context,
      type: isError?AlertType.error:AlertType.info,
      title: title,
      desc: message,
      style: AlertStyle(
        isOverlayTapDismiss: false,
      ),
      closeFunction: (){},
      closeIcon: Icon(Icons.close,size:0,color:Colors.transparent),
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: AppDefault_Green,
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();
    }
    

  


   Widget NoDataFound(BuildContext context, String title){

    return new Center(
      child: new Container(
        color: Colors.transparent,
        height: MediaQuery.of(context).size.width -140,
        width: MediaQuery.of(context).size.width -140,
        child: Column(mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
           new Container(
                        color: Colors.transparent,
                        child: Image.asset("assets/NoData.png",fit: BoxFit.fitHeight,)
                      ),
            new SizedBox(height: 10,),
            new Text(title, style:GoogleFonts.montserrat(textStyle:TextStyle(fontSize: 17,color:Colors.grey)))
          ],
        ),
      ),
    );
  }


}