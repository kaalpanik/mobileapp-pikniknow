import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationHandler{
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin=new FlutterLocalNotificationsPlugin();
  var androidPlatformChannelSpecifics;
  var platformChannelSpecifics;

  Future selectNotification(String payload) async {
    if (payload != null) {
      //debugPrint('notification payload: ' + payload);
    }
//    await Navigator.push(
//      context,
//      MaterialPageRoute(builder: (context) => SecondScreen(payload)),
//    );
  }
  
  Future<void> showNotify11(Map<String, dynamic> msg) async {
    
    var notificationAppLaunchDetails =
    flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

    var initializationSettingsAndroid = AndroidInitializationSettings('main_logo');
    var initializationSettingsIOS = IOSInitializationSettings(
        requestSoundPermission: false,
        requestBadgePermission: false,
        requestAlertPermission: false,
    );
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    var vibrationPattern = new Int64List(4);
    vibrationPattern[0] = 0;
    vibrationPattern[1] = 1000;
    vibrationPattern[2] = 5000;
    vibrationPattern[3] = 2000;

    log("none "+msg.toString());

    androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        "defaultId",
        'appname',
        'no discription',
        icon: 'main_logo',
        channelShowBadge: true,
        playSound: true,
        enableLights: true,
        enableVibration: true,
        visibility: NotificationVisibility.Public,
        ticker: "ticker",
        importance: Importance.Max, priority: Priority.High,
        vibrationPattern: vibrationPattern,
        color:  Colors.white);

    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      showNotify(msg);
  }

  int currentTimeInSeconds() {
    var ms = (new DateTime.now()).millisecondsSinceEpoch;
    return (ms / 1000).round();
  }
  Future<void> showNotify(Map<String, dynamic> msg) async {
    int value=currentTimeInSeconds();
    flutterLocalNotificationsPlugin.show(
        value, msg['notification']["title"],msg['notification']["body"], platformChannelSpecifics,
        payload: 'item x');
  }
}