import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:rflutter_alert/rflutter_alert.dart';


class Alerts{
  showAlertDialog(BuildContext context, String title, String message, bool isError){
    Alert(
      context: context,
      type: isError?AlertType.error:AlertType.success,
      title: title,
      desc: message,
      style: AlertStyle(
        isOverlayTapDismiss: false,
      ),
      closeFunction: (){},
      closeIcon: Icon(Icons.close,size:0,color:Colors.transparent),
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: AppDefault_Green,
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();
    // showDialog(
    //   context: context,
    //   builder: (alertcontext){
    //     if(Platform.isIOS){
    //                         return CupertinoAlertDialog(
                              
    //                         content: new Text(message,textScaleFactor: 1, style: GoogleFonts.roboto(),),
    //                         actions: <Widget>[
    //                           new CupertinoButton(
    //                             child: new Text("OK",textScaleFactor: 1, style: GoogleFonts.roboto(),),
    //                             onPressed: (){
    //                                Navigator.of(context).pop();
    //                             },
    //                           )
    //                         ],
    //                       );
    //                      }
    //                      else{
    //                        return AlertDialog(
    //                          content: new Text(message,textScaleFactor: 1, style: GoogleFonts.roboto(),),
    //                          actions: [
    //                            new RaisedButton(
    //                             child: new Text("OK",textScaleFactor: 1, style: GoogleFonts.roboto(color: Colors.white),),
    //                             onPressed: (){
    //                                Navigator.of(context).pop();
    //                             },
    //                             color: AppDefault_Green,
    //                           )
    //                          ],
    //                        );
    //                      }
        
    //   }
    // );
  }

}