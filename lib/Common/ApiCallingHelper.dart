import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:ui';
import 'package:connectivity/connectivity.dart';
import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:newapp/Common/Alerts.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/controllers/auth/LoginScreen.dart';
import 'dart:async';
import 'ProgressBar.dart';
import 'dialogs.dart';





class ApiCallingHelper {
  static bool isLogged = false;
  static String baseUrl = "http://travelappapi-dev.ap-south-1.elasticbeanstalk.com/api/";

  
  Future<String> callPostApiJson(BuildContext context,
    String webLink, Object params) async {
      
    if (isLogged) log(json.encode(params));
    Loader.getInstance().showOverlay(context);
    check().then((intenet) {
      if (intenet != null && intenet) {
        // Internet Present Case
      }
      else{
        Future.delayed(const Duration(milliseconds: 100), (){
          Loader.getInstance().dismissOverlay();
          Dialogs.getInstance().showErrorAlert(context, "","Please check your internet connection or try again later",true);
        });
        
      }
      
    });
    var resp = await http.post(webLink, body: params);
    // log(webLink);
    // log(params);
    if (isLogged) log(resp.body);

    Loader.getInstance().dismissOverlay();

    return resp.body;
  }


  Future<String> callPostApiJsonWithoutLoader(BuildContext context,
    String webLink, Object params) async {
      
    if (isLogged) log(json.encode(params));
    check().then((intenet) {
      if (intenet != null && intenet) {
        // Internet Present Case
      }
      else{
        Future.delayed(const Duration(milliseconds: 100), (){
          Loader.getInstance().dismissOverlay();
          Dialogs.getInstance().showErrorAlert(context, "","Please check your internet connection or try again later",true);
        });
        
      }
      
    });
    var resp = await http.post(webLink, body: params);
    // log(webLink);
    // log(params);
    if (isLogged) log(resp.body);


    return resp.body;
  }

  Future<String> callPostApi(BuildContext context,
    String webLink, Object params) async {
      
    if (isLogged) log(json.encode(params));
    Loader.getInstance().showOverlay(context);
    check().then((intenet) {
      if (intenet != null && intenet) {
        // Internet Present Case
      }
      else{
        Future.delayed(const Duration(milliseconds: 100), (){
          Loader.getInstance().dismissOverlay();
          Dialogs.getInstance().showErrorAlert(context, "","Please check your internet connection or try again later",true);
        });
        
      }
      
    });
    var resp = await http.post(webLink, body: params);
    // log(webLink);
    // log(params);
    if (isLogged) log(resp.body);

    Loader.getInstance().dismissOverlay();

    return resp.body;
  }

  Future<String> callPostApiWithoutProgress(BuildContext context,
    String webLink, Object params) async {
      
    if (isLogged) log(json.encode(params));
    check().then((intenet) {
      if (intenet != null && intenet) {
        // Internet Present Case
      }
      else{
        Future.delayed(const Duration(milliseconds: 100), (){
          Loader.getInstance().dismissOverlay();
          Dialogs.getInstance().showErrorAlert(context, "","Please check your internet connection or try again later",true);
        });
        
      }
      
    });
    var resp = await http.post(webLink, body: params);
    // log(webLink);
    // log(params);
    if (isLogged) log(resp.body);


    return resp.body;
  }

  Future<String> callPostApiHeader(BuildContext context,
      String webLink, Object params) async {

        Loader.getInstance().showOverlay(context);

    if (isLogged) log(json.encode(params));

    Map<String,String> headers = {
      'Content-type' : 'application/json', 
      'Accept': 'application/json',
    };

    var resp = await http.post(webLink, body: params, headers: headers);
    if (isLogged) log(resp.body);

    
    Loader.getInstance().dismissOverlay();


    return resp.body;
  }


  Future<String> apiRequest(String url, Map jsonMap) async {


    bool trustSelfSigned = true;
    HttpClient httpClient = new HttpClient()..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);;
       

    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(jsonMap)));
    log(json.encode(jsonMap));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    log(reply);
    return reply;
  }

  Future<String> callGetApi(BuildContext context,String webLink) async {

    Loader.getInstance().showOverlay(context);
    check().then((intenet) {
      if (intenet != null && intenet) {
        // Internet Present Case
      }
      else{
        Future.delayed(const Duration(milliseconds: 100), (){
          Loader.getInstance().dismissOverlay();
        });
        
      }
      
    });

    var resp = await http.get(webLink);
    // log(webLink);
    if (isLogged) log(resp.body);

    Loader.getInstance().dismissOverlay();
    return resp.body;
  }

  Future<String> callGetApiAuthorized(BuildContext context,String webLink,String Token) async {

    Loader.getInstance().showOverlay(context);
    check().then((intenet) {
      if (intenet != null && intenet) {
        // Internet Present Case
      }
      else{
        Future.delayed(const Duration(milliseconds: 100), (){
          Loader.getInstance().dismissOverlay();
        });
        
      }
      
    });

    var resp = await http.get(webLink,headers: {"Authorization": Token});
    // log(webLink);
    if (isLogged) log(resp.body);

    Loader.getInstance().dismissOverlay();
    return resp.body;
  }

  //FormData

  Future<String> callApiWithImage(String webLink, FormData params) async {

    Dio dio = new Dio();

    var resp = await dio.post(webLink, data: params);
    // log(webLink);
    // log(params.toString());
    if (isLogged) log(resp.data);

    return jsonEncode(resp.data);
  }

  String generateMd5(String input) {
    return md5.convert(utf8.encode(input)).toString();
  }

  Future<bool> check() async {
        var connectivityResult = await (Connectivity().checkConnectivity());
        if (connectivityResult == ConnectivityResult.mobile) {
          return true;
        } else if (connectivityResult == ConnectivityResult.wifi) {
          return true;
        }
        return false;
      }


  

}


class ApiManager{
  static bool showLog = false;

  static String BaseURL = "http://travelappapi-dev.ap-south-1.elasticbeanstalk.com/api/";
  // Live


  //Get API

  Future<dynamic> callGetApi(BuildContext context, String webLink, bool showLoading, String token) async{
    if(showLoading){
      showLoader(context, true);
    }
    
    Dio dio = Dio();
    try {
      var response = await dio.get(webLink,options: Options(headers: {"Authorization": token}));
      var data = response.data;
      if(showLoading){
        showLoader(context, false);
      }
      
      if(showLog){
        log("\n\n**************************URL**************************\n\n$webLink\n\n");
        log("\n\n**************************RESPONSE**************************\n\n$data\n\n");
      }
      return data;
    } on DioError catch(error){
      if(showLoading){
        showLoader(context, false);
      }
      log(error.toString());
      if(DioErrorType.RECEIVE_TIMEOUT == error.type || DioErrorType.CONNECT_TIMEOUT == error.type){
        log("Server is not reachable. Please verify your internet connection and try again");
        Alerts().showAlertDialog(context, "Server Unreachable", "Server is not reachable. Please verify your internet connection and try again",true);
      }
      else if(DioErrorType.RESPONSE == error.type){
        if (error.response.statusCode == 404) {
           log("Url is not found or Page is missing. Please check Url.");
         }
        else if (error.response.statusCode == 410) {
           log("Token Expired");

           showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (BuildContext context){
                          if(Platform.isIOS){
                            return CupertinoAlertDialog(
                              
                            content: new Text("Your Session is expired. Try Login Again",textScaleFactor: 1, style: GoogleFonts.roboto(),),
                            actions: <Widget>[
                              new CupertinoButton(
                                child: new Text("OK",textScaleFactor: 1, style: GoogleFonts.roboto(),),
                                onPressed: ()async{
                                   await GoogleSignIn().signOut();
                                    setIntoDefault(UserDefault_IsLogin, "false");
                                    setIntoDefault(UserDefault_Profile, "");
                                    setIntoDefault(UserDefault_Location, "");
                                   Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>LoginScreen()), (route) => false);
                                },
                              )
                            ],
                          );
                         }
                         else{
                           return AlertDialog(
                             content: new Text("Your Session is expired. Try Login Again",textScaleFactor: 1, style: GoogleFonts.roboto(),),
                             actions: [
                               new RaisedButton(
                                child: new Text("OK",textScaleFactor: 1, style: GoogleFonts.roboto(color: Colors.white),),
                                onPressed: ()async{
                                   await GoogleSignIn().signOut();
                                    setIntoDefault(UserDefault_IsLogin, "false");
                                    setIntoDefault(UserDefault_Profile, "");
                                    setIntoDefault(UserDefault_Location, "");
                                   Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>LoginScreen()), (route) => false);
                                },
                                color: AppDefault_Green,
                              )
                             ],
                           );
                         }
                         return CupertinoAlertDialog(
                              
                            content: new Text("Your Session is expired. Try Login Again",textScaleFactor: 1, style: GoogleFonts.roboto(),),
                            actions: <Widget>[
                              new CupertinoButton(
                                child: new Text("Ok",textScaleFactor: 1, style: GoogleFonts.roboto(),),
                                onPressed: (){
                                  setIntoDefault(UserDefault_IsLogin, "false");
                                   Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>LoginScreen()), (route) => false);
                                   
                                },
                              ),
                              
                            ],
                          );
                        }
                      );
         }
         else if (error.message.contains('SocketException')) {
           log("Please check your internet connection and try again");
           Alerts().showAlertDialog(context, "Internet Unavailable", "Please check your internet connection and try again",true);
         }
         else if (error.response.statusCode == 500) {
           log("Internal Sever Error");
         }
      }
      else if(DioErrorType.RESPONSE == error.type){
        
      }
      else if(DioErrorType.RESPONSE == error.type){
        
      }
      else if(DioErrorType.DEFAULT == error.type){
        
      }
    }
  }

  // Post API
  Future<dynamic> callPostApi(BuildContext context, String webLink, Object params, bool showLoading) async{
    if(showLoading){
      showLoader(context, true);
    }

    Dio dio = Dio();
    try {
      var response = await dio.post(webLink,data: params,options: Options(headers: {"Content-Type":"application/json"}));
      var data = response.data;
      if(showLoading){
        showLoader(context, false);
      }
      if(showLog){
        log("\n\n**************************URL**************************\n\n$webLink\n\n");
        log("\n\n**************************REQUESTS**************************\n\n${jsonEncode(params)}\n\n");
        log("\n\n**************************RESPONSE**************************\n\n$data\n\n");
      }
      return data;
    } on DioError catch(error){
      if(showLoading){
        showLoader(context, false);
      }
      log(error.toString());
      if(DioErrorType.RECEIVE_TIMEOUT == error.type || DioErrorType.CONNECT_TIMEOUT == error.type){
        log("Server is not reachable. Please verify your internet connection and try again");
      }
      else if(DioErrorType.RESPONSE == error.type){
        if (error.response.statusCode == 404) {
           log("Url is not found or Page is missing. Please check Url.");
         }
      }
      else if(DioErrorType.RESPONSE == error.type){
        if (error.response.statusCode == 406) {
           log("Request or parameter are bad which not acceptable by server. Please check requests or parameters.");
         }
      }
      else if(DioErrorType.RESPONSE == error.type){
        if (error.response.statusCode == 500) {
           log("Internal Sever Error");
         }
      }
      else if(DioErrorType.DEFAULT == error.type){
        if (error.message.contains('SocketException')) {
           log("Please check your internet connection and try again");
           Alerts().showAlertDialog(context, "Internet Connection", "Please check your internet connection and try again",true);
         }
      }
    }
  }

  Future<dynamic> callMultiPartApi(BuildContext context, String webLink, FormData params, bool showLoading,String token) async{
    if(showLoading){
      showLoader(context, true);
    }

    Dio dio = Dio();
    
    try {
      var response = await dio.post(webLink,data: params,options: Options(headers: {"Authorization": token}));
      var data = response.data;
      if(showLoading){
        showLoader(context, false);
      }
      if(showLog){
        log("\n\n**************************URL**************************\n\n$webLink\n\n");
        // log("\n\n**************************REQUESTS**************************\n\n$params\n\n");
        log("\n\n**************************RESPONSE**************************\n\n$data\n\n");
      }
      return data;
    } on DioError catch(error){
      if(showLoading){
        showLoader(context, false);
      }
      log(error.toString());
      if(DioErrorType.RECEIVE_TIMEOUT == error.type || DioErrorType.CONNECT_TIMEOUT == error.type){
        log("Server is not reachable. Please verify your internet connection and try again");
      }
      else if(DioErrorType.RESPONSE == error.type){
        if (error.response.statusCode == 404) {
           log("Url is not found or Page is missing. Please check Url.");
         }
      }
      else if(DioErrorType.RESPONSE == error.type){
        if (error.response.statusCode == 406) {
           log("Request or parameter are bad which not acceptable by server. Please check requests or parameters.");
         }
      }
      else if(DioErrorType.RESPONSE == error.type){
        if (error.response.statusCode == 500) {
           log("Internal Sever Error");
         }
      }
      else if(DioErrorType.DEFAULT == error.type){
        if (error.message.contains('SocketException')) {
           log("Please check your internet connection and try again");
           Alerts().showAlertDialog(context, "Internet Connection", "Please check your internet connection and try again",true);
         }
      }
    }
  }



  
  //Loader 
  static void showLoader(BuildContext context,bool show){
    if(show){
      showDialog(
        barrierColor: Colors.black.withOpacity(0.1),
        context: context,
        barrierDismissible: false,
        builder: (context){
          return new Center(
              child: new ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: new BackdropFilter(
                  filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                  child: new Container(
                    width: 80.0,
                    height: 80.0,
                    decoration: new BoxDecoration(
                      color: Colors.black.withOpacity(0.3)
                    ),
                    child: Center(
                      child: CupertinoActivityIndicator(
                        radius: 20,
                      )
                    ),
                  ),
                ),
              ),
            );
        }
      );
    }
    else{
        Navigator.of(context, rootNavigator: true).pop("");
    }
  }
}


