import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:logging/logging.dart';
import 'package:newapp/Bloc/viewStoryBloc.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/Common/multi_manager/flick_multi_manager.dart';
import 'package:newapp/Common/multi_manager/flick_multi_player.dart';
import 'package:newapp/Common/utils.dart';
import 'package:photo_view/photo_view.dart';
import 'package:signalr_client/signalr_client.dart';
import 'package:vector_math/vector_math_64.dart' as vector;
import 'package:matrix_gesture_detector/matrix_gesture_detector.dart';

class ImageZoomIn extends StatefulWidget {
  var name = "";
  var imageList;
  var current;
  var screen;
  var isView;
  var storyId;
  var isLike;
  ImageZoomIn({this.current, this.imageList, this.name,this.screen,this.isView,this.storyId,this.isLike});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ImageZoomInState();
  }
}

class ImageZoomInState extends State<ImageZoomIn> {
  PageController _pageController;
  var imgUrl = "";
  var minScale = 0.8;
  var maxScale = 6.6;
  var yOffset = 400.0;
  var xOffset = 50.0;

  var currentIndex = 0;
  PhotoViewControllerBase controller;
  PhotoViewScaleStateController scaleStateController;
  double defScale = 0.1;
  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;

  double _previousScale = 1.0;
  var _scale = 0.8;
  Matrix4 matrix = Matrix4.identity();

  // _reset() {
  //   if (mounted) {
  //     setState(() {
  //       minScale = 0.8;
  //       maxScale = 6.6;
  //       _previousScale = 1.0;
  //       _scale = 0.8;
  //       scaleStateController.reset();
  //       controller.reset();
  //       controller.position = Offset.zero;
  //     });
  //   }
  // }

  void onScaleState(PhotoViewScaleState scaleState) {}
  void onController(PhotoViewControllerValue value) {}

  FlickMultiManager flickMultiManager;

  @override
  void initState() {
    super.initState();
    if (widget.current == null || widget.current == 0) {
      setState(() {
        currentIndex = 0;
      });
    } else {
      setState(() {
        currentIndex = widget.current;
      });
    }

    flickMultiManager = FlickMultiManager();  

    scaleStateController = PhotoViewScaleStateController()
      ..outputScaleStateStream.listen(onScaleState);
    _pageController =
        PageController(initialPage: currentIndex, keepPage: false);
    controller = PhotoViewController()
      ..scale = defScale
      ..outputStateStream.listen(onController);


      if(widget.screen == "Dashboard"){
        Logger.root.level = Level.ALL;
        // Writes the log messages to the console
        Logger.root.onRecord.listen((LogRecord rec) {
          print('${rec.level.name}: ${rec.time}: ${rec.message}');
        });

        serverUrl = "http://travelappapi-dev.ap-south-1.elasticbeanstalk.com/eventHub";
        httpOptions = new HttpConnectionOptions(logger: transportProtLogger);

        hubConnection = HubConnectionBuilder().withUrl(serverUrl, options: httpOptions).configureLogging(hubProtLogger).build();

        hubConnection.onclose( (error) => print("Connection Closed"));
        hubConnection.keepAliveIntervalInMilliseconds = 100000;

        initViewHub();

        if(widget.isView == 0){
          apiViewStrory(context);
        }
    }
  }

  @override
  void dispose() {
    super.dispose();
    scaleStateController.dispose();
    controller.dispose();
    if(widget.screen == "Dashboard"){
      hubConnection.stop();
    }
  }


  void initViewHub() async{
    await hubConnection.start().catchError((E){
      print(E);
    });

    

    hubConnection.on("storyView", (object){
      log("Socket --------- ${object.toString()}");
      viewStoryBloc.changeViewStoryBlocState(object);
    });
    
  }


  final hubProtLogger = Logger("SignalR - hub");
final transportProtLogger = Logger("SignalR - transport");

var serverUrl;
final connectionOptions = HttpConnectionOptions;
var httpOptions;

HubConnection hubConnection;

  apiViewStrory(BuildContext context) async{
    var token = await getFromUserDefault(UserDefault_TOKEN);
    var userId = await getFromUserDefault(UserDefault_USERID);
    String webLink = ApiCallingHelper.baseUrl + "Story/ManageStory";

    var params = {
      "StoryId":widget.storyId,
      "IsLike":widget.isLike ==1?true:false,
      "OperationType":2,
      "UserId":int.parse(userId),
      "IsView": true
    };

    Dio dio = Dio();

    var resp = await dio.post(webLink,data:params,options: Options(headers: {"Authorization": token}));

    

    var data = resp.data["response"];

    if(resp.data["status"]){

      // setState(() {
      //   Array_Stories[index].isLike = islike ? 1 : 0;
      //   Array_Stories[index].totalLike = islike ? "${int.parse(Array_Stories[index].totalLike) + 1}":Array_Stories[index].totalLike == "0"?"0":"${int.parse(Array_Stories[index].totalLike) - 1}";
      // });
      // _onRefresh();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.name,
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        leading: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(Icons.arrow_back, color: Colors.black)),
        backgroundColor: Colors.white,
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Column(
          children: <Widget>[
            Expanded(
                child: PageView.builder(
                    reverse: false,
                    physics: ClampingScrollPhysics(),
                    controller: _pageController,
                    pageSnapping: true,
                    itemCount: widget.imageList.length,
                    onPageChanged: (page) {
                      setState(() {
                        currentIndex = page;
                      });
                    },
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        alignment: Alignment.center,
                        child: widget.imageList[index]["FileTypeId"] == 6?PhotoView.customChild(
                            backgroundDecoration:
                                BoxDecoration(color: Colors.white),
                            customSize: Size(
                                screenAwareHeight(370, context),
                                screenAwareHeight(500, context)),
                            minScale: minScale,
                            maxScale: maxScale,
                            initialScale: PhotoViewComputedScale.contained * 0.1,
                            scaleStateController: scaleStateController,
                            controller: controller,
                            enableRotation: false,
                            tightMode: false,
                            child: Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: FadeInImage(
                                    fit: BoxFit.contain,
                                    placeholder: Image.asset(
                                      "'assets/AppLogo/PikNikTransparent.png'",
                                      fit: BoxFit.contain,
                                    ).image,
                                    image: widget.imageList.isNotEmpty
                                        ? CachedNetworkImageProvider(
                                            widget.imageList[index]["FilePath"])
                                        : Image.asset(
                                            "'assets/AppLogo/PikNikTransparent.png'",
                                            fit: BoxFit.contain,
                                          ).image).image
                                )
                              ),
                            )
                            ):FlickMultiPlayer(
                                              url: widget.imageList[index]["FilePath"],
                                              flickMultiManager: flickMultiManager,
                                              image: "assets/PikNikTransparent.png",
                                            ),
                      );
                    })),
            widget.imageList.isNotEmpty
                ? Container(
                    margin: EdgeInsets.only(
                        top: screenAwareWidth(5, context)),
                    padding: EdgeInsets.only(
                        top: screenAwareWidth(10, context),
                        bottom: screenAwareWidth(10, context)),
                    width: double.maxFinite,
                    height: screenAwareWidth(90, context),
                    color: Colors.white,
                    child: ListView.separated(
                        padding: EdgeInsets.only(
                            left: screenAwareWidth(15, context),
                            right:
                                screenAwareWidth(15, context)),
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              //BOTTOMSLIDER
                              setState(() {
                                if (index != null) {
                                  _pageController.jumpToPage(index);
                                }
                              });
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                width: screenAwareWidth(60, context),
                                decoration: BoxDecoration(
                                    color: Colors.grey[300],
                                    border: Border.all(
                                        color: index == currentIndex
                                            ? Colors.red
                                            : Colors.transparent,
                                        width: 1.5),
                                    borderRadius: BorderRadius.circular(5)),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    widget.imageList[index]["FileTypeId"] == 6?FadeInImage(
                                        height: screenAwareWidth(50, context),
                                        width: screenAwareWidth(55, context),
                                        fit: BoxFit.contain,
                                        placeholder: Image.asset(
                                          "'assets/AppLogo/PikNikTransparent.png'",
                                          fit: BoxFit.contain,
                                        ).image,
                                        image: widget.imageList.isNotEmpty
                                            ? CachedNetworkImageProvider(
                                                widget.imageList[index]["FilePath"])
                                            : Image.asset(
                                                    'assets/AppLogo/PikNikTransparent.png')
                                                .image): new Icon(Icons.play_arrow_rounded),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                        separatorBuilder: (BuildContext context, int index) {
                          return Container(
                              width: screenAwareWidth(10, context));
                        },
                        itemCount: widget.imageList.length),
                  )
                : Container()
          ],
        ),
      ),
    );
  }

  Widget scaled() {
    return Positioned.fromRect(
        rect: Rect.fromPoints(Offset(xOffset - screenAwareWidth(350.0, context), yOffset - screenAwareHeight(100.0, context)),
            Offset(xOffset + screenAwareWidth(350.0, context), yOffset + screenAwareHeight(100.0, context))),
        child: GestureDetector(
          onScaleStart: (scaleDetails) {
            _previousScale = _scale;
            print(' scaleStarts = ${scaleDetails.focalPoint}');
          },
          onScaleUpdate: (scaleUpdates) {
            //ScaleUpdateDetails
            var offset = scaleUpdates.focalPoint;
            print(
                ' scaleUpdates = ${scaleUpdates.scale} rotation = ${scaleUpdates.rotation}');
            setState(() {
              xOffset = offset.dx;
              yOffset = offset.dy;
            });
            setState(() => _scale = (_previousScale * scaleUpdates.scale)
                .clamp(minScale, maxScale));
          },
          onScaleEnd: (scaleEndDetails) {
            _previousScale = null;
            print(' scaleEnds = ${scaleEndDetails.velocity}');
          },
          child: Transform(
              transform:
                  Matrix4.diagonal3(vector.Vector3(_scale, _scale, _scale)),
              alignment: FractionalOffset.center,
              child: Container(
                //color: Colors.red,
                width: double.maxFinite,
                child: FadeInImage(
                    fit: BoxFit.contain,
                    placeholder: Image.asset(
                      "'assets/AppLogo/PikNikTransparent.png'",
                      fit: BoxFit.contain,
                    ).image,
                    image: widget.imageList.isNotEmpty
                        ? CachedNetworkImageProvider(imgUrl)
                        : Image.asset('assets/AppLogo/PikNikTransparent.png').image),
              )),
        ));
  }

  Widget getMatrix() {
    return MatrixGestureDetector(
        shouldRotate: false,
        clipChild: false,
        shouldTranslate: true,
        focalPointAlignment: Alignment.center,
        onMatrixUpdate: (Matrix4 m, Matrix4 tm, Matrix4 sm, Matrix4 rm) {
          print(tm.getMaxScaleOnAxis());
          if (m.getMaxScaleOnAxis() > maxScale) {
            return;
          }
          setState(() {
            matrix = m;
          });
        },
        child: Container(
            // color: Colors.red,
            height: screenAwareWidth(500, context),
            width: double.maxFinite,
            child: Transform.scale(
              scale: 0.5,
              child: Transform(
                transform: matrix,
                child: FadeInImage(
                    fit: BoxFit.contain,
                    placeholder: Image.asset(
                      "'assets/AppLogo/PikNikTransparent.png'",
                      fit: BoxFit.contain,
                    ).image,
                    image: widget.imageList.isNotEmpty
                        ? CachedNetworkImageProvider(imgUrl)
                        : Image.asset('assets/AppLogo/PikNikTransparent.png').image),
              ),
            )));
  }
}
