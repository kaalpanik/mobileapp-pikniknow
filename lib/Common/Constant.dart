
import 'package:flutter/material.dart';

String UserDefault_OTP = "UserDefault_OTP";
String UserDefault_TOKEN = "UserDefault_TOKEN";
String UserDefault_USERID = "UserDefault_USERID";
String UserDefault_IsRegister = "UserDefault_IsRegister";
String UserDefault_IsLogin = "UserDefault_IsLogin";
String UserDefault_IsOnboard = "UserDefault_IsOnboard";
var OtpLottie = "assets/Animation/OtpLottie.json";

Color AppDefault_Green = Color(0xFF666c23);

String UserDefault_Profile = "UserDefault_Profile";
String UserDefault_Location = "UserDefault_Location";
