import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

double baseHeight = 812.0;

double baseWidth = 375.0;

double screenAwareHeight(double size, BuildContext context){
  return size * MediaQuery.of(context).size.height / baseHeight;
}

double screenAwareWidth(double size, BuildContext context){
  return size * MediaQuery.of(context).size.width / baseWidth;
}

Future<void> openMap(double latitude, double longitude) async {
    String googleUrl = 'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not open the map.';
    }
  }

