import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/Common/utils.dart';
import 'package:newapp/controllers/Onboarding/Onboarding.dart';

import 'controllers/Dashboard/Dashboard.dart';
import 'controllers/auth/LoginScreen.dart';


class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {

  double itemSize = 15;
  Duration animationDuration = Duration(milliseconds: 500);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLocation();
    moveTologinDash();
    Timer(Duration(milliseconds: 1), () {
      setState(() {
        itemSize = screenAwareWidth(200, context);
      });
    });
  }

  moveTologinDash() async{
    if(await getFromUserDefault(UserDefault_IsOnboard) == "true"){
      if(await getFromUserDefault(UserDefault_IsLogin) == "true"){
        Timer(
        Duration(seconds: 3),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => Dashboard())));
      }
      else{
        Timer(
        Duration(seconds: 3),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => LoginScreen())));
      }
    }
    else{
      Timer(
        Duration(seconds: 3),
        () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => Onboarding())));
    }
  }

  getLocation()async{
    Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        decoration: BoxDecoration(
//            gradient: RadialGradient(
//                radius: 0.3,
//                colors: [Colors.red[800], MyColors.ThemeColor]
//            ),
           color: Colors.white
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            AnimatedContainer(
              duration: animationDuration,
              width: itemSize,
              height: itemSize,
              curve: Curves.easeOutBack,
              decoration: new BoxDecoration(
                  image: DecorationImage(
                      image: Image.asset('assets/AppLogo/PikNikTransparent.png',height: screenAwareWidth(120, context),fit: BoxFit.cover).image
                  )
              ),
            ),
          ],
        ),
      ),
    );
  }
}
