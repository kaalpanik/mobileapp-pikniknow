import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/extention.dart';

class NotificationList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationListState();
  }
}

class NotificationListState extends State<NotificationList>{

  var Array_Notifications = [];

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero,(){
      ApiGetNotification(context);
    });
  }

  ApiGetNotification(BuildContext context) async{
    var token = await getFromUserDefault(UserDefault_TOKEN);
    var userId = await getFromUserDefault(UserDefault_USERID);

    var webLink = ApiManager.BaseURL + "user/UserNotification?userId=$userId&type=0&pageIndex=1&rowCount=1000";

    var params ={
      "userId": userId,
      "type": 0,
      "pageIndex": 1,
      "rowCount": 10000000
    };

    var formData = FormData.fromMap(params);

    ApiManager().callMultiPartApi(context, webLink,formData, true, token).then((data){
      if(data["status"]){
        var response = jsonDecode(data["response"]);
        for(var notification in response){
          setState(() {
            Array_Notifications.add(notification);
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Notifications",style: GoogleFonts.roboto(color: AppDefault_Green),),
        leading: InkWell(
          onTap: (){
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back,color: Colors.black,)),
      ),
      body: new Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: new ListView.builder(
          itemCount: Array_Notifications.length,
          itemBuilder: (context,index){
            return new Column(
              children: [
                new ListTile(
                  onTap: (){
                  },
                  leading: new Container(
                    height: 30,width:30,
                    child: new ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Icon(Icons.notifications),
                    ),
                  ),
                  title: new Text(Array_Notifications[index]["title"],style:GoogleFonts.poppins()),
                  subtitle: new Text(Array_Notifications[index]["content"],style:GoogleFonts.poppins()),
                ),
                new Divider(height: 1,)
              ],
            );
          },
        ),
      )
    );
  }
}