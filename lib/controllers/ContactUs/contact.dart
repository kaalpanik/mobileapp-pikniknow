import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/extention.dart';

class ContactUs extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return ContactState();
  }
}

class ContactState extends State<ContactUs>{

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero,(){
      apiContactUs(context);
    });
  }

  var contactUsDetails;

  apiContactUs(BuildContext context)async{
    var token = await getFromUserDefault(UserDefault_TOKEN);
    var webLink = ApiManager.BaseURL + "Page/GetByPageLocationId/16";

    ApiManager().callGetApi(context, webLink, true, token).then((data){
      if(data["status"] == true){
        setState(() {
          contactUsDetails = data["response"];
        });
      }      
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: new IconButton(icon: Icon(Icons.arrow_back_ios,color:Colors.black), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: new Text("Contact Us",style:GoogleFonts.roboto(color:Colors.black))
      ),
      body: new Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: new SingleChildScrollView(
          child:new Padding(
            padding: EdgeInsets.all(20),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Text(contactUsDetails == null? "":contactUsDetails["heading"],style:GoogleFonts.roboto(color:Colors.black,fontSize: 20,fontWeight: FontWeight.bold)),
                new SizedBox(height: 20,),
                new Text(contactUsDetails == null? "":contactUsDetails["title"],style:GoogleFonts.roboto(color:Colors.black,fontSize: 17,fontWeight: FontWeight.w600)),
                new SizedBox(height: 20,),
                new Text(contactUsDetails == null? "":contactUsDetails["description"],style:GoogleFonts.roboto(color:Colors.black,fontSize: 17)),
              ],
            )
          )
        ),
      ),
    );
  }
  
}