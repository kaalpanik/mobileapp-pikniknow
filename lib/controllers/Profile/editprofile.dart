import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:ui';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:newapp/Bloc/editNumberBloc.dart';
import 'package:newapp/Bloc/refreshProfileImage.dart';
import 'package:newapp/Common/Alerts.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/ProgressBar.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/controllers/Dashboard/Dashboard.dart';
import 'package:newapp/controllers/Story/searchMap.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:toast/toast.dart';

class EditProfile extends StatefulWidget {
  double lat, long;
  var screen;
  EditProfile({this.lat, this.long,this.screen});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return EditProfileState();
  }
}

class EditProfileState extends State<EditProfile> {

  final homeScaffoldKey = GlobalKey<ScaffoldState>();
  var userId = "";
  var token = "";
  var userName = "";
  var roleId = "";
  var mobileNumber = "";
  var profile = "";
  var bio = "";
  var lat = "";
  var long = "";
  var city = "";
  var state = "";
  bool isNotificationEnable = false;
  var emailID = "";
  var createdByDate = "";
  var modifiedById = "";
  CameraPosition _kGooglePlex;
  CameraPosition _kLake;

  TextEditingController _nameCont = TextEditingController();
  TextEditingController _bioCont = TextEditingController();
  TextEditingController _mobileCont = TextEditingController();
  TextEditingController emailCont = TextEditingController();

  TextEditingController cityCont = TextEditingController();
  TextEditingController stateCont = TextEditingController();

  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: "AIzaSyB0wVEE6PvMyYd-NdTxYOr2Q7KHZnF0Xek");

  Future<void> _handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: "AIzaSyB0wVEE6PvMyYd-NdTxYOr2Q7KHZnF0Xek",
      onError: onError,
      mode: Mode.overlay,
      language: "in",
      components: [Component(Component.country, "in")],
      types: ["(cities)"]
    );

    displayPrediction(p);
}

Future<Null> displayPrediction(Prediction p) async {
  if (p != null) {
    // get detail (lat/lng)
    PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
    final lat = detail.result.geometry.location.lat;
    final lng = detail.result.geometry.location.lng;
    getLocationName(lat, lng);
  }
}

void onError(PlacesAutocompleteResponse response) {
    homeScaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

getLocationName(double latitude,double longitude) async{
  final coordinates = new Coordinates(latitude, longitude);
  var addresses = await Geocoder.google("AIzaSyCAgyC2qa2RGzifO_bgIFd-HMBB0rkTQiw").findAddressesFromCoordinates(coordinates);
  var first = addresses.first;
  print("${first.featureName} : ${first.addressLine}");
  setState(() {
    cityCont.text = first.subAdminArea;
    stateCont.text = first.adminArea;
  });
}

var userProfile;

  apiGetProfile(BuildContext context) async{
    Loader.getInstance().showOverlay(context);
    String webLink = ApiCallingHelper.baseUrl + "User/$userId";

    Dio dio = new Dio();

    var resp = await dio.get(webLink, options: Options(headers: {"Authorization": token}));

    Loader.getInstance().dismissOverlay();
    var data = resp.data;

    if(data["status"]){
      setState(() {
        userProfile = data["response"];
        log(userProfile.toString());
        _nameCont.text = userProfile["userName"] == null?"":userProfile["userName"];
        _mobileCont.text = userProfile["mobileNumber"] == null?"":userProfile["mobileNumber"];
        _bioCont.text = userProfile["bio"] == null?"":userProfile["bio"];
        cityCont.text = userProfile["city"] == null?"":userProfile["city"];
        stateCont.text = userProfile["state"] == null?"":userProfile["state"];
        isNotificationEnable = userProfile["isNotificationEnable"];
        emailCont.text = userProfile["emailId"] == null?"":userProfile["emailId"];
        setIntoDefault(UserDefault_Profile, userProfile["profilePicturePath"]);
        setIntoDefault(UserDefault_Location, "${userProfile['city'] == null?'':userProfile['city']}, ${userProfile['state'] == null?'':userProfile['state']}");
        if(userProfile["profilePicturePath"]!=null){
          refreshProfileBloc.changeRefreshProfileState({"profilePath":userProfile["profilePicturePath"],"location": "${userProfile['city']}, ${userProfile['state']}"});
        }
        
      });
    }
  }

  API_EDIT_PROFILE(BuildContext context) async{
    String webLink = "http://travelappapi-dev.ap-south-1.elasticbeanstalk.com/api/user/UpdateProfile";

    Map<String, dynamic> params = {
      "userId": userId,
      "userName": _nameCont.text,
      "roleId": userProfile["roleId"],
      "mobileNumber": _mobileCont.text,
      "bio": _bioCont.text,
      "latitude": widget.lat.toString(),
      "longitude": widget.long.toString(),
      "city":cityCont.text,
      "state": stateCont.text,
      "isNotificationEnable": isNotificationEnable.toString(),
      "emailId": emailCont.text,
      "createdByDate": "2020-10-03T16:13:03.857",
      "createdById": userProfile["createdById"].toString(),
      "statusId": userProfile["statusId"],
      "sessionId": userProfile["sessionId"].toString(),
    };

    if(_image != null){
      params["profileMultipartImage"] = await MultipartFile.fromFile(_image.path,filename:_image.path.split("/").last);
    }
    else{
      params["profilePicturePath"] = userProfile["profilePicturePath"];
    }
    

    print(params);

    FormData other = FormData.fromMap(params);


    ApiManager().callMultiPartApi(context, webLink, other, true, token).then((data){
      var apiData = data;
      if(apiData["status"]){
        apiGetProfile(context);
        if(widget.screen == "OTP"){
          // showDialog(
          //   context: context,
          //   builder: (alertcontext){
          //     if(Platform.isIOS){
          //                   return CupertinoAlertDialog(
                              
          //                   title: new Text("Successfully",style: GoogleFonts.roboto()),
          //         content: new Text("Your profile has been updated.",style: GoogleFonts.roboto()),
          //                   actions: <Widget>[
          //                     new CupertinoButton(
          //                       child: new Text("OK",textScaleFactor: 1, style: GoogleFonts.roboto(),),
          //                       onPressed: (){
          //                          Navigator.of(alertcontext).pop();
          //               Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>Dashboard()),(Route<dynamic>route)=>false);
          //                       },
          //                     )
          //                   ],
          //                 );
          //                }
          //                else{
          //                  return AlertDialog(
          //                    title: new Text("Successfully",style: GoogleFonts.roboto()),
          //         content: new Text("Your profile has been updated.",style: GoogleFonts.roboto()),
          //                    actions: [
          //                      new RaisedButton(
          //                       child: new Text("OK",textScaleFactor: 1, style: GoogleFonts.roboto(color: Colors.white),),
          //                       onPressed: (){
          //                          Navigator.of(alertcontext).pop();
          //               Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>Dashboard()),(Route<dynamic>route)=>false);
          //                       },
          //                       color: AppDefault_Green,
          //                     )
          //                    ],
          //                  );
          //                }
              
          //   }
          // );

          Alert(
      context: context,
      type: AlertType.success,
      title: "Successful",
      desc: "Your profile has been updated.",
      closeFunction: (){},
      style: AlertStyle(
        isOverlayTapDismiss: false,
      ),
      closeIcon: Icon(Icons.close,size:0,color:Colors.transparent),
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: (){
            Navigator.of(context).pop();
                        Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>Dashboard()),(Route<dynamic>route)=>false);
          },
          color: AppDefault_Green,
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();
        }
        else{
          Alerts().showAlertDialog(context, "Successful", "Your profile has been updated.",false);
        }
        
      }
      else{
        Alerts().showAlertDialog(context, "", apiData["message"],true);
      }
    });

  }

 
 
  // navigateToMap(BuildContext context, Position position) async {
  //   var result = await Navigator.of(context).push(CupertinoPageRoute(
  //       builder: (context) => SearchMap(
  //             lat: position.latitude,
  //             long: position.longitude,
  //           )));

  //   print(result);

  //   lat = result["latitude"];
  //   long = result["longitude"];

  //   _goToTheLocation();
  // }

  // Future<void> _goToTheLocation() async {
  //   final GoogleMapController controller = await _controller.future;
  //   controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  // }

  @override
  void initState() {
    // _kGooglePlex = CameraPosition(
    //   target: LatLng(widget.lat, widget.long),
    //   zoom: 14.4746,
    // );

    // _kLake =
    //     CameraPosition(target: LatLng(widget.lat, widget.long), zoom: 14.4746);
    // TODO: implement initState
    super.initState();
    getLocation();
    getFromUserDefault(UserDefault_TOKEN).then((t) {
      token = t;
    });
    getFromUserDefault(UserDefault_USERID).then((id) {
      userId = id;
      apiGetProfile(context);
    });

    editNumberBloc.EditNumberBlocState.listen((event) {
      setState(() {
        _mobileCont.text = event;
      });
    });
  }

  getLocation()async{
    Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  File _image;

  Future getImage(String source) async {
    if(source == "Camera"){
      var image = await ImagePicker.pickImage(source: ImageSource.camera);
      setState(() {
        _image = image;
      });
    }
    else{
      var image = await ImagePicker.pickImage(source: ImageSource.gallery);
      setState(() {
        _image = image;
      });
    }
    

    _cropImage(_image);
  }

  Future<Null> _cropImage(File imageFile) async {
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: imageFile.path,
      androidUiSettings: AndroidUiSettings(
        toolbarTitle: "Crop Image",
        toolbarColor: AppDefault_Green,
        toolbarWidgetColor: Colors.white,
      ),
    );

    setState(() {
      _image = croppedFile;
    });
}

Future<bool> validation(BuildContext context) async{
  if(_nameCont.text.isEmpty){
    Alerts().showAlertDialog(context, "", "Please Enter Name.", true);
    return false;
  }
  else if(_mobileCont.text.isEmpty){
    Alerts().showAlertDialog(context, "", "Please Enter Mobile Number.", true);
    return false;
  }
  
  else{
    return true;
  }
}

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: homeScaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: widget.screen == "OTP"? new SizedBox(height: 0):InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.black,
            )),
        centerTitle: true,
        title: Text(
          "Edit Profile",
          style: GoogleFonts.roboto(color: Colors.black),
        ),
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Center(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 30,
                ),
                Stack(
                  children: <Widget>[
                    Container(
                      width: 120,
                      height: 120,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(width: 1),
                          image: DecorationImage(
                              image: userProfile != null?userProfile["profilePicturePath"] == null? _image ==null?AssetImage("assets/AppLogo/PikNikTransparent.png"): FileImage(_image):_image ==null?NetworkImage(userProfile["profilePicturePath"]):FileImage(_image):AssetImage("assets/AppLogo/PikNikTransparent.png"),
                              fit: BoxFit.fill)),
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaX: 0.5, sigmaY: 0.5),
                        child: Container(
                            alignment: Alignment.center,
                            child: RaisedButton(
                                onPressed: () {

                                  Alert(
      context: context,
      title: "",
      desc: "Select Option",
      image: Icon(Icons.image,size:70),
      
      style: AlertStyle(
        titleStyle: TextStyle(fontSize: 2),
        isOverlayTapDismiss: false,
      ),
      // closeFunction: (){},
      // closeIcon: Icon(Icons.close,size:0,color:Colors.transparent),
      buttons: [
        DialogButton(
          child: Text(
            "Camera",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: (){
            getImage("Camera");
                                              Navigator.of(context).pop();
          },
          color: AppDefault_Green,
          radius: BorderRadius.circular(0.0),
        ),

        DialogButton(
          child: Text(
            "Gallery",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: (){
           getImage("Gallery");
                                              Navigator.of(context).pop();
          },
          color: AppDefault_Green,
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();


                                 
                                },
                                color: Colors.transparent,
                                child: Text(
                                  "Edit Image",
                                  style: GoogleFonts.roboto(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500),
                                ))),
                      ),
                    ),
                  ],
                ),
                //  Row(
                //    mainAxisAlignment: MainAxisAlignment.center,
                //    children: <Widget>[
                //      Icon(Icons.add_photo_alternate),
                //      SizedBox(width: 5,),
                //      Text("Edit photo",style: GoogleFonts.roboto(),)

                //    ],)
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: ListTile(
                    title: Text("Name*",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                    subtitle: CupertinoTextField(
                      placeholder: "Enter Name",
                      controller: _nameCont,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadiusDirectional.circular(10),
                          color: Colors.grey[200]),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: ListTile(
                    title: Text("Bio",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                    subtitle: CupertinoTextField(
                      placeholder: "Enter Bio",
                      controller: _bioCont,
                      maxLines: 4,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadiusDirectional.circular(10),
                          color: Colors.grey[200]),
                    ),
                  ),
                ),
                
               
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: ListTile(
                    title: Text("Phone no*",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                    subtitle: CupertinoTextField(
                      enabled: false,
                      placeholder: "Enter Phone Number",
                      controller: _mobileCont,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadiusDirectional.circular(10),
                          color: Colors.grey[200]),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(35, 0, 35, 0),
                  child: Row(
                    children: [
                      new Expanded(child:new Container()),
                      InkWell(
                        onTap: (){
                          showDialog(context: context,barrierDismissible: false,builder: (context)=>MobileNumberPopup());
                        },
                        child: Text("Change Number",
                                style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color:CupertinoColors.activeBlue)),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: ListTile(
                    title: Text("Email Id*",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                    subtitle: CupertinoTextField(
                      enabled: userProfile == null ?true :userProfile["emailId"] == ""? true: true,
                      controller: emailCont,
                      placeholder: "Enter Email Address",
                      decoration: BoxDecoration(
                          borderRadius: BorderRadiusDirectional.circular(10),
                          color: Colors.grey[200]),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: ListTile(
                    onTap: (){
                      FocusScope.of(context).requestFocus(FocusNode());
                        _handlePressButton();
                    },
                    title: Text("City*",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                    subtitle: AbsorbPointer(
                      child: CupertinoTextField(
                        
                        placeholder: "Select City",
                        controller: cityCont,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadiusDirectional.circular(10),
                            color: Colors.grey[200]),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: ListTile(
                    title: Text("State*",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                    subtitle: AbsorbPointer(
                      child: CupertinoTextField(
                        placeholder: "Select State",
                        controller: stateCont,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadiusDirectional.circular(10),
                            color: Colors.grey[200]),
                      ),
                    ),
                  ),
                ),
                
                Padding(
                  padding: const EdgeInsets.fromLTRB(15,0,15,0),
                  child: ListTile(
                    title: Text("Notifications",
                        style: GoogleFonts.roboto(
                          color: Colors.black,
                          fontWeight: FontWeight.w600,
                        )),
                    trailing: CupertinoSwitch(
                        value: isNotificationEnable,
                        activeColor: AppDefault_Green,
                        onChanged: (value) {
                          setState(() {
                            isNotificationEnable = value;
                          });
                        }),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                
                new SizedBox(height: 30,),
                Container(
                  width: 200,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadiusDirectional.circular(10)),
                      color: AppDefault_Green,
                      child: Text("Submit",
                          style: GoogleFonts.roboto(
                              fontWeight: FontWeight.w500,
                              color: Colors.white)),
                      onPressed: () {
                        print(token);
                        // print(userId);
                        validation(context).then((value){
                          if(value){
                            API_EDIT_PROFILE(context);
                          }
                        });
                        // API_EDIT_PROFILE(context);
                        // Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>Dashboard()),(Route<dynamic>route)=>false);
                      }),
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MobileNumberPopup extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MobileNoPopupState();
  }
}

class MobileNoPopupState extends State<MobileNumberPopup>{

  TextEditingController mobileCont = TextEditingController();
  TextEditingController otpCont = TextEditingController();

  @override
  void initState() {
    super.initState();
    
  }

  Future<bool> validationMobile(BuildContext context)async{
    if(mobileCont.text.isEmpty){
      Toast.show("Please enter mobile no", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM,backgroundColor: AppDefault_Green,textColor: Colors.white);
      return false;
    }
    else if(mobileCont.text.length != 10){
      Toast.show("Please enter valid mobile no", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM,backgroundColor: AppDefault_Green,textColor: Colors.white);
      return false;
    }
    else{
      return true;
    }
  }

  Future<bool> validationOTP(BuildContext context)async{
    if(otpCont.text.isEmpty){
      Toast.show("Please enter OTP", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM,backgroundColor: AppDefault_Green,textColor: Colors.white);
      return false;
    }
    else{
      return true;
    }
  }

  bool sendOtp = false;
  String _verificationId;  
  AuthCredential _phoneAuthCredential;

  Future<void> _submitPhoneNumber(String phone, BuildContext context) async {
    /// NOTE: Either append your phone number country code or add in the code itself
    /// Since I'm in India we use "+91 " as prefix `phoneNumber`
    String phoneNumber = "+91 " + phone;
    print(phoneNumber);

    /// The below functions are the callbacks, separated so as to make code more readable
    void verificationCompleted(AuthCredential phoneAuthCredential) {
      print('verificationCompleted');
      this._phoneAuthCredential = phoneAuthCredential;
      print(phoneAuthCredential);
    }

    void verificationFailed(FirebaseAuthException error) {
      print(error);
    }

    void codeSent(String verificationId, [int code]) {
      print('codeSent');
      setState(() {
        sendOtp = true;
      });
      _verificationId = verificationId;
    }

    void codeAutoRetrievalTimeout(String verificationId) {
      print('codeAutoRetrievalTimeout');
    }

    await FirebaseAuth.instance.verifyPhoneNumber(
      /// Make sure to prefix with your country code
      phoneNumber: phoneNumber,

      /// `seconds` didn't work. The underlying implementation code only reads in `milliseconds`
      timeout: Duration(milliseconds: 110000),

      /// If the SIM (with phoneNumber) is in the current device this function is called.
      /// This function gives `AuthCredential`. Moreover `login` function can be called from this callback
      verificationCompleted: verificationCompleted,

      /// Called when the verification is failed
      verificationFailed: verificationFailed,

      /// This is called after the OTP is sent. Gives a `verificationId` and `code`
      codeSent: codeSent,

      /// After automatic code retrival `tmeout` this function is called
      codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
    ); // All the callbacks are above
  }



  void _submitOTP() {
    /// get the `smsCode` from the user
    String smsCode = otpCont.text;

    /// when used different phoneNumber other than the current (running) device
    /// we need to use OTP to get `phoneAuthCredential` which is inturn used to signIn/login
    this._phoneAuthCredential = PhoneAuthProvider.credential(
        verificationId: this._verificationId, smsCode: smsCode);

    _login();
  }

  Future<void> _login() async {
    /// This method is used to login the user
    /// `AuthCredential`(`_phoneAuthCredential`) is needed for the signIn method
    /// After the signIn method from `AuthResult` we can get `FirebaserUser`(`_firebaseUser`)
    try {
      await FirebaseAuth.instance
          .signInWithCredential(this._phoneAuthCredential)
          .then((UserCredential authRes) {
        Loader.getInstance().dismissOverlay();

        authRes.user.getIdTokenResult().then((value){
                                      editNumberBloc.changeEditNumberBlocState(mobileCont.text);
                                      Navigator.of(context).pop();
                                    }); 
      });
    } catch (e) {
      Loader.getInstance().dismissOverlay();
      print(e.message);
      if(e.code == "invalid-verification-code"){
        Toast.show("OTP not matched. Try Again.", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM,backgroundColor: AppDefault_Green,textColor: Colors.white);
      }
      else{
        Toast.show("Authentication error, kindly try login again.", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM,backgroundColor: AppDefault_Green,textColor: Colors.white);
      }
    }
  }


  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: SingleChildScrollView(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: new Container(
                  width: MediaQuery.of(context).size.width -50,
                  color:Colors.white,
                  child: new Padding(padding: EdgeInsets.all(15),
                    child: new Column(
                      children: [
                        Stack(
                          children: [
                            Container(
                              width: double.maxFinite,
                              child: new Text("Mobile Verification !!",textAlign: TextAlign.center,style: GoogleFonts.roboto(
                                    fontWeight: FontWeight.w500,

                                    fontSize: 17,
                                    )),
                            ),
                            new Align(
                              alignment: Alignment.centerRight,
                              child: InkWell(onTap:(){
                                Navigator.of(context).pop();
                              },child: Icon(Icons.close))
                            )
                          ],
                        ),
                        new SizedBox(height: 15,),
                        new Divider(height: 0,),

                        new SizedBox(height: 15,),

                        ListTile(
                          contentPadding: EdgeInsets.all(0),

                          title: Text("Phone no",
                              style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                          subtitle: Padding(
                            padding: const EdgeInsets.only(top:8.0),
                            child: Container(
                              child: Center(
                                child: TextField(
                                  keyboardType: TextInputType.number,
                                  controller: mobileCont,
                                  style: GoogleFonts.roboto(),
                                  decoration: InputDecoration(
                                    hintText: "Enter Mobile Number",
                                    hintStyle: GoogleFonts.roboto(),
                                    contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(20))
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        new SizedBox(height: 10,),

                        sendOtp ?ListTile(
                          contentPadding: EdgeInsets.all(0),

                          title: Text("OTP",
                              style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                          subtitle: Padding(
                            padding: const EdgeInsets.only(top:8.0),
                            child: Container(
                              child: Center(
                                child: TextField(
                                  keyboardType: TextInputType.number,
                                  controller: otpCont,
                                  style: GoogleFonts.roboto(),
                                  decoration: InputDecoration(
                                    hintText: "Enter OTP",
                                    hintStyle: GoogleFonts.roboto(),
                                    contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(20))
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ): new SizedBox(height:0),

                        new SizedBox(height: 15,),

                        Container(
                  width: 200,
                  child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadiusDirectional.circular(10)),
                      color: AppDefault_Green,
                      child: Text(sendOtp?"Submit":"Send OTP",
                          style: GoogleFonts.roboto(
                              fontWeight: FontWeight.w500,
                              color: Colors.white)),
                      onPressed: () {

                        if(sendOtp){
                          validationOTP(context).then((value){
                            if(value){
                              _submitOTP();
                            }
                          });
                        }
                        else{
                          validationMobile(context).then((value){
                            if(value){
                              _submitPhoneNumber(mobileCont.text, context);
                            }
                          });
                          
                        }
                        
                      }),
                ),

                        

                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
