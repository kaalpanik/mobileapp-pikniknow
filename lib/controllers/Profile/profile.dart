import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Profile extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ProfileState();
  }
}

class ProfileState extends State<Profile>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: InkWell(
          onTap: (){
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back,color: Colors.black,)),
        centerTitle: true,
        title: Text("Profile",style: GoogleFonts.roboto(color: Colors.black),
    ),
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Center(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[
               SizedBox(height: 30,),
               Stack(
                 children: <Widget>[
                   Container(
                     width: 120,
                     height: 120,
                     decoration: BoxDecoration(
                       shape: BoxShape.circle,
                       border: Border.all(
                         width: 1
                       ),
                       image: DecorationImage(image: AssetImage("assets/lonvala.jpg"),fit: BoxFit.fill)
                     ),
                   child: BackdropFilter(
                     filter: ImageFilter.blur(
                       sigmaX: 0.5,
                       sigmaY: 0.5
                     ),
                     child: Container(
                       alignment: Alignment.center,
                       child: RaisedButton(
                         onPressed: (){},
                         color: Colors.transparent,
                         child: Text("Edit Image",style: GoogleFonts.roboto(color: Colors.white,fontWeight: FontWeight.w500),))),),
                   ),
                 ],
               ),
              //  Row(
              //    mainAxisAlignment: MainAxisAlignment.center,
              //    children: <Widget>[
              //      Icon(Icons.add_photo_alternate),
              //      SizedBox(width: 5,),
              //      Text("Edit photo",style: GoogleFonts.roboto(),)

              //    ],)
              SizedBox(height: 10,),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: ListTile(
                  title: Text("Name*",style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                  subtitle: CupertinoTextField(
                    placeholder: "Enter Name",
                    
                    decoration: BoxDecoration(
                      borderRadius: BorderRadiusDirectional.circular(10),
                      color: Colors.grey[200]
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: ListTile(
                  title: Text("Bio*",style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                  subtitle: CupertinoTextField(
                    placeholder: "Enter Bio",
                    decoration: BoxDecoration(
                       borderRadius: BorderRadiusDirectional.circular(10),
                      color: Colors.grey[200]
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Text("Select Location",
                      style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                  SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: Card(
                      elevation: 10,
                      child: Stack(
                        children: <Widget>[
                          Container(
                            height: 100,
                            width: 300,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage("assets/map2.png"),
                                    fit: BoxFit.fill)),
                                    child: Icon(Icons.person_pin,size: 50,color: Colors.indigo,),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                   Padding(
                     padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                     child: ListTile(
                title: Text("Phone no*",style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                subtitle: CupertinoTextField(
                  placeholder: "Enter Phone Number",
                  decoration: BoxDecoration(
                       borderRadius: BorderRadiusDirectional.circular(10),
                      color: Colors.grey[200]
                  ),
                ),
              ),
                   ),
               Padding(
                  padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                 child: ListTile(
                  title: Text("Email Id*",style: GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                  subtitle: CupertinoTextField(
                    placeholder: "Enter Email Address",
                    decoration: BoxDecoration(
                       borderRadius: BorderRadiusDirectional.circular(10),
                      color: Colors.grey[200]
                    ),
                  ),
              ),
               ),
              SizedBox(height: 10,),
              Container(
                width: 200,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(10)
                  ),
                  color: Colors.blue[700],
                  child: Text("Submit",style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: Colors.white)),
                  
                  onPressed: (){

                }),
              ),
              SizedBox(height: 20,)
              ],
            ),
          ),
        ),
      ),
    );
  }
}