import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/ProgressBar.dart';
import 'package:newapp/Common/extention.dart';

import 'editprofile.dart';

class UserProfileScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return UserProfileScreenState();
  }
}

class UserProfileScreenState extends State<UserProfileScreen> with TickerProviderStateMixin{


  var token = "";
  var userId = "";

  getUserProfile() async {
    String webLink = ApiCallingHelper.baseUrl + "User/$userId";

    Dio dio = new Dio();
    var resp = await dio.get(webLink,options: Options(headers: {"Authorization": token}));
    Loader.getInstance().showOverlay(context);
    var data =resp.data["response"];
    if(resp.data["status"]){
     Loader.getInstance().dismissOverlay();
     setState(() {
      
     });

    }
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getFromUserDefault(UserDefault_TOKEN).then((t){
      token = t;
      
    });
     getFromUserDefault(UserDefault_USERID).then((id){
      userId = id;
     getUserProfile();
    });
   
    // TabController(vsync: this,length: 2);
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("UserName",style:TextStyle(fontWeight: FontWeight.w500,color: AppDefault_Green),),
        backgroundColor: Colors.white,
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        width: 100,
                        height: 100,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                            width: 2,
                            color: AppDefault_Green
                          ),
                        ),
                        child: FadeInImage(
                              placeholder: AssetImage("assets/lonvala.jpg"),
                                image:AssetImage("assets/lonvala.jpg"),fit: BoxFit.fill,),
                      ),
                      Positioned(
                        top: 75,
                        left: 70,
                        child: Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppDefault_Green
                          ),
                          child: Icon(Icons.add,color: Colors.white,size: 20,)))
                    ],
                  ),

                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          child: Column(
                          children: <Widget>[
                            Text("1",style: TextStyle(fontWeight:FontWeight.w500),),
                            Text("Post")
                          ],
                          ),
                        ),
                      ),
                       Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: Container(
                          child: Column(
                          children: <Widget>[
                            Text("1",style: TextStyle(fontWeight:FontWeight.w500)),
                            Text("Followers")
                          ],
                          ),
                      ),
                       ),
                       Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: Container(
                          child: Column(
                          children: <Widget>[
                            Text("1",style: TextStyle(fontWeight:FontWeight.w500)),
                            Text("Following")
                          ],
                          ),
                      ),
                       )
                    ],
                  ), 
                ],
              ), 
            ),
            // SizedBox(height: 5,),
                  Padding(
                    padding: const EdgeInsets.only(left:15.0,top: 8.0,bottom: 8.0),
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("UserName"),
                          Text("User's Descripttion")
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height:5),
                  InkWell(
                    onTap: (){
                      Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>EditProfile()));
                    },
                      child: Center(
                      child: Container(
                        width: MediaQuery.of(context).size.width - 50,
                        decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: Colors.black
                            ),
                            borderRadius: BorderRadius.circular(5)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Center(
                            child: Text("Edit Profile",style: GoogleFonts.roboto(fontWeight: FontWeight.w500),)
                          ),
                        ),
                      ),
                    ),
                  )
              
          ],
        ),
      ),
    );
  }
}