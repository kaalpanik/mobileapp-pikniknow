import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/controllers/Suggetions/suggetions.dart';

class FollowingList extends StatefulWidget{
  var userId;
  FollowingList({@required this.userId});
  @override
  State<StatefulWidget> createState() {
    return FollowingListState();
  }
}

class FollowingListState extends State<FollowingList>{
  var Array_Followings = [];
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero,(){
      apiGetFollowing(context);
    });
  }

  apiGetFollowing(BuildContext context) async{
    var token = await getFromUserDefault(UserDefault_TOKEN);
    
    var webLink = ApiManager.BaseURL + "UserFollower?searchQuery&orderByColumn&sortOrder&pageIndex=1&rowCount=1000000000&userId=${widget.userId}&typeId=1";

    ApiManager().callGetApi(context, webLink, false, token).then((data){
      if(data["status"]){
        var response = jsonDecode(data["response"]);
        for(var user in response){
          setState(() {
            Array_Followings.add(user);
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: new ListView.builder(
          itemCount: Array_Followings.length,
          itemBuilder: (context,index){
            return new Column(
              children: [
                new ListTile(
                  onTap: (){
                    Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>SuggetionScreen(userId: Array_Followings[index]["userId"].toString())));
                  },
                  leading: new Container(
                    height: 30,width:30,
                    child: new ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Array_Followings[index]["profilePicturePath"] == null? Icon(Icons.person) : new Image.network( Array_Followings[index]["profilePicturePath"]),
                    ),
                  ),
                  title: new Text(Array_Followings[index]["userName"],style:GoogleFonts.poppins()),
                  subtitle: new Text(Array_Followings[index]["emailId"],style:GoogleFonts.poppins()),
                  trailing: new Icon(Icons.arrow_forward_ios),
                ),
                new Divider(height: 1,)
              ],
            );
          },
        ),
      ),
    );
  }
}