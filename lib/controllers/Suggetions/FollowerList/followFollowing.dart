import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/controllers/Suggetions/FollowerList/followerList.dart';
import 'package:newapp/controllers/Suggetions/FollowerList/followingList.dart';

class FollowFollowing extends StatefulWidget{
  var userId;
  int segmentValue;
  FollowFollowing({@required this.segmentValue, @required this.userId});
  @override
  State<StatefulWidget> createState() {
    return FollowFollowingState();
  }
}

class FollowFollowingState extends State<FollowFollowing>{

  var segmentValue = 0;

  @override
  void initState() {
    super.initState();
    segmentValue = widget.segmentValue;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Profile",style: GoogleFonts.roboto(color: AppDefault_Green),),
        leading: InkWell(
          onTap: (){
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back,color: Colors.black,)),
      ),
      body: new Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: new Column(
          children: [
            new SizedBox(height: 10,),
            Container(
                    width: 250,
                    child: new CupertinoSlidingSegmentedControl(children: 
                    {
                      0: new Text("Followers", style:GoogleFonts.roboto()),
                      1: new Text("Followings", style:GoogleFonts.roboto()),
                    }, 
                    groupValue: segmentValue,
                    onValueChanged: (value){
                      setState(() {
                        segmentValue = value;
                      });
                      
                    }),
                  ),
                  new SizedBox(height: 10),
            new Expanded(
              child: segmentValue == 0? FollowerList(userId: widget.userId,) : FollowingList(userId: widget.userId,),
            ),
          ],
        ),
      ),
    );
  }
}