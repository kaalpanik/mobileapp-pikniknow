import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widgets/flutter_widgets.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/ImageZoomInScreen.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/Common/multi_manager/flick_multi_manager.dart';
import 'package:newapp/Common/multi_manager/flick_multi_player.dart';
import 'package:newapp/Common/utils.dart';
import 'package:newapp/controllers/Dashboard/Comments/storyComment.dart';
import 'package:newapp/controllers/Dashboard/Dashboard.dart';
import 'package:newapp/controllers/Story/UpdateStory.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share/share.dart';

class ProfileStroyDetail extends StatefulWidget{
  List<dashboardStoryModel> Array_Stories;
  var index;
  var userName;
  ProfileStroyDetail({this.Array_Stories,this.userName,this.index});
  @override
  State<StatefulWidget> createState() {
    return ProfileStoryDetailState();
  }
}

class ProfileStoryDetailState extends State<ProfileStroyDetail>{

  ItemScrollController _scrollController = ItemScrollController();

  FlickMultiManager flickMultiManager;

  var userId = "";

  var token = "";

  int _current;

  int present = 0;
  int perPage = 1;
  int TotalRecords = 0;

  RefreshController _refreshController;

  bool isLoading = false;

  bool showError = false;

  @override
  void initState() {
    super.initState();
    _refreshController = RefreshController();

    present = present + perPage;

    getFromUserDefault(UserDefault_TOKEN).then((t){
      token = t;
      
    });

    getFromUserDefault(UserDefault_USERID).then((id){
      userId = id;
    });

    
    flickMultiManager = FlickMultiManager();    

    getLocationDistance();
  }

  getLocationDistance() async{
    for(var story in widget.Array_Stories){

          var index = widget.Array_Stories.indexOf(story);

          Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

          var _distanceInMeters =  GeolocatorPlatform.instance.distanceBetween(position.latitude, position.longitude, double.parse(story.lat), double.parse(story.long));

          var distance = (_distanceInMeters/1000);
          var twodisdecimal = distance.toStringAsFixed(2);

          // getDistance(double.parse(story["latitude"]), double.parse(story["longitude"]),i);

          final coordinates = new Coordinates(double.parse(story.lat), double.parse(story.long));
          var addresses = await Geocoder.google("AIzaSyBGqdkh3xkI_YeOPh9nvRoOXhZt6S2LsnU").findAddressesFromCoordinates(coordinates);
          var first = addresses.first;
          var placeName = "${first.featureName} - ${first.subAdminArea}";

          
          setState(() {
            widget.Array_Stories[index].locality = placeName.length>15?placeName.replaceRange(15, placeName.length, "..."):placeName;
            widget.Array_Stories[index].distance = double.parse(twodisdecimal);
          });
        }
  }

  apiLikeStory(int index, int StoryId, bool islike)async{
    String webLink = ApiCallingHelper.baseUrl + "Story/ManageStory";

    var params = {
      "StoryId":StoryId,
      "IsLike":islike,
      "OperationType":1,
      "UserId":int.parse(userId)
    };

    Dio dio = Dio();

    var resp = await dio.post(webLink,data:params,options: Options(headers: {"Authorization": token}));

    

    var data = resp.data["response"];

    if(resp.data["status"]){

      setState(() {
        widget.Array_Stories[index].isLike = islike ? 1 : 0;
        widget.Array_Stories[index].totalLike = islike ? "${int.parse(widget.Array_Stories[index].totalLike) + 1}":widget.Array_Stories[index].totalLike == "0"?"0":"${int.parse(widget.Array_Stories[index].totalLike) - 1}";
      });
      // _onRefresh();
    }

  }

  apiFollowUnfollow(int index, int ToId, bool isFollow)async{
    String webLink = ApiCallingHelper.baseUrl + "UserFollower";

    var params = {
      "UserFollowerFriendId":ToId,
      "CreatedById":int.parse(userId),
      "UserFollowerTypeId":1,
      "UnFollow":!isFollow
    };

    Dio dio = Dio();

    var resp = await dio.post(webLink,data:params,options: Options(headers: {"Authorization": token}));

    

    var data = resp.data["response"];

    if(resp.data["status"]){      
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        leading: new IconButton(icon: Icon(Icons.arrow_back,color:Colors.black), onPressed: (){
          Navigator.of(context).pop();
        }),
        actionsIconTheme: IconThemeData(color:Colors.black),
        backgroundColor: Colors.white,
        title: new Text("Posts",textAlign: TextAlign.center,style: GoogleFonts.roboto(fontSize: 15,color:Colors.black),)
      ),
      body: ScrollablePositionedList.builder(
        itemCount: widget.Array_Stories.length,
        itemScrollController: _scrollController,
        initialScrollIndex: widget.index,
        addAutomaticKeepAlives: true,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.fromLTRB(5, 0, 5, 8),
            child: InkWell(
              onTap: (){
                // Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>StoryDetailScreen()));
              },
              child: Card(
                elevation: 10,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: double.maxFinite,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                // SizedBox(
                                //   width: 10,
                                // ),
                                Row(
                                  children: <Widget>[
                                    Container(
                                      height: 35,
                                      width: 35,
                                      decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          border: Border.all(
                                            width: 1.0,
                                            color: Colors.black,
                                          ),
                                          image: DecorationImage(
                                              image: widget.Array_Stories[index].ProfilePicturePath != null?NetworkImage( widget.Array_Stories[index].ProfilePicturePath):AssetImage("assets/lonvala.jpg"),
                                              fit: BoxFit.fill)),
                                    ),

                                    Padding(
                                  padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        widget.Array_Stories[index].userName == null?"Bruce Wyne":widget.Array_Stories[index].userName,
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w600)
                                      ),
                                      new SizedBox(height:5),
                                      InkWell(
                                        onTap: (){
                                          openMap(double.parse(widget.Array_Stories[index].lat), double.parse(widget.Array_Stories[index].long));
                                        },
                                        child: Row(
                                            children: <Widget>[
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                child: Icon(Icons.location_on,size:15,color: Colors.grey,),
                                              ),
                                              Padding(
                                            padding: const EdgeInsets.fromLTRB(2, 0, 2, 0),
                                            child: Text("${widget.Array_Stories[index].locality}",
                                                style: GoogleFonts.roboto(fontSize: 11,
                                                    fontWeight: FontWeight.w500,color: Colors.grey)),
                                          ),
                                           Padding(
                                           padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                                           child: Text("${widget.Array_Stories[index].distance}km",style:GoogleFonts.roboto(fontSize:11,color: Colors.grey),),
                                         )
                                            ],
                                          ),
                                      ),
                                    ],
                                  ),
                                ),
                                  ],
                                ),
                                // SizedBox(
                                //   width:80,
                                // ),
                                
                                widget.Array_Stories[index].createdById == userId? new SizedBox(width: 0,) : InkWell(
                                     onTap: (){
                                       apiFollowUnfollow(index, int.parse(widget.Array_Stories[index].createdById), widget.Array_Stories[index].isFollow == 0?true:false);
                                     },
                                     child: Container(
                                       height: 30,
                                       decoration: BoxDecoration(
                                         border: Border.all(
                                           width: 1,
                                           color: AppDefault_Green
                                         ),
                                         borderRadius: BorderRadius.circular(50)
                                       ),
                                       child: Padding(
                                         padding: const EdgeInsets.all(5.0),
                                         child: Center(child: widget.Array_Stories[index].isFollow != 0?Text("Following", style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green,)):Text("Follow", style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green,))),
                                       )),
                                   )
                               
                              ],
                            ),
                           
                          ),
                          
                          Divider(
                            height: 2,
                          ),
                          
                          
                        
                        ],
                      ),
                    ),
                    Stack(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                          child: Stack(
                            children: <Widget>[
                              Container(
                                height: 260,
                                width: double.maxFinite,
                                child: CarouselSlider.builder(
                                  
                              itemCount: widget.Array_Stories[index].storyMedias.length>0
                                  ?widget.Array_Stories[index].storyMedias.length
                                  :2,
                              itemBuilder: (BuildContext context, int itemIndex) =>
                                  InkWell(
                                    onTap: (){
                                      Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>ImageZoomIn(current: itemIndex,imageList: widget.Array_Stories[index].storyMedias,name: widget.Array_Stories[index].title,)));
                                    },
                                    child: Column(
                                      children: [
                                        Flexible(
                                          child: Container(
                                            width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.symmetric(horizontal: 2.0),
                                              // margin: EdgeInsets.only(left:  ScreenConfig().screenAwareWidth(10, context),
                                              //     right:  ScreenConfig().screenAwareWidth(10, context),
                                              //     bottom:  ScreenConfig().screenAwareWidth(3, context)),
                                              decoration: new BoxDecoration(
                                                // color: Colors.white,
                                                borderRadius: BorderRadius.circular( screenAwareWidth(5, context)),
                                              ),
                                              child: Container(
                                                height: 260,
                                                  width: double.maxFinite,
                                                  child: ClipRRect(
                                                      borderRadius: BorderRadius.circular(5),
                                                     child : widget.Array_Stories[index].storyMedias[itemIndex]["FileTypeId"] == 6?CachedNetworkImage(imageUrl:widget.Array_Stories[index].storyMedias[itemIndex]["FilePath"],fit: BoxFit.contain,progressIndicatorBuilder: (context, url, downloadProgress) => 
                                                      Center(child: CircularProgressIndicator(value: downloadProgress.progress)),):
                                                        FlickMultiPlayer(
                                                    url: widget.Array_Stories[index].storyMedias[itemIndex]["FilePath"],
                                                    flickMultiManager: flickMultiManager,
                                                    image: "assets/PikNikTransparent.png",
                                                  )
                                                  )
                                              )
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                              options: CarouselOptions(
                                height: 260,
                                initialPage: 0,
                                scrollPhysics: BouncingScrollPhysics(),
                                viewportFraction: 1.0,
                                enableInfiniteScroll: true,
                                // reverse: false,
                                enlargeCenterPage: false,
                                // autoPlay: true,
                                autoPlayInterval: Duration(seconds: 10),
                                autoPlayAnimationDuration: Duration(days: 11111),
                                // autoPlayCurve: Curves.fastOutSlowIn,
                                // pauseAutoPlayOnTouch: true,
                                onPageChanged: (index,reason) {
                                  setState(() {
                                    _current = index;
                                  });
                                },
                                scrollDirection: Axis.horizontal,
                              ),
                            ),
                              ),
                             
                            ],
                          ),
                        ),
                        Positioned(
                          bottom: 15,
                          left: 0,right: 0,
                          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: widget.Array_Stories[index].storyMedias.map((url) {
              int index1 = widget.Array_Stories[index].storyMedias.indexOf(url);
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index1
                    ? Colors.white
                    : Colors.grey
                ),
              );
            }).toList(),
          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(0),
                          child: Row(
                            children: <Widget>[
                              IconButton(
                                onPressed: (){
                                  apiLikeStory(index, int.parse(widget.Array_Stories[index].StoryId), widget.Array_Stories[index].isLike ==0?true:false);
                                },
                                icon: widget.Array_Stories[index].isLike ==0?ImageIcon(
                                  AssetImage("assets/NotLike.png"),
                                  size: 20,
                                  color: AppDefault_Green,
                                ):ImageIcon(
                                  AssetImage("assets/Like.png"),
                                  color: AppDefault_Green,
                                  size: 20,
                                ),
                              ),
                              Row(
                                children: [
                                  Transform.translate(
                                    offset: Offset(-11, 0),
                                    child: IconButton(
                                      onPressed: (){
                                        Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>StoryComment(storyId: widget.Array_Stories[index].StoryId,)));
                                      },
                                      icon: ImageIcon(
                                      AssetImage("assets/chat.png"),
                                      color: AppDefault_Green,
                                      size: 20,
                                    ),
                                    ),
                                  ),
                                  Transform.translate(
                                    offset: Offset(-22, 0),
                                    child: IconButton(
                                      padding: EdgeInsets.all(0),
                                      onPressed: (){
                                        Share.share('check out my story\n\nhttps://example.com\n\nSpot Information: ${widget.Array_Stories[index].spotInformation}\n\nHow to get there: ${widget.Array_Stories[index].metaDesc}');
                                      },
                                      icon: ImageIcon(
                                        AssetImage("assets/share.png"),
                                        color: AppDefault_Green,
                                        size: 20,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.only(right:15.0),
                          child: Row(
                      children: <Widget>[
                          SizedBox(
                            width: 10,
                          ),
                          Text("${widget.Array_Stories[index].totalView} views,"),
                          SizedBox(
                            width: 5,
                          ),
                          Text("${widget.Array_Stories[index].totalLike} Likes")
                      ],
                    ),
                        ),
                      ],
                    ),
                    new SizedBox(height: 10,),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(10,0,10,0),
                      child: Container(
                        width: double.maxFinite,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  widget.Array_Stories[index].isMore?new Container():new RichText(text: TextSpan(
                                    style: GoogleFonts.roboto(fontSize: 14.0,
                                    color: Colors.black,),
                                    
                                    children: [
                                      new TextSpan(
                                        
                                        text: widget.Array_Stories[index].userName,
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.bold,decoration: TextDecoration.underline)
                                      ),
                                      new TextSpan(
                                        text: "  ",
                                        style: GoogleFonts.roboto()
                                      ),

                                      new TextSpan(
                                        text: widget.Array_Stories[index].title,
                                        style: GoogleFonts.roboto(
                                      fontWeight: FontWeight.w500)
                                      ),

                                      new TextSpan(
                                        text: "  ",
                                        style: GoogleFonts.roboto()
                                      ),

                                      new TextSpan(
                                        text: "\n",
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500)
                                      ),
                                      
                                      new TextSpan(
                                        text: "Spot Info: ",
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green),
                                      ),
                                      new TextSpan(
                                        text: widget.Array_Stories[index].spotInformation.length>19? widget.Array_Stories[index].spotInformation.replaceRange(20, widget.Array_Stories[index].spotInformation.length, "...."):widget.Array_Stories[index].spotInformation,
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w400),
                                      )
                                    ]
                                  )),
                                  widget.Array_Stories[index].isMore?new RichText(text: TextSpan(
                                    style: GoogleFonts.roboto(fontSize: 14.0,
                                    color: Colors.black,),
                                    children: [

                                      new TextSpan(
                                        
                                        text: widget.Array_Stories[index].userName,
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.bold,decoration: TextDecoration.underline)
                                      ),
                                      new TextSpan(
                                        text: "  ",
                                        style: GoogleFonts.roboto()
                                      ),
                                      new TextSpan(
                                        text: "Spot Info: ",
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green)
                                      ),
                                      new TextSpan(
                                        text: widget.Array_Stories[index].spotInformation,
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w400),
                                      ),

                                      new TextSpan(
                                        text: "\n",
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500)
                                      ),
                                      new TextSpan(
                                        text: "\n",
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500)
                                      ),
                                      
                                      new TextSpan(
                                        text: "Get there by ",
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green)
                                      ),
                                      new TextSpan(
                                        text: widget.Array_Stories[index].metaDesc,
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w400)
                                      ),
                                      new TextSpan(
                                        text: "\n",
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500)
                                      ),
                                      new TextSpan(
                                        text: "\n",
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500)
                                      ),
                                      new TextSpan(
                                        text: "Tags: ",
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green)
                                      ),
                                      new TextSpan(
                                        text: widget.Array_Stories[index].tags,
                                        style: GoogleFonts.roboto(fontWeight: FontWeight.w400)
                                      )
                                    ]
                                  )):new Container(),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 20),
                              child: InkWell(
                                onTap: (){
                                  setState(() {
                                    widget.Array_Stories[index].isMore = !widget.Array_Stories[index].isMore;
                                  });
                                },
                                child: new Text(widget.Array_Stories[index].isMore?"Less":"More",style: GoogleFonts.roboto(fontSize: 14.0,
                                  color: Colors.black,fontWeight: FontWeight.bold),),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                     Padding(
                              padding: const EdgeInsets.fromLTRB(10, 15, 10, 5),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    formattedDate("dd-MM-yyyy HH:mm:ss", "dd MMM @ h:mm a", widget.Array_Stories[index].date),
                                    style: TextStyle(fontSize: 12),
                                  ),

                                   widget.Array_Stories[index].createdById == userId? InkWell(
                                    onTap: ()async{
                                      Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

     
                                      Navigator.of(context).push(
                                          CupertinoPageRoute(builder: (context) => UpdateStory(lat: position.latitude,long: position.longitude,storyDetails: widget.Array_Stories[index], )));
                                    },
                                    child: Text(
                                      "Edit Story",
                                      style: TextStyle(fontSize: 15,color:Colors.blue),
                                    ),
                                  ): new Container(),
                                ],
                              )),
                    Divider(
                      height: 1,
                      // color: Colors.black,
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
  
}