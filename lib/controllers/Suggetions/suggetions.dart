import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Bloc/refreshProfileImage.dart';
import 'package:newapp/Bloc/viewStoryBloc.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/ProgressBar.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/Common/utils.dart';
import 'package:newapp/controllers/Dashboard/Dashboard.dart';
import 'package:newapp/controllers/Profile/editprofile.dart';
import 'package:newapp/controllers/Suggetions/FollowerList/followFollowing.dart';
import 'package:newapp/controllers/Suggetions/profileStoryDetails.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:share/share.dart';
import 'package:shimmer/shimmer.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class SuggetionScreen extends StatefulWidget{

  var userId;
  SuggetionScreen({this.userId});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SuggetionScreenState();
  }
}

class SuggetionScreenState extends State<SuggetionScreen>{

  var segmentValue = 0;

  List<dashboardStoryModel> Array_Stories = [];
  List<dashboardStoryModel> Array_LikeStories = [];
  var token = "";

  var userId = "";

  bool isLoading = false;

  bool showError = false;

   ScrollController _scrollController = ScrollController();
  int present = 0;
  int perPage = 1;
  int TotalRecords = 0;
  RefreshController _refreshController;

  bool isDataLoaded = false;
  var loginUserId = "";
  
  @override
  void initState() {
    super.initState();

    _refreshController = RefreshController();

    present = present + perPage;

    getFromUserDefault(UserDefault_TOKEN).then((t){
      token = t;
      
    });

    

    getFromUserDefault(UserDefault_USERID).then((id){
      if(widget.userId != null){
        userId = widget.userId;
      }
      else{
        userId = id;
      }
      loginUserId = id;
      apiGetProfile(context);
      apiDashboard(context,present);
    });

    refreshProfileBloc.refreshProfileState.listen((event) {
      apiGetProfile(context);
      _onRefresh();
    });

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        int mainPage = (TotalRecords / 100000000).ceil();
        if (present != mainPage) {
          present = present + perPage;
          apiDashboard(context,present);
        } else {
          setState(() {
            isLoading = false;
          });
        }
      }
    });
  }

  var userProfile;
  var placeName = "";

  apiGetProfile(BuildContext context) async{
    String webLink = ApiCallingHelper.baseUrl + 'user?orderByColumn=&sortOrder=&pageIndex=1&rowCount=15&searchQuery={"loginUserId":$loginUserId}&userId=$userId&roleId=0';

    Dio dio = new Dio();

    var resp = await dio.get(webLink, options: Options(headers: {"Authorization": token}));

    var data = resp.data;

    if(data["status"]){
      if((jsonDecode(data["response"]) as List).isNotEmpty){
          userProfile = jsonDecode(data["response"])[0];

          final coordinates = new Coordinates(double.parse(userProfile["latitude"]), double.parse(userProfile["longitude"]));
          var addresses = await Geocoder.google("AIzaSyBGqdkh3xkI_YeOPh9nvRoOXhZt6S2LsnU").findAddressesFromCoordinates(coordinates);
          var first = addresses.first;
          placeName = "${first.subLocality}, ${first.locality}, ${first.adminArea}";

      setState(() {
        
      });
        }
      
    }
  }

  apiDashboard(BuildContext context, int pageNumber) async{
    String webLink = ApiCallingHelper.baseUrl + 'Story?userId=$userId&searchQuery={"loginUserId":$userId,"searchFreeText":""}&orderByColumn&sortOrder&pageIndex=$pageNumber&rowCount=100000000&storyId=0';

    setState(() {
      isLoading = true;
    });

    ApiManager().callGetApi(context, webLink, false,token).then((data)async{
      var apiData = data;

      if(apiData["status"]){

        for(var story in jsonDecode(apiData["response"])){

        TotalRecords = story["totalCount"];

        var path = "";

        if(story["storyMedias"][0]["FileTypeId"] == 7){
          path  = await getImageThumbnail(story["storyMedias"][0]["FilePath"]);
        }

        

        setState((){
          Array_Stories.add(dashboardStoryModel(
            ProfilePicturePath: story["profilePicturePath"],
            StoryId: story["storyId"].toString(),
            createdById: story["createdById"].toString(),
            date: story["createdByDate"].toString(),
            lat: story["latitude"].toString(),
            long: story["longitude"].toString(),
            spotInformation: story["spotInformation"].toString(),
            storyMedias: story["storyMedias"],
            storyTags: story["storyTags"],
            title: story["title"],
            totalLike: story["totalLike"].toString(),
            totalShare: story["totalShare"].toString(),
            totalView: story["totalView"].toString(),
            userName: story["userName"],
            isLike: story["isLike"],
            locality: "",
            distance: 0.0,
            metaDesc: story["metaDescription"],
            isMore: false,
            ArrayTags: story["storyTags"],
            locationName: story["locationName"],
            postalCode: story["postalCode"],
            thumbnailPathForProfile: path,
            isFollow: story["isFollow"]
          ));
        });
        }
        



        if (Array_Stories.isEmpty) {
          setState(() {
            showError = true;
          });
        } else {
          setState(() {
            showError = false;
          });
        }

        if (TotalRecords == Array_Stories.length) {
          setState(() {
            isLoading = false;
          });
        }

        setState(() {
          isDataLoaded = true;
        });
      }
    });
  }

  apiLikeDashboard(BuildContext context, int pageNumber) async{
    String webLink = ApiCallingHelper.baseUrl + 'Story/GetLikeUserStory?userId=$userId&pageIndex=1&rowCount=100000000';

    setState(() {
      isLoading = true;
    });

    ApiManager().callGetApi(context, webLink, false,token).then((data)async{
      var apiData = data;

      if(apiData["status"]){

        for(var story in jsonDecode(apiData["response"])){

        TotalRecords = story["totalCount"];

        var path = "";

        if(story["storyMedias"][0]["FileTypeId"] == 7){
          path  = await getImageThumbnail(story["storyMedias"][0]["FilePath"]);
        }

        

        setState((){
          Array_LikeStories.add(dashboardStoryModel(
            ProfilePicturePath: story["profilePicturePath"],
            StoryId: story["storyId"].toString(),
            createdById: story["createdById"].toString(),
            date: story["createdByDate"].toString(),
            lat: story["latitude"].toString(),
            long: story["longitude"].toString(),
            spotInformation: story["spotInformation"].toString(),
            storyMedias: story["storyMedias"],
            storyTags: story["storyTags"],
            title: story["title"],
            totalLike: story["totalLike"].toString(),
            totalShare: story["totalShare"].toString(),
            totalView: story["totalView"].toString(),
            userName: story["userName"],
            isLike: story["isLike"],
            locality: "",
            distance: 0.0,
            metaDesc: story["metaDescription"],
            isMore: false,
            ArrayTags: story["storyTags"],
            locationName: story["locationName"],
            postalCode: story["postalCode"],
            thumbnailPathForProfile: path,
            isFollow: story["isFollow"]
          ));
        });
        }
        



        if (Array_LikeStories.isEmpty) {
          setState(() {
            showError = true;
          });
        } else {
          setState(() {
            showError = false;
          });
        }

        if (TotalRecords == Array_LikeStories.length) {
          setState(() {
            isLoading = false;
          });
        }

        setState(() {
          isDataLoaded = true;
        });
      }
    });
  }

  

  void _onRefresh() {

    setState(() {
          isDataLoaded = false;
        });
    
    setState(() {
        Array_Stories.clear();
        Array_LikeStories.clear();
      });
    new Future.delayed(const Duration(milliseconds: 100)).then((val) {
      present = 0;
      perPage = 1;
      isLoading = false;
      present = 1;
      apiDashboard(context,present);
      apiLikeDashboard(context, present);
      
      _refreshController.refreshCompleted();
    });
  }

  getImageThumbnail(url)async{
    final uint8list = await VideoThumbnail.thumbnailFile(
      video: url,
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.PNG,
      maxHeight: 256, // specify the height of the thumbnail, let the width auto-scaled to keep the source aspect ratio
      quality: 25,
    );
    print(uint8list);
    return uint8list;
  }

  apiFollowUnfollow(int index, int ToId, bool isFollow)async{
    String webLink = ApiCallingHelper.baseUrl + "UserFollower";

    var params = {
      "UserFollowerFriendId":ToId,
      "CreatedById":int.parse(loginUserId),
      "UserFollowerTypeId":1,
      "UnFollow":isFollow
    };

    Dio dio = Dio();

    var resp = await dio.post(webLink,data:params,options: Options(headers: {"Authorization": token}));

    

    var data = resp.data["response"];

    if(resp.data["status"]){
      apiGetProfile(context);
      followBloc.changeFollowBlocState(resp.data["response"]);
      setState(() {
        
      });
      // _onRefresh();
    }

  }

  apiLikeStory(int index, int StoryId, bool islike)async{
    String webLink = ApiCallingHelper.baseUrl + "Story/ManageStory";

    var params = {
      "StoryId":StoryId,
      "IsLike":islike,
      "OperationType":1,
      "UserId":int.parse(userId)
    };

    Dio dio = Dio();

    var resp = await dio.post(webLink,data:params,options: Options(headers: {"Authorization": token}));

    

    var data = resp.data["response"];

    if(resp.data["status"]){

      for(var story in Array_Stories){
          if(story.StoryId == resp.data["response"]["storyId"].toString()){
              story.isLike = resp.data["response"]["isLike"]?1:0; 
            }
        }
      setState(() {
        
      });

      // setState(() {
      //   Array_Stories[index].isLike = islike ? 1 : 0;
      //   Array_Stories[index].totalLike = islike ? "${int.parse(Array_Stories[index].totalLike) + 1}":Array_Stories[index].totalLike == "0"?"0":"${int.parse(Array_Stories[index].totalLike) - 1}";
      // });
      // _onRefresh();
    }

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Profile",style: GoogleFonts.roboto(color: AppDefault_Green),),
        leading: InkWell(
          onTap: (){
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back,color: Colors.black,)),
        actions: <Widget>[
           widget.userId != null? widget.userId != loginUserId? new SizedBox(width: 0,):InkWell(
            onTap: ()async{
              Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
              Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>EditProfile(lat: position.latitude,long: position.longitude,screen:"Profile")));
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(Icons.edit,color:AppDefault_Green),
            ),
          ):InkWell(
            onTap: ()async{
              Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
              Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>EditProfile(lat: position.latitude,long: position.longitude,screen:"Profile")));
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(Icons.edit,color:AppDefault_Green),
            ),
          ),
          
        ],
      ),
     body: Container(
       width: double.maxFinite,
       height: double.maxFinite,
       child: SmartRefresher(
                  header: ClassicHeader(),
                  controller: _refreshController,
                  enablePullDown: true,
                  enablePullUp: false,
                  onRefresh: _onRefresh,
                  child:SingleChildScrollView(
         controller: _scrollController,
         physics: BouncingScrollPhysics(),
         child: Column(
           crossAxisAlignment: CrossAxisAlignment.center,
           children: <Widget>[
             SizedBox(height:10),
                  userProfile == null?  ProfileLoaded() : new Column(children: [
                    Container(
                  width: 120,
                  height: 120,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(width: 1),
                      image: DecorationImage(
                          image: userProfile["profilePicturePath"] != null?NetworkImage( userProfile["profilePicturePath"]): AssetImage("assets/AppLogo/PikNikTransparent.png"),
                          fit: BoxFit.fill)),
                ),
                SizedBox(height:5),
                Text(userProfile["userName"] != null?userProfile["userName"]: "",style: GoogleFonts.roboto(fontWeight: FontWeight.w600,fontSize: 16)),
                 SizedBox(height:5),
                 Text(userProfile["bio"] != null?userProfile["bio"]: "",style: GoogleFonts.roboto(),),
                  SizedBox(height:2),
                  Text(placeName,style: GoogleFonts.roboto(color: Colors.grey[500]),),
                  SizedBox(height:5),
                  Container(
                    width: double.maxFinite,
                    height: 50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        
                         InkWell(
                           onTap: (){
                             Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>FollowFollowing(segmentValue: 0,userId: userId,)));
                           },
                           child: Container(
                            child:Row(children: <Widget>[
                              Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: AppDefault_Green
                              ),
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(5, 3, 5, 3),
                                  child: Text(userProfile["totalFollowers"].toString(),style: GoogleFonts.roboto(color: Colors.white),),
                                )),
                                SizedBox(width:3),
                              Container(
                                decoration: BoxDecoration(
                                   borderRadius: BorderRadius.circular(10),
                                  color: Colors.grey[200]
                                ),
                                child:Padding(
                                   padding: const EdgeInsets.fromLTRB(5, 4, 5, 4),
                                  child: Text("Followers",style: GoogleFonts.roboto(),),
                                ))
                            ],)
                        ),
                         ),
                         SizedBox(width:20),
                        InkWell(
                    onTap: (){
                      Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>FollowFollowing(segmentValue: 1,userId: userId,)));
                    },
                          child: Container(
                            child:Row(children: <Widget>[
                              Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: AppDefault_Green
                              ),
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(5, 3, 5, 3),
                                  child: Text(userProfile["totalFollowings"].toString(),style: GoogleFonts.roboto(color: Colors.white),),
                                )),
                                SizedBox(width:3),
                              Container(
                                decoration: BoxDecoration(
                                   borderRadius: BorderRadius.circular(10),
                                  color: Colors.grey[200]
                                ),
                                child:Padding(
                                   padding: const EdgeInsets.fromLTRB(5, 4, 5, 4),
                                  child: Text("Following",style: GoogleFonts.roboto(),),
                                ))
                            ],)
                          ),
                        ),
                        
                      ],
                    ),
                  ),
                  ],),
                  new SizedBox(height: 10),
                  widget.userId != null? widget.userId == loginUserId ? new SizedBox(width: 0,) :InkWell(
                                     onTap: (){
                                       apiFollowUnfollow(0, userProfile["userId"], userProfile["isFollow"] == 0?false:true);
                                     },
                                     child: Container(
                                       height: 30,
                                       width: 150,
                                       decoration: BoxDecoration(
                                         border: Border.all(
                                           width: 1,
                                           color: AppDefault_Green
                                         ),
                                         borderRadius: BorderRadius.circular(50)
                                       ),
                                       child: Padding(
                                         padding: const EdgeInsets.all(5.0),
                                         child: userProfile == null? new SizedBox(height: 0,) : Center(child: userProfile["isFollow"] != 0?Text("Following", style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green,)):Text("Follow", style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green,))),
                                       )),
                                   ) : new SizedBox(width: 0,),
                  userId != loginUserId? new SizedBox(width: 0,): Container(
                    width: 250,
                    child: new CupertinoSlidingSegmentedControl(children: 
                    {
                      0: new Text("Posted (${Array_Stories.length})", style:GoogleFonts.roboto()),
                      1: new Text("Liked (${Array_LikeStories.length})", style:GoogleFonts.roboto()),
                    }, 
                    groupValue: segmentValue,
                    onValueChanged: (value){
                      setState(() {
                        segmentValue = value;
                      });
                      _onRefresh();
                    }),
                  ),
                  new SizedBox(height: 10),
                  isDataLoaded?segmentValue == 0?catalinagridList(context): LikedStroygridList(context):DataLoded(context)
           ],
         ),
       )),
     ),
    );
  }
  catalinagridList(BuildContext context){
    return  GridView.builder(
      itemCount: Array_Stories.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 2,
        childAspectRatio: 1,
        mainAxisSpacing: 2,
      ),
      itemBuilder: (context,index){
        return Container(
                       child: InkWell(
                         onTap: (){
                           Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>ProfileStroyDetail(Array_Stories: Array_Stories,userName: userProfile["userName"],index: index,)));
                         },
                         child: Card(
                           elevation: 10,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Stack(
                                  children: <Widget>[
                                    Container(
                                      width:double.maxFinite,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(image: Array_Stories[index].storyMedias[0]["FileTypeId"] == 6 ? NetworkImage(Array_Stories[index].storyMedias[0]["FilePath"]): FileImage(File(Array_Stories[index].thumbnailPathForProfile)),fit: BoxFit.fitWidth)
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children: <Widget>[
                                            InkWell(onTap: (){
                                              Share.share('check out my story\n\nhttps://example.com\n\nSpot Information: ${Array_Stories[index].spotInformation}\n\nHow to get there: ${Array_Stories[index].metaDesc}');
                                            },child: Icon(Icons.share,color: Colors.white,)),
                                            SizedBox(height:30),
                                            InkWell(onTap: (){
                                              apiLikeStory(index, int.parse(Array_Stories[index].StoryId), Array_Stories[index].isLike ==0?true:false);
                                            },child: Array_Stories[index].isLike ==0?ImageIcon(
                                    AssetImage("assets/NotLike.png"),
                                    size: 20,
                                    color: Colors.white,
                                  ):ImageIcon(
                                    AssetImage("assets/Like.png"),
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                 SizedBox(height:1),
                                new Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("${Array_Stories[index].totalView} Views",style: GoogleFonts.roboto(),),
                                      SizedBox(height:3),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                       ),
                      );
      },
                    );
  }


  LikedStroygridList(BuildContext context){
    return  GridView.builder(
      itemCount: Array_LikeStories.length,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 2,
        childAspectRatio: 1,
        mainAxisSpacing: 2,
      ),
      itemBuilder: (context,index){
        return Container(
                       child: InkWell(
                         onTap: (){
                           Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>ProfileStroyDetail(Array_Stories: Array_LikeStories,userName: userProfile["userName"],index: index,)));
                         },
                         child: Card(
                           elevation: 10,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                        width:double.maxFinite,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(image: Array_LikeStories[index].storyMedias[0]["FileTypeId"] == 6 ? NetworkImage(Array_LikeStories[index].storyMedias[0]["FilePath"]): FileImage(File(Array_LikeStories[index].thumbnailPathForProfile)),fit: BoxFit.fitWidth)
                                        ),
                                        
                                      ),
                                ),
                                new Padding(
                                  padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("${Array_LikeStories[index].totalLike} Likes",style: GoogleFonts.roboto(),),
                                      SizedBox(height:3),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                       ),
                      );
      },
                    );
  }

  Widget DataLoded(BuildContext context){
    return ListView(
      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          ListView.builder(
              itemCount: 10,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context,int mainIndex){
                return Shimmer.fromColors(
                  baseColor: Colors.white,
                  highlightColor: Colors.grey[200],
                  enabled: true,
                  child: Column(
                    children: <Widget>[
                      new Row(
                        children: [
                          new Expanded(child: new Container(height: 150,color: Colors.white),),
                          new SizedBox(width: 10,),
                          new Expanded(child: new Container(height: 150,color: Colors.white),),
                          new SizedBox(width: 10,),
                          new Expanded(child: new Container(height: 150,color: Colors.white),)
                        ],
                      ),  
                      new SizedBox(height: 10,),                    
                    ],
                  ),
                );
              }
              )
        ]
    );
  }

  Widget ProfileLoaded(){
    return new ListView(shrinkWrap: true,physics: NeverScrollableScrollPhysics(),children: [
                    Shimmer.fromColors(
                  baseColor: Colors.white,
                  highlightColor: Colors.grey[200],
                  enabled: true,
                  child:Container(
                    width: double.maxFinite,
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            height: 120,
                            width: 120,
                            child: new CircleAvatar(

                            ),
                          ),
                          SizedBox(height:10),

                          new Container(height: 20,color: Colors.white,width: 200),
                          SizedBox(height:10),
                          new Container(height: 20,color: Colors.white,width: 300),
                          Container(
                    width: double.maxFinite,
                    height: 50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child:Row(children: <Widget>[
                            Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: AppDefault_Green
                            ),
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 3, 5, 3),
                                child: Text("10",style: GoogleFonts.roboto(color: Colors.white),),
                              )),
                              SizedBox(width:3),
                            Container(
                              decoration: BoxDecoration(
                                 borderRadius: BorderRadius.circular(10),
                                color: Colors.grey[200]
                              ),
                              child:Padding(
                                 padding: const EdgeInsets.fromLTRB(5, 4, 5, 4),
                                child: Text("Following",style: GoogleFonts.roboto(),),
                              ))
                          ],)
                        ),
                        SizedBox(width:20),
                         Container(
                          child:Row(children: <Widget>[
                            Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: AppDefault_Green
                            ),
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 3, 5, 3),
                                child: Text("35",style: GoogleFonts.roboto(color: Colors.white),),
                              )),
                              SizedBox(width:3),
                            Container(
                              decoration: BoxDecoration(
                                 borderRadius: BorderRadius.circular(10),
                                color: Colors.grey[200]
                              ),
                              child:Padding(
                                 padding: const EdgeInsets.fromLTRB(5, 4, 5, 4),
                                child: Text("Followers",style: GoogleFonts.roboto(),),
                              ))
                          ],)
                        )
                      ],
                    ),
                  ),
                        ],
                      ),
                  ))
                  ],);
  }


  gridList(BuildContext context){
    return  GridView.count(
                    crossAxisCount: 2,
                    padding: EdgeInsets.all(20),
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    children: <Widget>[
                     Container(
                       child: Card(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Stack(
                                children: <Widget>[
                                  Container(
                                    height: 100,
                                    width:double.maxFinite,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(image: AssetImage("assets/lonvala.jpg"),fit: BoxFit.fill)
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Icon(Icons.share),
                                          SizedBox(height:30),
                                          ImageIcon(AssetImage("assets/heart.png"))
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                               SizedBox(height:1),
                              Text("217 Views"),
                              SizedBox(height:3),
                              Text("This is demo story")
                            ],
                          ),
                        ),
                      ),
                      Container(
                       child: Card(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Stack(
                                children: <Widget>[
                                  Container(
                                    height: 100,
                                    width:double.maxFinite,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(image: AssetImage("assets/lonvala.jpg"),fit: BoxFit.fill)
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Icon(Icons.share),
                                          SizedBox(height:30),
                                          ImageIcon(AssetImage("assets/heart.png"))
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                               SizedBox(height:1),
                              Text("217 Views"),
                              SizedBox(height:3),
                              Text("This is demo story")
                            ],
                          ),
                        ),
                      ),
                       Container(
                       child: Card(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Stack(
                                children: <Widget>[
                                  Container(
                                    height: 100,
                                    width:double.maxFinite,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(image: AssetImage("assets/lonvala.jpg"),fit: BoxFit.fill)
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Icon(Icons.share),
                                          SizedBox(height:30),
                                          ImageIcon(AssetImage("assets/heart.png"))
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                               SizedBox(height:1),
                              Text("217 Views"),
                              SizedBox(height:3),
                              Text("This is demo story")
                            ],
                          ),
                        ),
                      ),
                        Container(
                       child: Card(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Stack(
                                children: <Widget>[
                                  Container(
                                    height: 100,
                                    width:double.maxFinite,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(image: AssetImage("assets/lonvala.jpg"),fit: BoxFit.fill)
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Icon(Icons.share),
                                          SizedBox(height:30),
                                          ImageIcon(AssetImage("assets/heart.png"))
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                               SizedBox(height:1),
                              Text("217 Views"),
                              SizedBox(height:3),
                              Text("This is demo story")
                            ],
                          ),
                        ),
                      ),
                    ],
                    );
  }
}