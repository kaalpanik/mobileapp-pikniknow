import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:logging/logging.dart';
import 'package:newapp/Bloc/refreshProfileImage.dart';
import 'package:newapp/Bloc/viewStoryBloc.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/ImageZoomInScreen.dart';
import 'package:newapp/Common/ProgressBar.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/Common/multi_manager/flick_multi_manager.dart';
import 'package:newapp/Common/multi_manager/flick_multi_player.dart';
import 'package:newapp/Common/utils.dart';
import 'package:newapp/controllers/AboutUs/aboutUs.dart';
import 'package:newapp/controllers/ContactUs/contact.dart';
import 'package:newapp/controllers/Dashboard/Comments/storyComment.dart';
import 'package:newapp/controllers/Dashboard/storyLikeUserList.dart';
import 'package:newapp/controllers/Dashboard/storyViewUserList.dart';
import 'package:newapp/controllers/Notifications/notificationList.dart';
import 'package:newapp/controllers/Settings/settings.dart';
import 'package:newapp/controllers/Story/AddStoryScreen.dart';
import 'package:newapp/controllers/Story/UpdateStory.dart';
import 'package:newapp/controllers/Suggetions/suggetions.dart';
import 'package:newapp/controllers/Terms_Condition/terms.dart';
import 'package:newapp/controllers/auth/LoginScreen.dart';
import 'package:newapp/main.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:share/share.dart';
import 'package:shimmer/shimmer.dart';
import 'package:signalr_client/signalr_client.dart';
import 'package:visibility_detector/visibility_detector.dart';

class Dashboard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return DashboardState();
  }
}

class DashboardState extends State<Dashboard>  with RouteAware {

  Position position ;

   @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
  }

  bool isSearch = false;


  

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var token = "";

  ScrollController _scrollController = ScrollController();
  int present = 0;
  int perPage = 1;
  int TotalRecords = 0;
  RefreshController _refreshController;

  TextEditingController searchTextController = TextEditingController();

  bool isLoading = false;

  bool showError = false;
  int _current;

  List<dashboardStoryModel> Array_Stories = [];



  FlickMultiManager flickMultiManager;

  var userId = "";

  bool searchedData = false;
  int _debouncetime = 500;
  Timer _debounce;

  bool isDataLoaded = false;

  var ProfilePic = "";
  var LocationPath = "";


  final hubProtLogger = Logger("SignalR - hub");
final transportProtLogger = Logger("SignalR - transport");

var serverUrl;
final connectionOptions = HttpConnectionOptions;
var httpOptions;

HubConnection hubConnection;


getLocation() async{
  position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
}

apiGetProfile(BuildContext context) async{
    String webLink = ApiCallingHelper.baseUrl + 'user?orderByColumn=&sortOrder=&pageIndex=1&rowCount=15&searchQuery={"loginUserId":$userId}&userId=$userId&roleId=0';

    Dio dio = new Dio();

    var resp = await dio.get(webLink, options: Options(headers: {"Authorization": token}));

    var data = resp.data;
    var userProfile;

    if(data["status"]){
      setState(() {
        userProfile = jsonDecode(data["response"])[0];
      });


      final coordinates = new Coordinates(double.parse(userProfile["latitude"]), double.parse(userProfile["longitude"]));
          var addresses = await Geocoder.google("AIzaSyBGqdkh3xkI_YeOPh9nvRoOXhZt6S2LsnU").findAddressesFromCoordinates(coordinates);
          var first = addresses.first;
          "${first.subLocality}, ${first.locality}, ${first.adminArea}";

          setIntoDefault(UserDefault_Location, "${first.locality}, ${first.adminArea}");
          setIntoDefault(UserDefault_Profile,  userProfile["profilePicturePath"]);

      setState(() {
        
      });
    }
  }


  @override
  void initState() {
    searchTextController.addListener(_onSearchChanged);
    super.initState();
    getLocation();

    refreshProfileBloc.refreshProfileState.listen((event) {
      setState(() {
        ProfilePic = event["profilePath"];
        LocationPath = event["location"];
      });
      setIntoDefault(UserDefault_Profile, ProfilePic);
      setIntoDefault(UserDefault_Location, LocationPath);
      setState(() {
        
      });
    });

    getFromUserDefault(UserDefault_Profile).then((value){
      setState(() {
        ProfilePic = value;
      });
      
    });

    getFromUserDefault(UserDefault_Location).then((value){
      setState(() {
        LocationPath = value;
      });
      
    });

    _refreshController = RefreshController();

    present = present + perPage;

    getFromUserDefault(UserDefault_TOKEN).then((t){
      token = t;
      
    });

    getFromUserDefault(UserDefault_USERID).then((id){
      userId = id;
      apiGetBanner(context);

      apiGetProfile(context);
      
    });

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        int mainPage = (TotalRecords / 10).ceil();
        if (present != mainPage) {
          present = present + perPage;
          apiDashboard(context,present);
        } else {
          setState(() {
            isLoading = false;
          });
        }
      }
    });
    flickMultiManager = FlickMultiManager();   


    Logger.root.level = Level.ALL;
    // Writes the log messages to the console
    Logger.root.onRecord.listen((LogRecord rec) {
    });

    serverUrl = "http://travelappapi-dev.ap-south-1.elasticbeanstalk.com/eventHub";
    httpOptions = new HttpConnectionOptions(logger: transportProtLogger);

    hubConnection = HubConnectionBuilder().withUrl(serverUrl, options: httpOptions).configureLogging(hubProtLogger).build();

    hubConnection.onclose( (error) => print("Connection Closed"));
    hubConnection.keepAliveIntervalInMilliseconds = 100000;

    initLikeHub(); 

    viewStoryBloc.ViewStoryState.listen((event) {

      setState(() {
        for(var story in Array_StoriesBanner){
            if(story["type"] == "Story"){
              if((story["data"] as dashboardStoryModel).StoryId == (event[0] as Map)["storyId"].toString()){
              (story["data"] as dashboardStoryModel).totalView= (event[0] as Map)["totalView"].toString();
              (story["data"] as dashboardStoryModel).isView = 1;
            }
          }
        }
      });
    });

    followBloc.FollowBlocState.listen((event) {
      setState(() {
        for(var story in Array_StoriesBanner){
            if(story["type"] == "Story"){
              if((story["data"] as dashboardStoryModel).createdById == event["userFollowerTypeId"].toString()){
              (story["data"] as dashboardStoryModel).isFollow= event["unFollow"] == true?0:1;
            }
          }
        }
      });
    });
  }

  _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(Duration(milliseconds: _debouncetime), () {
       if (searchTextController.text != "") {
         if(!searchedData){
           _onRefresh();
         }
       }
    });
  }

  getDistance(double storyLat, double storyLong,int index) async{
    Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

    var _distanceInMeters =  GeolocatorPlatform.instance.distanceBetween(position.latitude, position.longitude, storyLat, storyLong);
    
    var distance = (_distanceInMeters/1000);
    var twodisdecimal = distance.toStringAsFixed(2);

    setState(() {
      (Array_StoriesBanner[index]["data"] as dashboardStoryModel).distance = double.parse(twodisdecimal);
    });
    
  }

  getLocationName(double storyLat, double storyLong,int index) async{
    final coordinates = new Coordinates(storyLat, storyLong);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
     var placeName = "${first.featureName} - ${first.subAdminArea}";

    setState(() {
      Array_StoriesBanner[index]["data"].locality = placeName;
    });

  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    _scrollController.dispose();
     hubConnection.stop();
    super.dispose();
  }

  void didPopNext() {
    Logger.root.level = Level.ALL;
    // Writes the log messages to the console
    Logger.root.onRecord.listen((LogRecord rec) {
    });

    serverUrl = "http://travelappapi-dev.ap-south-1.elasticbeanstalk.com/eventHub";
    httpOptions = new HttpConnectionOptions(logger: transportProtLogger);

    hubConnection = HubConnectionBuilder().withUrl(serverUrl, options: httpOptions).configureLogging(hubProtLogger).build();

    hubConnection.onclose( (error) => print("Connection Closed"));
    hubConnection.keepAliveIntervalInMilliseconds = 100000;

    initLikeHub(); 
  }

  void initLikeHub() async{
    await hubConnection.start().catchError((E){
      print(E);
    });

    hubConnection.on("storyLikes", (object){
      log("Socket --------- ${object.toString()}");

      setState(() {
        for(var story in Array_StoriesBanner){
          if(story["type"] == "Story"){
            if((story["data"] as dashboardStoryModel).StoryId == (object[0] as Map)["storyId"].toString()){
              (story["data"] as dashboardStoryModel).totalLike = (object[0] as Map)["totalLike"].toString();
              (story["data"] as dashboardStoryModel).isLike = (object[0] as Map)["isLike"]?1:0; 
            }
          }
        }
        });
    });

    hubConnection.on("storyView", (object){
      log("Socket --------- ${object.toString()}");
      viewStoryBloc.changeViewStoryBlocState(object);
    });

    hubConnection.on("userFollow", (object){
      log("Socket --------- ${object.toString()}");

      setState(() {
        for(var story in Array_StoriesBanner){
          if(story["type"] == "Story"){
            if((story["data"] as dashboardStoryModel).createdById == (object[0] as Map)["followingUserId"].toString()){
              (story["data"] as dashboardStoryModel).isFollow = (object[0] as Map)["following"]?1:0;
            }
          }
        }
      });
    });

    hubConnection.on("userUnFollow", (object){
      log("Socket --------- ${object.toString()}");

      // (Array_StoriesBanner[index]["data"] as dashboardStoryModel)

      setState(() {
        for(var story in Array_StoriesBanner){
            if(story["type"] == "Story"){
              if((story["data"] as dashboardStoryModel).createdById == (object[0] as Map)["followingUserId"].toString()){
              (story["data"] as dashboardStoryModel).isFollow = (object[0] as Map)["following"]?1:0;
            }
          }
        }
      });
    });
  }

  apiFollowUnfollow(int index, int ToId, bool isFollow)async{
    String webLink = ApiCallingHelper.baseUrl + "UserFollower";

    var params = {
      "UserFollowerFriendId":ToId,
      "CreatedById":int.parse(userId),
      "UserFollowerTypeId":1,
      "UnFollow":!isFollow
    };

    Dio dio = Dio();

    var resp = await dio.post(webLink,data:params,options: Options(headers: {"Authorization": token}));

    

    var data = resp.data["response"];

    if(resp.data["status"]){      
    }

  }


  apiLikeStory(int index, int StoryId, bool islike)async{
    String webLink = ApiCallingHelper.baseUrl + "Story/ManageStory";

    var params = {
      "StoryId":StoryId,
      "IsLike":islike,
      "OperationType":1,
      "UserId":int.parse(userId)
    };

    Dio dio = Dio();

    var resp = await dio.post(webLink,data:params,options: Options(headers: {"Authorization": token}));

    

    var data = resp.data["response"];

    if(resp.data["status"]){

      // setState(() {
      //   Array_Stories[index].isLike = islike ? 1 : 0;
      //   Array_Stories[index].totalLike = islike ? "${int.parse(Array_Stories[index].totalLike) + 1}":Array_Stories[index].totalLike == "0"?"0":"${int.parse(Array_Stories[index].totalLike) - 1}";
      // });
      // _onRefresh();
    }

  }

  List<dynamic> Array_StoriesBanner = [];
  List<BannerModel> ArrayBanner = [];

  apiGetBanner(BuildContext context) async{
    var token = await getFromUserDefault(UserDefault_TOKEN);
    var webLink = ApiManager.BaseURL + "banner";

    ApiManager().callGetApi(context, webLink, true, token).then((data){
      log(data.toString());

      for(var banner in data["response"]){
         setState(() {
           ArrayBanner.add(BannerModel(
             bannerId: banner["bannerId"],
             bannerImage: banner["bannerImage"],
             createdByDate: banner["createdByDate"],
             createdById: banner["createdById"],
             imageURL: banner["imageURL"],
             metaDesc: banner["metaDescription"],
             modifiedByDate: banner["modifiedByDate"],
             modifiedById: banner["modifiedById"],
             name: banner["name"],
             orderId: banner["orderId"],
             statusId: banner["statusId"],
             type: "Banner",
             redirectURL : banner["redirectURL"]
           ));
         });
      }


      
      
    }).whenComplete((){
      apiDashboard(context,present);
      getLocationDistance();
    });
  }

  apiDashboard(BuildContext context, int pageNumber) async{
    String webLink = ApiCallingHelper.baseUrl + 'Story?userId=0&searchQuery={"loginUserId":$userId,"searchFreeText":"${searchTextController.text}"}&orderByColumn&sortOrder&pageIndex=$pageNumber&rowCount=10&storyId=0';

    setState(() {
      isLoading = true;
    });

    ApiManager().callGetApi(context, webLink, true,token).then((data)async{
      var apiData = data;
      // log(apiData.toString());

      if(apiData["status"]){

        for(var story in jsonDecode(apiData["response"])){

        TotalRecords = story["totalCount"];


        

        var tagName = "";

        if(story["storyTags"] != null){
          for(var tag in story["storyTags"]){
          if(tagName.isEmpty){
            tagName = tag["name"];
          }
          else{
            tagName = tagName + ", " + tag["name"];
          }
        }
        }



        setState((){
          Array_Stories.add(dashboardStoryModel(
            ProfilePicturePath: story["profilePicturePath"],
            StoryId: story["storyId"].toString(),
            createdById: story["createdById"].toString(),
            date: story["createdByDate"].toString(),
            lat: story["latitude"].toString(),
            long: story["longitude"].toString(),
            spotInformation: story["spotInformation"].toString(),
            storyMedias: story["storyMedias"],
            storyTags: story["storyTags"],
            title: story["title"],
            totalLike: story["totalLike"].toString(),
            totalShare: story["totalShare"].toString(),
            totalView: story["totalView"].toString(),
            userName: story["userName"],
            isLike: story["isLike"],
            locality: "",
            distance: 0.0,
            metaDesc: story["metaDescription"],
            tags: tagName,
            isMore: false,
            ArrayTags: story["storyTags"],
            locationName: story["locationName"],
            postalCode: story["postalCode"],
            isFollow: story["isFollow"],
            type: "Story",
            isView: story["isView"],
            statusId: story["statusId"]
          ));
        });

        Array_StoriesBanner.add({"type": "Story","data":dashboardStoryModel(
            ProfilePicturePath: story["profilePicturePath"],
            StoryId: story["storyId"].toString(),
            createdById: story["createdById"].toString(),
            date: story["createdByDate"].toString(),
            lat: story["latitude"].toString(),
            long: story["longitude"].toString(),
            spotInformation: story["spotInformation"].toString(),
            storyMedias: story["storyMedias"],
            storyTags: story["storyTags"],
            title: story["title"],
            totalLike: story["totalLike"].toString(),
            totalShare: story["totalShare"].toString(),
            totalView: story["totalView"].toString(),
            userName: story["userName"],
            isLike: story["isLike"],
            locality: "",
            distance: 0.0,
            metaDesc: story["metaDescription"],
            tags: tagName,
            isMore: false,
            ArrayTags: story["storyTags"],
            locationName: story["locationName"],
            postalCode: story["postalCode"],
            isFollow: story["isFollow"],
            type: "Story",
            isView: story["isView"],
            statusId: story["statusId"]
          )});
        }

        // for(var story in Array_StoriesBanner){
        //   if(story["type"] == "Banner"){
        //     var index = Array_StoriesBanner.indexOf(story);
        //     Array_StoriesBanner.removeAt(index);
        //   }
        // }

        var toRemove = [];


        Array_StoriesBanner.forEach( (story) {
         if(story["type"] == "Banner") 
           toRemove.add(story);
        });

        Array_StoriesBanner.removeWhere( (e) => toRemove.contains(e));



        var s = 3;
        var i = 0;

      while (s< Array_StoriesBanner.length){
        if(i == ArrayBanner.length){
          break;
        }
        Array_StoriesBanner.insert(s, {"type":"Banner","data":ArrayBanner[i]});
        s += (3+1);
        i = i + 1;
      }

      setState(() {
        
      });

        




        if (Array_Stories.isEmpty) {
          setState(() {
            showError = true;
            isLoading = false;
          });
        } else {
          setState(() {
            showError = false;
          });
        }

        if (TotalRecords == Array_Stories.length) {
          setState(() {
            isLoading = false;
          });
        }

        

        setState(() {
          isDataLoaded = true;
        });
        
      }
    }).whenComplete((){
      getLocationDistance();
    });
  }

  

  void _onRefresh() {
    
    setState(() {
      isDataLoaded = false;
      Array_Stories.clear();
        Array_StoriesBanner.clear();
      });
    new Future.delayed(const Duration(milliseconds: 100)).then((val) {
      present = 0;
      perPage = 1;
      isLoading = false;
      present = 1;
      apiDashboard(context,present);
      
      _refreshController.refreshCompleted();
      searchedData = true;
    });
  }

  var isSearchOn = false;

  getLocationDistance() async{
    for(var story in Array_StoriesBanner){

      if(story["type"] == "Story"){
        var index = Array_StoriesBanner.indexOf(story);

          Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

          var _distanceInMeters =  GeolocatorPlatform.instance.distanceBetween(position.latitude, position.longitude, double.parse(story["data"].lat), double.parse(story["data"].long));

          var distance = (_distanceInMeters/1000);
          var twodisdecimal = distance.toStringAsFixed(2);

          // getDistance(double.parse(story["latitude"]), double.parse(story["longitude"]),i);

          final coordinates = new Coordinates(double.parse(story["data"].lat), double.parse(story["data"].long));
          var addresses = await Geocoder.google("AIzaSyBGqdkh3xkI_YeOPh9nvRoOXhZt6S2LsnU").findAddressesFromCoordinates(coordinates);
          var first = addresses.first;
          var placeName = "${first.featureName} - ${first.subAdminArea}";

          
          setState(() {
            (Array_StoriesBanner[index]["data"] as dashboardStoryModel).locality = placeName.length>15?placeName.replaceRange(15, placeName.length, "..."):placeName;
            (Array_StoriesBanner[index]["data"] as dashboardStoryModel).distance = double.parse(twodisdecimal);
          });
      }

          
    }
  }

  

  


  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      key: _scaffoldKey,
      // appBar: AppBar(
      //   title: Text(
      //     "Dashboard",
      //     style: GoogleFonts.roboto(color: Colors.black),
      //   ),
      //   backgroundColor: Colors.white,
      //   centerTitle: true,
      //   leading: InkWell(
      //     onTap: (){
      //   _scaffoldKey.currentState.openDrawer();
      //     },
      //     child: Icon(Icons.menu,color:AppDefault_Green,),
      //   ),
      //   bottom: PreferredSize(
      //     preferredSize: Size(double.maxFinite, 30),
      //     child: Padding(
      //           padding: const EdgeInsets.all(8.0),
      //           child: Row(
      //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //             children: <Widget>[
      //               new SizedBox(width: 10,),
      //               Expanded(
      //                 child: Container(
      //                   child: CupertinoTextField(
      //                     controller: searchTextController,
      //                     prefix: Icon(Icons.search,color:AppDefault_Green),
      //                     placeholder: "Search",
      //                     placeholderStyle: GoogleFonts.roboto(),
      //                     onChanged: (text){
      //                       if(text.isEmpty){
      //                           _onRefresh();
      //                         }
      //                         else{
      //                           searchedData = false;
      //                         }
      //                     },
      //                   ),
      //                 ),
      //               ),
      //               new SizedBox(width: 10,),

                    

      //               IconButton(icon: Icon(
      //                 Icons.add_circle_outline,
      //                 size: 30,
      //                 color:AppDefault_Green
      //               ), onPressed: ()async{
      //                 Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);

     
      //                 Navigator.of(context).push(
      //                     CupertinoPageRoute(builder: (context) => AddStory(lat: position.latitude,long: position.longitude,)));
      //               }),
      //               new SizedBox(width: 10,),
      //               Icon(
      //                 Icons.notifications,
      //                 size: 30,
      //                 color:AppDefault_Green
      //               )
      //             ],
      //           ),
      //         ),
      //   ),
      // ),
      drawer: _drawer(context),

      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Center(
          child: Column(
            children: <Widget>[
              buildCustomAppBar(context),
              SizedBox(
                height: 5,
              ),
              Expanded(child: isDataLoaded? SmartRefresher(
                  header: ClassicHeader(),
                  controller: _refreshController,
                  enablePullDown: true,
                  enablePullUp: false,
                  onRefresh: _onRefresh,
                  child:_postList(context)): DataLoded())
              
            ],
          ),
        ),
      ),
    );
  }

  _drawer(BuildContext context) {
    return Drawer(
      child: Container(
        height: double.maxFinite,
        width: MediaQuery.of(context).size.width - 200,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 80,
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                    Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>SuggetionScreen()));
                },
                child: Container(
                  width: 120,
                  height: 120,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(width: 1),
                      image: DecorationImage(
                          image: ProfilePic == ""? AssetImage("assets/AppLogo/PikNikTransparent.png"): CachedNetworkImageProvider(ProfilePic),
                          fit: BoxFit.fill)),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text(LocationPath == null?"":LocationPath),
              SizedBox(
                height: 30,
              ),
              Divider(
                height: 2,
              ),
              Expanded(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Center(
                  child: InkWell(
                    onTap: () async{
                      Navigator.of(context).pop();
                      Loader.getInstance().showOverlay(context);
                    position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
                    Loader.getInstance().dismissOverlay();
                      Navigator.of(context).push(
                          CupertinoPageRoute(builder: (context) => AddStory(lat: position.latitude,long: position.longitude,)));
                    },
                    child: ListTile(
                      leading: Icon(Icons.add_box),
                        title: Text(
                      "Add Story",
                      // textAlign: TextAlign.center,
                      style: GoogleFonts.roboto(fontWeight: FontWeight.w600,fontSize: 16)
                    )),
                  ),
                ),
                // Divider(
                //   height: 2,
                // ),
                // InkWell(
                //   onTap: (){
                //     Navigator.of(context).pop();
                //     Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>Settings()));
                //   },
                //   child: ListTile(
                //     leading: Icon(Icons.settings),
                //       title: Text(
                    
                //     "Settings",
                //     // textAlign: TextAlign.center,
                //     style: GoogleFonts.roboto(fontWeight: FontWeight.w600,fontSize:16),
                //   )),
                // ),
                Divider(
                  height: 2,
                ),
                
                InkWell(
                  onTap: (){
                    Navigator.of(context).pop();
                    
                    Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>SuggetionScreen()));
                  },
                  child: ListTile(
                    leading: Icon(Icons.person),
                      title: Text(
                    "Profile",
                    // textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(fontWeight: FontWeight.w600,fontSize:16),
                  )),
                ),
                
                Divider(
                  height: 2,
                ),
                InkWell(
                  onTap: (){
                    Navigator.of(context).pop();
                    Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>ContactUs()));
                  },
                  child: ListTile(
                    leading: Icon(Icons.contact_phone),
                      title: Text(
                    "Contact Us",
                    // textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(fontWeight: FontWeight.w600,fontSize:16),
                  )),
                ),
                Divider(
                  height: 2,
                ),
                InkWell(
                  onTap: (){
                    Navigator.of(context).pop();
                    Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>AboutUs()));
                  },
                  child: ListTile(
                    leading: Icon(Icons.info),
                      title: Text(
                    "About",
                    // textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(fontWeight: FontWeight.w600,fontSize:16),
                  )),
                ),
                Divider(
                  height: 2,
                ),
                InkWell(
                  onTap: (){
                    Navigator.of(context).pop();
                    Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>Terms()));
                  },
                  child: ListTile(
                    leading: Icon(Icons.list),
                      title: Text(
                    "Terms & Conditions",
                    // textAlign: TextAlign.center,
                    style: GoogleFonts.roboto(fontWeight: FontWeight.w600,fontSize:16),
                  )),
                ),
                Divider(
                  height: 2,
                ),
                ListTile(
                  onTap: (){

                    Alert(
                      type: AlertType.warning,
      context: context,
      title: "",
      desc: "Are you sure you want to logout ?",
      
      style: AlertStyle(
        titleStyle: TextStyle(fontSize: 2),
        isOverlayTapDismiss: false,
        isCloseButton: false
      ),
      closeFunction: (){},
      closeIcon: Icon(Icons.close,size:0,color:Colors.transparent),
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: ()async{
             await GoogleSignIn().signOut();
                                    setIntoDefault(UserDefault_IsLogin, "false");
                                    setIntoDefault(UserDefault_Profile, "");
                                    setIntoDefault(UserDefault_Location, "");
                                     Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>LoginScreen()), (route) => false);
                                             
          },
          color: AppDefault_Green,
          radius: BorderRadius.circular(0.0),
        ),

        DialogButton(
          child: Text(
            "CANCEL",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: (){
            Navigator.of(context).pop();
          },
          color: AppDefault_Green,
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();
    
                    
                  },
                  leading: Icon(Icons.power_settings_new,color: Colors.red,),
                    title: Text(
                  "Logout",
                  // textAlign: TextAlign.center,
                  style: GoogleFonts.roboto(fontWeight: FontWeight.w600,color: Colors.red,fontSize:16),
                )),
                Divider(height: 1,)
                  ]
                ),
              ),
              new Container(height: 40,width:double.maxFinite,
                color:Colors.black38,
                child: new Center(child: Text("App Version: 2.0",style:GoogleFonts.roboto()))
              ),
              new SizedBox(height: 40,)
            ],
          ),
        ),
      ),
    );
  }

  buildCustomAppBar(BuildContext context){
    return new Container(
      width:double.maxFinite,
      height: 60 + MediaQuery.of(context).padding.top,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
            blurRadius: 10,
            color: Colors.grey[300],
            spreadRadius: 2,
          )
        ],
        
      ),
      child: SafeArea(
        top: true,
        left: true,
        right: true,
        bottom: false,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(10,0,10,0),
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  new IconButton(
                    color: Colors.black,
                    icon: Icon(Icons.menu,color:AppDefault_Green,),
                    onPressed: (){
                      _scaffoldKey.currentState.openDrawer();
                    },
                  ),

                  Expanded(
                    child: isSearch?new Container(
                        height: 40,
                        child: Row(
                          children: [
                            Expanded(
                              child: Center(
                                child: new TextField(
                                  controller: searchTextController,
                                  onChanged: (text){
                                    if(text.isEmpty){
                                      _onRefresh();
                                    }
                                    else{
                                      searchedData = false;
                                    }
                                  },
                                  
                                  style: GoogleFonts.roboto(),
                                  decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.fromLTRB(10, 0, 10, 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                        borderSide: BorderSide(color:AppDefault_Green)
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(50),
                                        borderSide: BorderSide(color:AppDefault_Green)
                                      ),
                                      hintText: "Search",
                                      hintStyle: GoogleFonts.roboto())),
                                ),
                              ),
                            new SizedBox(width:5),
                            
                          ],
                        ),
                      ):
                      Container(
                        height: 35,
                        child:Row(
                          children: [
                            Image.asset("assets/Auth/DashBoardLogo.png"),
                          ],
                        )
                      ),
                  ),

                  new SizedBox(width: 10,),
                  InkWell(onTap: (){
                    setState(() {
                      isSearch = !isSearch;
                    });
                  },child: new Icon(isSearch?Icons.close:Icons.search,size:25,color:AppDefault_Green,)),

                  new SizedBox(width: 10,),
                  InkWell(onTap: (){
                   Navigator.of(context).push(
                          CupertinoPageRoute(builder: (context) => NotificationList()));
                  },child: new Icon(Icons.notifications,size:25,color:AppDefault_Green,)),
                  
                  new SizedBox(width: 10,),
                  InkWell(onTap: ()async{
                    Loader.getInstance().showOverlay(context);
                    position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
                    Loader.getInstance().dismissOverlay();
                      Navigator.of(context).push(
                          CupertinoPageRoute(builder: (context) => AddStory(lat: position.latitude,long: position.longitude,)));
                  },child: new Icon(Icons.add_circle_outline,size:25,color:AppDefault_Green,)),
                  new SizedBox(width: 10,),
                ],
              ),
            ),
            
            
          ],
        ),
      ),
    );
  }

 
  _postList(BuildContext context) {
    return ListView.builder(
      controller: _scrollController,
        itemCount: TotalRecords != Array_StoriesBanner.length
          ? Array_StoriesBanner.length + 1
          : Array_StoriesBanner.length,
        itemBuilder: (context, index) {
          if (index == Array_StoriesBanner.length) {
          return _buildProgressIndicator(context);
        }

          return Array_StoriesBanner[index]["type"] == "Banner"? new Padding(
            padding: const EdgeInsets.fromLTRB(5, 0, 5, 8),
            child: new Container(
              width:double.maxFinite,
              height: 200,
              child: InkWell(
                onTap: (){
                  launchURL((Array_StoriesBanner[index]["data"] as BannerModel).redirectURL);
                },
                child: new Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                  child:new Container(
                    height: double.maxFinite,
                    width: double.maxFinite,
                    child: CachedNetworkImage(imageUrl: "https://travel-app-media.s3.ap-south-1.amazonaws.com/"+(Array_StoriesBanner[index]["data"] as BannerModel).imageURL,fit: BoxFit.cover,progressIndicatorBuilder: (context, url, downloadProgress) => 
                                                          Center(child: CircularProgressIndicator(value: downloadProgress.progress)),)
                  )
                ),
              )
            )
          ) :  Padding(
            padding: const EdgeInsets.fromLTRB(5, 0, 5, 8),
            child: InkWell(
              onTap: (){
                // Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>StoryDetailScreen()));
              },
              child: VisibilityDetector(
                key: Key('$index'),
                onVisibilityChanged: (visibilityInfo) {
                  var visiblePercentage = visibilityInfo.visibleFraction * 100;
                  
                },
                child: Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        width: double.maxFinite,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  // SizedBox(
                                  //   width: 10,
                                  // ),
                                  Expanded(
                                    child: Row(
                                      children: <Widget>[
                                        InkWell(
                                            onTap: (){
                                              Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>SuggetionScreen(userId: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).createdById,)));
                                            },
                                          child: Container(
                                            height: 35,
                                            width: 35,
                                            decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                border: Border.all(
                                                  width: 1.0,
                                                  color: Colors.black,
                                                ),
                                                image: DecorationImage(
                                                    image: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).ProfilePicturePath == null? AssetImage("assets/AppLogo/PikNikTransparent.png"): CachedNetworkImageProvider((Array_StoriesBanner[index]["data"] as dashboardStoryModel).ProfilePicturePath),
                                                    fit: BoxFit.fill)),
                                          ),
                                        ),

                                        Expanded(
                                          child: Padding(
                                      padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                      child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            InkWell(
                                              onTap: (){
                                                Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>SuggetionScreen(userId: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).createdById)));
                                              },
                                              child: AutoSizeText(
                                                (Array_StoriesBanner[index]["data"] as dashboardStoryModel).userName == null?"Bruce Wyne":(Array_StoriesBanner[index]["data"] as dashboardStoryModel).userName,
                                                style: GoogleFonts.roboto(fontWeight: FontWeight.w600),
                                                maxLines: 1,minFontSize: 1,

                                              ),
                                            ),
                                            new SizedBox(height:5),
                                            InkWell(
                                              onTap: (){
                                                openMap(double.parse((Array_StoriesBanner[index]["data"] as dashboardStoryModel).lat), double.parse((Array_StoriesBanner[index]["data"] as dashboardStoryModel).long));
                                              },
                                              child: Row(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                      child: Icon(Icons.location_on,size:15,color: Colors.grey,),
                                                    ),
                                                    Padding(
                                                  padding: const EdgeInsets.fromLTRB(2, 0, 2, 0),
                                                  child: Text("${(Array_StoriesBanner[index]["data"] as dashboardStoryModel).locality}",
                                                      style: GoogleFonts.roboto(fontSize: 11,
                                                          fontWeight: FontWeight.w500,color: Colors.grey)),
                                                ),
                                                 Padding(
                                                 padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                                                 child: Text("${(Array_StoriesBanner[index]["data"] as dashboardStoryModel).distance}km",style:GoogleFonts.roboto(fontSize:11,color: Colors.grey),),
                                               )
                                                  ],
                                                ),
                                            ),
                                          ],
                                      ),
                                    ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  // SizedBox(
                                  //   width:80,
                                  // ),
                                  
                                    (Array_StoriesBanner[index]["data"] as dashboardStoryModel).createdById.toString() == userId? new SizedBox(height: 0,) :InkWell(
                                     onTap: (){
                                       apiFollowUnfollow(index, int.parse((Array_StoriesBanner[index]["data"] as dashboardStoryModel).createdById), (Array_StoriesBanner[index]["data"] as dashboardStoryModel).isFollow == 0?true:false);
                                     },
                                     child: Container(
                                       height: 30,
                                       width: 70,
                                       decoration: BoxDecoration(
                                         border: Border.all(
                                           width: 1,
                                           color: AppDefault_Green
                                         ),
                                         borderRadius: BorderRadius.circular(50)
                                       ),
                                       child: Padding(
                                         padding: const EdgeInsets.all(5.0),
                                         child: Center(child: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).isFollow != 0?Text("Following", style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green,)):Text("Follow", style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green,))),
                                       )),
                                   )
                                 
                                ],
                              ),
                             
                            ),
                            
                            Divider(
                              height: 2,
                            ),
                            
                            
                          
                          ],
                        ),
                      ),
                      Stack(
                        children: [
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  height: 260,
                                  width: double.maxFinite,
                                  child: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).storyMedias.isEmpty ? new Container():CarouselSlider.builder(
                                    
                                itemCount: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).storyMedias == null ? 0:(Array_StoriesBanner[index]["data"] as dashboardStoryModel).storyMedias.length>0
                                    ?(Array_StoriesBanner[index]["data"] as dashboardStoryModel).storyMedias.length
                                    :2,
                                itemBuilder: (BuildContext context, int itemIndex) =>
                                    InkWell(
                                      onTap: (){
                                        Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>ImageZoomIn(current: itemIndex,imageList: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).storyMedias,name: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).title,storyId: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).StoryId,screen: "Dashboard",isView: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).isView,isLike: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).isLike,)));
                                      },
                                      child: Column(
                                        children: [
                                          Flexible(
                                            child: Container(
                                              width: MediaQuery.of(context).size.width,
                                                margin: EdgeInsets.symmetric(horizontal: 2.0),
                                                decoration: new BoxDecoration(
                                                  // color: Colors.white,
                                                  borderRadius: BorderRadius.circular( screenAwareWidth(5, context)),
                                                ),
                                                child: Container(
                                                  height: 260,
                                                    width: double.maxFinite,
                                                    child: ClipRRect(
                                                        borderRadius: BorderRadius.circular(5),
                                                       child : (Array_StoriesBanner[index]["data"] as dashboardStoryModel).storyMedias[itemIndex]["FileTypeId"] == 6?CachedNetworkImage(imageUrl:(Array_StoriesBanner[index]["data"] as dashboardStoryModel).storyMedias[itemIndex]["FilePath"],fit: BoxFit.contain,progressIndicatorBuilder: (context, url, downloadProgress) => 
                                                        Center(child: CircularProgressIndicator(value: downloadProgress.progress)),):
                                                          FlickMultiPlayer(
                                                      url: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).storyMedias[itemIndex]["FilePath"],
                                                      flickMultiManager: flickMultiManager,
                                                      image: "assets/PikNikTransparent.png",
                                                    )//new Container()
                                                    )
                                                )
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                options: CarouselOptions(
                                  height: 260,
                                  initialPage: 0,
                                  scrollPhysics: BouncingScrollPhysics(),
                                  viewportFraction: 1.0,
                                  enableInfiniteScroll: true,
                                  // reverse: false,
                                  enlargeCenterPage: false,
                                  // autoPlay: true,
                                  autoPlayInterval: Duration(seconds: 10),
                                  autoPlayAnimationDuration: Duration(days: 11111),
                                  // autoPlayCurve: Curves.fastOutSlowIn,
                                  // pauseAutoPlayOnTouch: true,
                                  onPageChanged: (index,reason) {
                                    setState(() {
                                      _current = index;
                                    });
                                  },
                                  scrollDirection: Axis.horizontal,
                                ),
                              ),
                                ),
                               
                              ],
                            ),
                          ),
                          (Array_StoriesBanner[index]["data"] as dashboardStoryModel).storyMedias == null ? new SizedBox(height: 0,) :Positioned(
                            bottom: 15,
                            left: 0,right: 0,
                            child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).storyMedias.map((url) {
                int index1 = (Array_StoriesBanner[index]["data"] as dashboardStoryModel).storyMedias.indexOf(url);
                return Container(
                  width: 8.0,
                  height: 8.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: _current == index1
                      ? Colors.white
                      : Colors.grey
                  ),
                );
            }).toList(),
          ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(0),
                            child: Row(
                              children: <Widget>[
                                IconButton(
                                  onPressed: (){
                                    apiLikeStory(index, int.parse((Array_StoriesBanner[index]["data"] as dashboardStoryModel).StoryId), (Array_StoriesBanner[index]["data"] as dashboardStoryModel).isLike ==0?true:false);
                                  },
                                  icon: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).isLike ==0?ImageIcon(
                                    AssetImage("assets/NotLike.png"),
                                    size: 20,
                                    color: AppDefault_Green,
                                  ):ImageIcon(
                                    AssetImage("assets/Like.png"),
                                    color: AppDefault_Green,
                                    size: 20,
                                  ),
                                ),
                                Row(
                                  children: [
                                    Transform.translate(
                                      offset: Offset(-11, 0),
                                      child: IconButton(
                                        onPressed: (){
                                          Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>StoryComment(storyId: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).StoryId,)));
                                        },
                                        icon: ImageIcon(
                                        AssetImage("assets/chat.png"),
                                        color: AppDefault_Green,
                                        size: 20,
                                      ),
                                      ),
                                    ),
                                    Transform.translate(
                                      offset: Offset(-22, 0),
                                      child: IconButton(
                                        padding: EdgeInsets.all(0),
                                        onPressed: (){
                                          Share.share('check out my story\n\nhttps://example.com\n\nSpot Information: ${(Array_StoriesBanner[index]["data"] as dashboardStoryModel).spotInformation}\n\nHow to get there: ${(Array_StoriesBanner[index]["data"] as dashboardStoryModel).metaDesc}');
                                        },
                                        icon: ImageIcon(
                                          AssetImage("assets/share.png"),
                                          color: AppDefault_Green,
                                          size: 20,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(right:15.0),
                            child: Row(
                        children: <Widget>[
                            SizedBox(
                              width: 10,
                            ),
                            InkWell(onTap:(){
                              Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>StoryViewList(StoryId: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).StoryId,userId: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).createdById,)));
                            },child: Text("${(Array_StoriesBanner[index]["data"] as dashboardStoryModel).totalView} views,")),
                            SizedBox(
                              width: 5,
                            ),
                            InkWell(onTap:(){
                              Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>StoryLikeList(StoryId: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).StoryId)));
                            },child: Text("${(Array_StoriesBanner[index]["data"] as dashboardStoryModel).totalLike} Likes"))
                        ],
                      ),
                          ),
                        ],
                      ),
                      new SizedBox(height: 10,),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(10,0,10,0),
                        child: Container(
                          width: double.maxFinite,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    (Array_StoriesBanner[index]["data"] as dashboardStoryModel).isMore?new Container():new RichText(text: TextSpan(
                                      style: GoogleFonts.roboto(fontSize: 14.0,
                                      color: Colors.black,),
                                      
                                      children: [
                                        new TextSpan(
                                          
                                          text: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).userName,
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.bold,decoration: TextDecoration.underline)
                                        ),
                                        new TextSpan(
                                          text: "  ",
                                          style: GoogleFonts.roboto()
                                        ),

                                        new TextSpan(
                                          text: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).title,
                                          style: GoogleFonts.roboto(
                                        fontWeight: FontWeight.w500)
                                        ),

                                        new TextSpan(
                                          text: "  ",
                                          style: GoogleFonts.roboto()
                                        ),

                                        new TextSpan(
                                          text: "\n",
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w500)
                                        ),
                                        
                                        new TextSpan(
                                          text: "Spot Info: ",
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green),
                                        ),
                                        new TextSpan(
                                          text: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).spotInformation.length>19? (Array_StoriesBanner[index]["data"] as dashboardStoryModel).spotInformation.replaceRange(20, (Array_StoriesBanner[index]["data"] as dashboardStoryModel).spotInformation.length, "...."):(Array_StoriesBanner[index]["data"] as dashboardStoryModel).spotInformation,
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w400),
                                        )
                                      ]
                                    )),
                                    (Array_StoriesBanner[index]["data"] as dashboardStoryModel).isMore?new RichText(text: TextSpan(
                                      style: GoogleFonts.roboto(fontSize: 14.0,
                                      color: Colors.black,),
                                      children: [

                                        new TextSpan(
                                          
                                          text: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).userName,
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.bold,decoration: TextDecoration.underline)
                                        ),
                                        new TextSpan(
                                          text: "  ",
                                          style: GoogleFonts.roboto()
                                        ),
                                        new TextSpan(
                                          text: "Spot Info: ",
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green)
                                        ),
                                        new TextSpan(
                                          text: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).spotInformation,
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w400),
                                        ),

                                        new TextSpan(
                                          text: "\n",
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w500)
                                        ),
                                        new TextSpan(
                                          text: "\n",
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w500)
                                        ),
                                        
                                        new TextSpan(
                                          text: "Get there by ",
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green)
                                        ),
                                        new TextSpan(
                                          text: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).metaDesc,
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w400)
                                        ),
                                        new TextSpan(
                                          text: "\n",
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w500)
                                        ),
                                        new TextSpan(
                                          text: "\n",
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w500)
                                        ),
                                        new TextSpan(
                                          text: "Tags: ",
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: AppDefault_Green)
                                        ),
                                        new TextSpan(
                                          text: (Array_StoriesBanner[index]["data"] as dashboardStoryModel).tags,
                                          style: GoogleFonts.roboto(fontWeight: FontWeight.w400)
                                        )
                                      ]
                                    )):new Container(),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(right: 20),
                                child: InkWell(
                                  onTap: (){
                                    setState(() {
                                      (Array_StoriesBanner[index]["data"] as dashboardStoryModel).isMore = !(Array_StoriesBanner[index]["data"] as dashboardStoryModel).isMore;
                                    });
                                  },
                                  child: new Text((Array_StoriesBanner[index]["data"] as dashboardStoryModel).isMore?"Less":"More",style: GoogleFonts.roboto(fontSize: 14.0,
                                    color: Colors.black,fontWeight: FontWeight.bold),),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                       Padding(
                                padding: const EdgeInsets.fromLTRB(10, 15, 10, 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      formattedDate("dd-MM-yyyy HH:mm:ss", "dd MMM @ h:mm a", (Array_StoriesBanner[index]["data"] as dashboardStoryModel).date),
                                      style: TextStyle(fontSize: 12),
                                    ),

                                    (Array_StoriesBanner[index]["data"] as dashboardStoryModel).createdById.toString() == userId? InkWell(
                                      onTap: ()async{

                                        // Navigator.of(context).push(
                                        //     CupertinoPageRoute(builder: (context) => UpdateStory(lat: position.latitude,long: position.longitude,storyDetails: Array_Stories[index], )));
     
                                        Navigator.of(context).push(
                                            CupertinoPageRoute(builder: (context) => UpdateStory(lat: double.parse((Array_StoriesBanner[index]["data"] as dashboardStoryModel).lat),long: double.parse((Array_StoriesBanner[index]["data"] as dashboardStoryModel).long),storyDetails: (Array_StoriesBanner[index]["data"] as dashboardStoryModel), )));
                                      },
                                      child: Text(
                                        "Edit Story",
                                        style: TextStyle(fontSize: 15,color:Colors.blue),
                                      ),
                                    ):new Container(),
                                  ],
                                )),
                      Divider(
                        height: 1,
                        // color: Colors.black,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  Widget _buildProgressIndicator(BuildContext context) {
    if (TotalRecords != Array_Stories.length) {
      return new Padding(
        padding: const EdgeInsets.all(8.0),
        child: new Center(
          child: new Opacity(
            opacity: isLoading ? 1.0 : 0.0,
            child: CupertinoActivityIndicator(),
          ),
        ),
      );
    } else {
      return new Padding(
        padding: const EdgeInsets.all(8.0),
        child: new Center(
          child: new Opacity(
            opacity: isLoading ? 0.0 : 0.0,
            child: CupertinoActivityIndicator(),
          ),
        ),
      );
    }
  }

  Widget DataLoded(){
    return ListView(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          ListView.builder(
              itemCount: 3,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context,int mainIndex){
                return Shimmer.fromColors(
                  baseColor: Colors.white,
                  highlightColor: Colors.grey[200],
                  enabled: true,
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: double.maxFinite,
                        child: new Padding(
                          padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
                          child: new Row(
                            children: [
                              new Container(
                                height: 40,width:40,
                                child:new CircleAvatar()
                              ),
                              new SizedBox(width:10),
                              new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  new Container(height: 5,width:60, color:Colors.white),
                                  new SizedBox(height:5),
                                  new Container(height: 5,width:80, color:Colors.white),
                                  new SizedBox(height:5),
                                  new Container(height: 5,width:100, color:Colors.white),
                                ]
                              )
                            ],
                          ),
                        )
                      ),
                      Container(
                        height: screenAwareWidth(350, context),
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            padding: EdgeInsets.only(left: screenAwareWidth(5, context),
                                right: screenAwareWidth(5, context)),
                            scrollDirection: Axis.horizontal,
                            itemCount: 1,
                            itemBuilder: (BuildContext context,int childIndex){
                              return Container(
                                margin: EdgeInsets.only(left: screenAwareWidth(10, context),top: screenAwareWidth(15, context),
                                    bottom: screenAwareWidth(15, context),right:screenAwareWidth(10, context)),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(
                                        width: 0.2,
                                        color: Colors.grey[400]
                                    ),
                                    color: Colors.white
                                ),
                                width: MediaQuery.of(context).size.width -50
                              );
                            }),
                      )
                    ],
                  ),
                );
              })
        ]
    );
  }

  
}

class dashboardStoryModel{
  String date;
  String spotInformation;
  String lat;
  String long;
  String createdById;
  String ProfilePicturePath;
  String StoryId;
  List storyMedias;
  List storyTags;
  String title;
  String totalShare;
  String totalLike;
  String totalView;
  String userName;
  int isLike;
  double distance;
  String locality;
  String metaDesc;
  String tags;
  bool isMore;
  var ArrayTags;
  var locationName;
  var postalCode;
  String thumbnailPathForProfile;
  String FilePathWithURL;
  int isFollow;
  String type;
  int isView;
  int statusId;
  dashboardStoryModel({this.date,this.spotInformation,this.lat,this.long,this.createdById,this.ProfilePicturePath,this.storyMedias,this.storyTags,this.title,this.StoryId,this.totalLike,this.totalShare,this.totalView,this.userName,this.isLike,this.distance,this.locality,this.metaDesc,this.tags,this.isMore,this.ArrayTags,this.postalCode,this.locationName,this.thumbnailPathForProfile,this.FilePathWithURL,this.isFollow,this.type,this.isView,this.statusId});
}

class BannerModel{
  var bannerId;
  var name;
  var metaDesc;
  var statusId;
  var orderId;
  var createdById;
  var createdByDate;
  var modifiedByDate;
  var modifiedById;
  var imageURL;
  var bannerImage;
  var type;
  var redirectURL;
  
  BannerModel({
    this.bannerId,
    this.name,
    this.metaDesc,
    this.statusId,
    this.orderId,
    this.createdById,
    this.createdByDate,
    this.modifiedByDate,
    this.modifiedById,
    this.imageURL,
    this.bannerImage,
    this.type,
    this.redirectURL
  });
}
