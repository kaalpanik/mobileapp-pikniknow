import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/controllers/Suggetions/suggetions.dart';

class StoryViewList extends StatefulWidget{
  var StoryId;
  var userId;
  StoryViewList({this.StoryId,this.userId});
  @override
  State<StatefulWidget> createState() {
    return StoryViewListState();
  }
}

class StoryViewListState extends State<StoryViewList>{

  var Array_Likes = [];
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero,(){
      apiGetLikes(context);
    });
  }

  apiGetLikes(BuildContext context) async{
    var token = await getFromUserDefault(UserDefault_TOKEN);
    var userId = await getFromUserDefault(UserDefault_USERID);
    
    var webLink = ApiManager.BaseURL + "Story/GetStoryLikes?storyId=${widget.StoryId}&pageIndex=1&rowCount=1000000000";

    ApiManager().callGetApi(context, webLink, true, token).then((data){
      if(data["status"]){
        var response = jsonDecode(data["response"]);
        for(var user in response){
          setState(() {
            if(user["typeId"] == 2){
              Array_Likes.add(user);
            }
            
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Story Views",style: GoogleFonts.roboto(color: AppDefault_Green),),
        leading: InkWell(
          onTap: (){
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back,color: Colors.black,)),
      ),
      body: new Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: new ListView.builder(
          itemCount: Array_Likes.length,
          itemBuilder: (context,index){
            return new Column(
              children: [
                new ListTile(
                  onTap: (){
                    Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>SuggetionScreen(userId: Array_Likes[index]["userId"].toString())));
                  },
                  leading: new Container(
                    height: 30,width:30,
                    child: new ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Array_Likes[index]["profilePicturePath"] == null? Icon(Icons.person) : new Image.network( Array_Likes[index]["profilePicturePath"]),
                    ),
                  ),
                  title: new Text(Array_Likes[index]["userName"],style:GoogleFonts.poppins()),
                  subtitle: new Text(formattedDate("dd-MM-yyyy HH:mm:ss", "dd MMM, yyyy @ h:m a", Array_Likes[index]["createdByDate"]),style:GoogleFonts.poppins()),
                  trailing: new Icon(Icons.arrow_forward_ios),
                ),
                new Divider(height: 1,)
              ],
            );
          },
        ),
      )
    );
  }


}
