import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/ProgressBar.dart';
import 'package:newapp/Common/dialogs.dart';
import 'package:newapp/Common/extention.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:signalr_client/signalr_client.dart';

class StoryComment extends StatefulWidget{
  var storyId;
  StoryComment({this.storyId});
  @override
  State<StatefulWidget> createState() {
    return StoryCommentState();
  }
}

class StoryCommentState extends State<StoryComment>{

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var token = "";

  ScrollController _scrollController = ScrollController();
  int present = 0;
  int perPage = 1;
  int TotalRecords = 0;
  RefreshController _refreshController;

  TextEditingController searchTextController = TextEditingController();

  bool isLoading = false;

  var isReply = false;
  var parentCommentId;
  var isReplyText = "";

  bool showError = false;
  int _current;

  List<CommentModel> ArrayComments = [];

  var userId = "";

  TextEditingController commentTextCont = TextEditingController();

  var focusNode = new FocusNode();


  final hubProtLogger = Logger("SignalR - hub");
final transportProtLogger = Logger("SignalR - transport");

var serverUrl;
final connectionOptions = HttpConnectionOptions;
var httpOptions;

HubConnection hubConnection;

  @override
  void initState() {
    super.initState();

    _refreshController = RefreshController();

    present = present + perPage;

    getFromUserDefault(UserDefault_TOKEN).then((t){
      token = t;
      
    });

    getFromUserDefault(UserDefault_USERID).then((id){
      userId = id;
      apiFetchComments(context,present);
    });

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        int mainPage = (TotalRecords / 15).ceil();
        if (present != mainPage) {
          present = present + perPage;
          apiFetchComments(context,present);
        } else {
          setState(() {
            isLoading = false;
          });
        }
      }
    });


    Logger.root.level = Level.ALL;
    // Writes the log messages to the console
    Logger.root.onRecord.listen((LogRecord rec) {
      print('${rec.level.name}: ${rec.time}: ${rec.message}');
    });

    serverUrl = "http://travelappapi-dev.ap-south-1.elasticbeanstalk.com/eventHub";
    httpOptions = new HttpConnectionOptions(logger: transportProtLogger);

    hubConnection = HubConnectionBuilder().withUrl(serverUrl, options: httpOptions).configureLogging(hubProtLogger).build();

    hubConnection.onclose( (error) => print("Connection Closed"));
    hubConnection.keepAliveIntervalInMilliseconds = 100000;

    initCommentHub();

  }

  apiFetchComments(BuildContext context,int pageNumber)async{
    Loader.getInstance().showOverlay(context);

    setState(() {
      isLoading = true;
    });

    String webLink = ApiCallingHelper.baseUrl + 'StoryComment?searchQuery&orderByColumn&sortOrder&pageIndex=$pageNumber&rowCount=15&storyId=${widget.storyId}';

    Dio dio = Dio();

    var resp = await dio.get(webLink,options: Options(headers: {"Authorization": token}));

    Loader.getInstance().dismissOverlay();
    

    var data = jsonDecode(resp.data["response"]);


    if(resp.data["status"]){

      print(jsonEncode(data));

      for(var comment in data){
        setState(() {
          ArrayComments.add(
            CommentModel(
              StoryId: comment["storyId"],
              comment: comment["comment"],
              commentId: comment["storyCommentId"],
              commentReply: comment["commentReplay"],
              date: comment["createdByDate"],
              isReply: comment["isReplay"],
              parentCommentId: int.parse(comment["parentStoryCommentId"]),
              profilePath: comment["profilePicturePath"],
              userName: comment["userName"]
            )
          );
        });
      }

      


      if (ArrayComments.isEmpty) {
          setState(() {
            showError = true;
          });
        } else {
          setState(() {
            showError = false;
          });
        }

        if (TotalRecords == ArrayComments.length) {
          setState(() {
            isLoading = false;
          });
        }
        scrollDownComment();
    }
  }

  void _onRefresh() {
    new Future.delayed(const Duration(milliseconds: 500)).then((val) {
      setState(() {
        ArrayComments.clear();
      });
      
      present = 0;
      perPage = 1;
      isLoading = false;
      present = present + perPage;
      apiFetchComments(context,present);
      _refreshController.refreshCompleted();
    });

  }

  void initCommentHub() async{
    await hubConnection.start().catchError((E){
      print(E);
    });

    hubConnection.on("storyComment", (object){
      log("Socket --------- ${object.toString()}");

      setState(() {

        if(widget.storyId == (object[0] as Map)["storyId"].toString()){
          if((object[0] as Map)["isReplay"]){
            for(var comment in ArrayComments){
              if(comment.commentId == (object[0] as Map)["parentStoryCommentId"]){
                comment.commentReply = List<dynamic>();
                
                Future.delayed(Duration(milliseconds: 200),(){
                  setState(() {
                    comment.commentReply.add((object[0] as Map));
                  });
                  
                });
                
              }
            }
          }
          else{
            ArrayComments.add(
              CommentModel(
                StoryId: (object[0] as Map)["storyId"],
                comment: (object[0] as Map)["comment"],
                commentId: (object[0] as Map)["storyCommentId"],
                // commentReply: (object[0] as Map)["commentReplay"],
                date: (object[0] as Map)["createdByDate"],
                isReply: (object[0] as Map)["isReplay"],
                parentCommentId: (object[0] as Map)["parentStoryCommentId"],
                profilePath: (object[0] as Map)["profilePicturePath"],
                userName: (object[0] as Map)["userName"]
              )
            );
            if((object[0] as Map)["isReplay"] == false){
              scrollDownComment();
            }
          }
          
          
        }          
        });
    });
  }

  void scrollDownComment() async {
    await Future.delayed(Duration(milliseconds: 200));
    _scrollController.animateTo(
      _scrollController.position.maxScrollExtent,
      curve: Curves.easeOut,
      duration: Duration(milliseconds: 300),
    );
  }


  ApiComment(BuildContext context, bool isreply, String text,int StoryId,)async{

    Loader.getInstance().showOverlay(context);

    var webLink = ApiCallingHelper.baseUrl + "StoryComment";

    var params;

    if(isreply){
      params = {
          "StoryCommentId":0,
          "StoryId":StoryId,
          "Comment":text,
          "IsReplay":true,
          "ParentStoryCommentId":parentCommentId,
          "CreatedById":int.parse(userId),
          "CreatedByDate":DateFormat("MM-dd-yyyy HH:mm:ss").format(DateTime.now())
      };
    }
    else{
      params = {
          "StoryCommentId":0,
          "StoryId":StoryId,
          "Comment":text,
          "IsReplay":false,
          "ParentStoryCommentId":0,
          "CreatedById":int.parse(userId),
          "CreatedByDate":DateFormat("MM-dd-yyyy HH:mm:ss").format(DateTime.now())
      };
    }

    Dio dio = Dio();

    var resp = await dio.post(webLink,data: params,options: Options(headers: {"Authorization": token}));

    Loader.getInstance().dismissOverlay();

    if(resp.data["status"]){
      setState(() {
          isReply = false;
          isReplyText ="";
          parentCommentId = 0;
        });
      commentTextCont.clear();
      // _onRefresh();
    }

    
  }

  void _handleAClientProvidedFunction(List<Object> parameters) {
    // logger.log(LogLevel.Information, "Server invoked the method");
    print(parameters);
  }

  @override
  void dispose() {
    hubConnection.stop();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          "Comments",
          style: GoogleFonts.roboto(color: AppDefault_Green),
        ),
        leading: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(
              Icons.arrow_back,
              color:AppDefault_Green,
            )),
      ),
      body: GestureDetector(
              onTapDown: (_){
                          FocusScope.of(context).requestFocus(FocusNode());
                        },
        child: new Container(
          height: double.maxFinite,
          width: double.maxFinite,
          child: Column(
            children: [
              Expanded(
                child: SmartRefresher(
                          header: ClassicHeader(),
                          controller: _refreshController,
                          enablePullDown: true,
                          enablePullUp: false,
                          onRefresh: _onRefresh,
                          child:new ListView.builder(
                  itemCount: ArrayComments.length,
                  controller: _scrollController,
                  itemBuilder: (context,index){
                    return new Column(
                      children: [
                        new ExpansionTile(
                          initiallyExpanded: true,
                          onExpansionChanged: (isReply){
                            Icon(Icons.keyboard_arrow_up, color: AppDefault_Green,);
                          },
                          leading: Container(
                                              height: 25,
                                              width: 25,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(
                                                    width: 1.0,
                                                    color: Colors.black,
                                                  ),
                                                  image: DecorationImage(
                                                      image: ArrayComments[index].profilePath == null? AssetImage("assets/AppLogo/PikNikTransparent.png") :NetworkImage(  ArrayComments[index].profilePath),
                                                      fit: BoxFit.fill)),
                                            ),
                          title: new Text(ArrayComments[index].userName != null? "${ArrayComments[index].userName} :" :"UserName :",style:GoogleFonts.roboto(fontSize: 12,fontWeight: FontWeight.w700,color: isReply?Colors.black:Colors.black)),
                          subtitle: Column(
                            children: [
                              Container(width: double.maxFinite,child: new Text(ArrayComments[index].comment,textAlign: TextAlign.left,style:GoogleFonts.roboto(color: Colors.black,fontSize:14))),
                              new Container(
                          width: double.maxFinite,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Text(formattedDate("dd-MM-yyyy HH:mm:ss", "dd MMM @ h:m a", ArrayComments[index].date),style:GoogleFonts.roboto(color: isReply?Colors.black:Colors.black,fontSize: 10,)),
                                InkWell(onTap: (){
                                  setState(() {
                                    isReply = true;
                                    isReplyText = ArrayComments[index].comment;
                                    parentCommentId = ArrayComments[index].commentId;
                                  });
                                  focusNode.requestFocus();
                                },child: new Text("Reply",style:GoogleFonts.roboto(color:AppDefault_Green)))
                              ],
                            ),
                          ),
                        ),
                            ],
                          ),
                          children: ArrayComments[index].commentReply != null?ArrayComments[index].commentReply.map<Widget>((replies){
                            var replyIndex = ArrayComments[index].commentReply.indexOf(replies);
                            return new ListTile(
                              contentPadding: EdgeInsets.only(left:50),
                              leading: Container(
                                              height: 25,
                                              width: 25,
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  border: Border.all(
                                                    width: 1.0,
                                                    color: Colors.black,
                                                  ),
                                                  image: DecorationImage(
                                                      image:ArrayComments[index].commentReply[replyIndex]["profilePicturePath"] == null? AssetImage("assets/AppLogo/PikNikTransparent.png") :NetworkImage(  ArrayComments[index].profilePath),
                                                      fit: BoxFit.fill)),
                                            ),
                          title: new Text(ArrayComments[index].commentReply[replyIndex]["userName"] == null?"UserName : ":"${ArrayComments[index].commentReply[replyIndex]["userName"]} :",style:GoogleFonts.roboto(fontSize:12,fontWeight: FontWeight.w700,color: isReply?Colors.black:Colors.black)),
                          subtitle: Column(
                            children: [
                              Container(width: double.maxFinite,child: new RichText(text: TextSpan(
                                style:GoogleFonts.roboto(color: Colors.black,fontSize:14),
                                children: [
                                  TextSpan(
                                    text: ArrayComments[index].commentReply[replyIndex]["comment"],
                                  ),
                                  TextSpan(
                                    text: "\n\n  ",
                                  ),
                                  TextSpan(
                                    style:GoogleFonts.roboto(color: Colors.black,fontSize:10),
                                    text: formattedDate("dd-MM-yyyy HH:mm:ss", "dd MMM @ h:m a", ArrayComments[index].commentReply[replyIndex]["createdByDate"]),
                                  )
                                ]
                              ))),
                        //       new Container(
                        //   width: double.maxFinite,
                        //   child: Padding(
                        //     padding: const EdgeInsets.all(8.0),
                        //     child: new Row(
                        //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //       children: [
                        //         new Text("12:00 PM 12-01-2020",style:GoogleFonts.roboto()),
                        //         new Text("Reply",style:GoogleFonts.roboto(color:CupertinoColors.activeBlue))
                        //       ],
                        //     ),
                        //   ),
                        // ),
                            ],
                          ),
                            );
                          }).toList():[]
                        ),
                        new Divider(height: 3,)
                      ],
                    );
                  },
                ),
        ),
              ),
              isReply?new Divider(height: 4,color: Colors.black,):new Container(),
              isReply?Stack(
                children: [
                  new Container(
                    width: double.maxFinite,
                    child: new Padding(
                      padding: EdgeInsets.all(20),
                      child: new Text(isReplyText,style:GoogleFonts.roboto(fontSize: 16)),
                    ),
                  ),
                  Positioned(
                    right:5,
                    top:5,
                    child: InkWell(onTap:(){
                      setState(() {
                        isReply = false;
                        isReplyText ="";
                        parentCommentId = 0;
                      });
                    },child: Icon(Icons.close,size:20))
                  )
                ],
              ):new Container(),
              new Divider(height: 4,color: Colors.black),
              new Padding(
                padding: EdgeInsets.only(bottom:15,left:20,right:20),
                child: new Row(
                  children: [
                    Expanded(
                                        child: Container(
                                          child: TextField(
                                            focusNode: focusNode,
                                            minLines: 1,//Normal textInputField will be displayed
                                            maxLines: 5,
                                            keyboardType: TextInputType.multiline,
                                            style: GoogleFonts.roboto(),
                                            onChanged: (str) {
                                             
                                            },
                                            controller: commentTextCont,
                                            decoration: InputDecoration(
                                              contentPadding: EdgeInsets.fromLTRB(20,10,20,10),
                                                isDense: true,
                                                hintText: "Comment",
                                                border: OutlineInputBorder(
                                                  borderRadius: BorderRadius.circular(50)
                                                ),
                                                hintStyle: GoogleFonts.roboto(
                                                    color: Colors.grey)),
                                          ),
                                        ),
                                      ), 
                    new SizedBox(width: 10,),
                    new Container(
                      height:45,
                      child:new RaisedButton(
                        onPressed: (){
                          if(commentTextCont.text.isEmpty){
                            Dialogs.getInstance().showErrorAlert(context, "", "Please enter comment.",true);
  
                          }
                          else{
                            ApiComment(context, isReply, commentTextCont.text, int.parse(widget.storyId));
                          }
                        },
                        color:AppDefault_Green,
                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
                        child: Text("Post",style:GoogleFonts.roboto(color:Colors.white,fontWeight: FontWeight.w500,fontSize: 17))
                      )
                    )
                  ],
                )
              )
            ],
          )),
      ),
    );
  }
}

class CommentModel{
  String comment;
  int StoryId;
  bool isReply;
  String userName;
  String date;
  int commentId;
  int parentCommentId;
  String profilePath;
  List<dynamic> commentReply;

  CommentModel({this.comment,this.StoryId,this.commentId,this.commentReply,this.date,this.isReply,this.parentCommentId,this.profilePath,this.userName});

}

class ReplyCommentModel{
  String comment;
  int StoryId;
  bool isReply;
  String userName;
  String date;
  int commentId;
  int parentCommentId;
  String profilePath;

  ReplyCommentModel({this.comment,this.StoryId,this.commentId,this.date,this.isReply,this.parentCommentId,this.profilePath,this.userName});
}