

import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_ip/get_ip.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/NotificationHandler.dart';
import 'package:newapp/Common/ProgressBar.dart';
import 'package:newapp/Common/SocialLoginService.dart';
// import 'package:newapp/Common/SocialLoginService.dart';
import 'package:newapp/Common/dialogs.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/controllers/Dashboard/Dashboard.dart';
import 'package:newapp/controllers/Profile/editprofile.dart';


import 'OTPScreen.dart';


class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  var GoogleDetails;
  var FaceBookDetails;
  var mobNoCont = TextEditingController();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  

  Future<bool> validation(BuildContext context)async{
    if(mobNoCont.text.isEmpty){
      Dialogs.getInstance().showErrorAlert(context, "", "Please enter mobile no",true);
      return false;
    }
    if(mobNoCont.text.length != 10){
      Dialogs.getInstance().showErrorAlert(context, "", "Please enter valid mobile no",true);
      return false;
    }
    else{
      return true;
    }
  }

 userLogin(BuildContext context)async{
   Loader.getInstance().showOverlay(context);
   String webLink = ApiCallingHelper.baseUrl + "UserAuthentication/OtpVerification";
    Map<String,dynamic> params = {
         "MobileNumber":mobNoCont.text
    };
  print(params);

  Dio dio = Dio();

  var resp = await dio.post(webLink,data: params).then((value){

    var data = value.data;
    Loader.getInstance().dismissOverlay();

    var status = data["status"];
    var message = data["message"];
    if(status == true){
      setIntoDefault(UserDefault_OTP, data["response"]["otp"].toString());
      setIntoDefault(UserDefault_USERID, data["response"]["userId"].toString());
      setIntoDefault(UserDefault_IsRegister, data["response"]["isRegister"].toString());
    }

  }).catchError((e){
      print(e);
  });
 
   
 }

 var fcmToken = "";

 @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // mobNoCont.text = "8239115168";
    _firebaseMsgListener();
  }

  void _firebaseMsgListener() {
        // if (Platform.isIOS) iOS_Permission();

        _firebaseMessaging.getToken().then((token) {
          print(token);
          fcmToken = token;
        });

        Future.delayed(Duration(seconds: 1), () {
          _firebaseMessaging.configure(
            // onBackgroundMessage: myBackgroundMessageHandler,
            onMessage: (Map<String, dynamic> message) async {
              print(message);
              NotificationHandler().showNotify11(message);
              // Fimber.d("=====>on message $message");
              // Fluttertoast.showToast(msg: "onMessage $message");
            },
            onResume: (Map<String, dynamic> message) async {
              NotificationHandler().showNotify11(message);
            },
            onLaunch: (Map<String, dynamic> message) async {
              NotificationHandler().showNotify11(message);
            },
          );
        });
      }


  verifyOTP(BuildContext context)async{
    String ipAddress = await GetIp.ipAddress;
    String webLink = ApiCallingHelper.baseUrl +"UserAuthentication/login";
    var params = {
      "MobileNumber":"",
      // "Otp":widget.Otp,
      "platformId":Platform.isAndroid? "5": "4",
	    "iPAddress":ipAddress,
	    "deviceName":Platform.isAndroid?"Android":"iOS",
	    "appVersion":"2.0",
	    "FCMToken":fcmToken,
      "FBToken": "",
      "Email": GoogleDetails== null?"":GoogleDetails["email"],
      "UserName": GoogleDetails == null?"":GoogleDetails["name"],
      "ProfilePicturePath": GoogleDetails == null?"":GoogleDetails["image"],
      "UId":GoogleDetails == null?"":GoogleDetails["id"]
    };


    Dio dio = Dio();

  var resp = await dio.post(webLink,data: params).then((value)async{

    var data = value.data;
    Loader.getInstance().dismissOverlay();

    var status = data["status"];
    var message = data["message"];
    if(status == true){
      setIntoDefault(UserDefault_TOKEN, data["response"]["token"].toString());
      setIntoDefault(UserDefault_USERID, data["response"]["userId"].toString());
      setIntoDefault(UserDefault_IsLogin, "true");

      if(data["response"]["isProfileUpdated"] == false){
        Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
        Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>EditProfile(lat: position.latitude,long: position.longitude,screen:"OTP")),(Route<dynamic>route)=>false);
      }
      else{
        Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>Dashboard()),(Route<dynamic>route)=>false);
      }

      

    }

  }).catchError((e){

  });
    
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SafeArea(
        child: GestureDetector(
              onTapDown: (_){
                          FocusScope.of(context).requestFocus(FocusNode());
                        },
          child: Container(
            width: double.maxFinite,
            height: double.maxFinite,
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                   Container(
                     height: 250,
                     width: 250,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/AppLogo/PikNikTransparent.png")
                        )
                      ),
                   ),
                    SizedBox(
                      height: 10,
                    ),
                    _mobileNo(context),
                    SizedBox(height: 30,),
                   Container(
                     height: 40,
                     width: 120,
                     child: RaisedButton(
                       shape: RoundedRectangleBorder(
                         borderRadius: BorderRadius.circular(20)
                       ),
                      color: AppDefault_Green,
                      child: Text("Login",style: GoogleFonts.roboto(color: Colors.white,fontSize: 16,fontWeight: FontWeight.w600,letterSpacing: 1.2),),
                       onPressed: (){
                         SystemChannels.textInput.invokeMethod("textInput.hide");
                         validation(context).then((value){
                           if(value){
                             Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>OtpVerification(Otp: "",Mobile: mobNoCont.text, FCMToken: fcmToken,socialDetails: GoogleDetails,)));
                           }
                         });
                         
                        //  Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>OtpVerification()));
                       }),             ),
                       SizedBox(height:25),
                        Text("Or",textAlign: TextAlign.right,style: GoogleFonts.roboto(fontWeight: FontWeight.w700,fontSize: 20),),
                        SizedBox(height:10),
                        Text("Sign In/Up With",textAlign: TextAlign.right,style: GoogleFonts.roboto(fontWeight: FontWeight.w600,fontSize: 16),),
                        SizedBox(height:25),
                       Container(
                         width: double.maxFinite,
                         child: Center(
                           child: Row(
                             mainAxisAlignment: MainAxisAlignment.center,
                             crossAxisAlignment: CrossAxisAlignment.center,
                             children: [
                               InkWell(
                                 onTap: (){
                                   SocialLoginService.signInWithGoogle().then((value){
                                     log({
                                       "name": value.user.displayName,
                                       "email": value.user.email,
                                       "phone": value.user.phoneNumber,
                                       "image": value.user.photoURL,
                                       "id": value.user.uid
                                     }.toString());
                                     GoogleDetails = {
                                       "name": value.user.displayName,
                                       "email": value.user.email,
                                       "phone": value.user.phoneNumber,
                                       "image": value.user.photoURL,
                                       "id": value.user.uid
                                     };
                                     verifyOTP(context);
                                   });
                                 },
                                 child: new Container(
                                   height: 40,
                                   width: 40,
                                   child: new Image.asset("assets/Auth/Icon_GoogleButton.png"),
                                 ),
                               ),
                              //  SizedBox(width:30),
                              //  InkWell(
                              //    onTap: (){
                              //      SocialLoginService.signInWithFacebook().then((value){
                              //        log({
                              //          "name": value.user.displayName,
                              //          "email": value.user.email,
                              //          "phone": value.user.phoneNumber,
                              //          "image": value.user.photoURL,
                              //          "id": value.user.uid
                              //        }.toString());
                              //        GoogleDetails = {
                              //          "name": value.user.displayName,
                              //          "email": value.user.email,
                              //          "phone": value.user.phoneNumber,
                              //          "image": value.user.photoURL,
                              //          "id": value.user.uid
                              //        };
                              //        verifyOTP(context);
                              //      });
                              //    },
                              //    child: new Container(
                              //      height: 40,
                              //      width: 40,
                              //      child: new Image.asset("assets/Auth/Icon_FbButton.png"),
                              //    ),
                              //  ),
                             ],
                           ),
                         ),
                       )
                      //  Text("Forgot password?",textAlign: TextAlign.right,style: GoogleFonts.roboto(fontWeight: FontWeight.w600,fontSize: 16),)
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }


  _mobileNo(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20,10,20,10),
      child: Container(
        height: 45,
        child: Row(
          children: <Widget>[
            Container(
              width: 60,
              child: AbsorbPointer(
                child: Card(
                  elevation: 10,
                  child: TextField(
                    
                    style: GoogleFonts.roboto(fontSize:16),
                    // textAlign: TextAlign.center,
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(bottom:10,left: 10),
                      border: InputBorder.none,
                      hintText: "+91",
                      hintStyle: GoogleFonts.roboto(
                        
                      )
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: Card(
                elevation: 10,
                child: Center(
                  child: TextField(
                    controller: mobNoCont,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                    ],
                    keyboardType:  TextInputType.number,
                    cursorColor: Colors.green[900],
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(bottom:12,left: 10),
                      hintText: "Enter your number",
                      hintStyle: GoogleFonts.roboto()
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),);
  }

  
}
