import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_ip/get_ip.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lottie/lottie.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/ProgressBar.dart';
import 'package:newapp/Common/dialogs.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/Common/utils.dart';
import 'package:newapp/controllers/Dashboard/Dashboard.dart';
import 'package:newapp/controllers/Profile/editprofile.dart';
import 'package:sms_otp_auto_verify/sms_otp_auto_verify.dart';

class OtpVerification extends StatefulWidget{

  var Otp;
  var Mobile;
  var FCMToken;
  var socialDetails;
  OtpVerification({this.Otp,this.Mobile,this.FCMToken,this.socialDetails});
  @override
  State<StatefulWidget> createState() {
    return OtpVerificationState();
  }
}


class OtpVerificationState extends State<OtpVerification>{
  
  TextEditingController textEditingController = TextEditingController();
  String currentText = "";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  int _otpCodeLength = 6;
  bool _isLoadingButton = false;
  bool _enableButton = false;
  String _otpCode = "";



  FirebaseAuth _auth;

  UserCredential result;

  var VerificationId;
  var FBToken = "";


  verifyOTP(BuildContext context)async{
    String ipAddress = await GetIp.ipAddress;
    String webLink = ApiCallingHelper.baseUrl +"UserAuthentication/login";
    var params = {
      "MobileNumber":widget.Mobile == null? "": widget.Mobile,
      // "Otp":widget.Otp,
      "platformId":Platform.isAndroid? "5": "4",
	    "iPAddress":ipAddress,
	    "deviceName":Platform.isAndroid?"Android":"iOS",
	    "appVersion":"2.0",
	    "FCMToken":widget.FCMToken,
      "FBToken": FBToken,
      "Email": widget.socialDetails == null?"":widget.socialDetails["email"],
      "UserName": widget.socialDetails == null?"":widget.socialDetails["name"],
      "ProfilePicturePath": widget.socialDetails == null?"":widget.socialDetails["image"],
      "UId":widget.socialDetails == null?"":widget.socialDetails["id"]
    };


    Dio dio = Dio();

  var resp = await dio.post(webLink,data: params).then((value)async{

    var data = value.data;
    Loader.getInstance().dismissOverlay();

    var status = data["status"];
    var message = data["message"];
    if(status == true){
      setIntoDefault(UserDefault_TOKEN, data["response"]["token"].toString());
      setIntoDefault(UserDefault_USERID, data["response"]["userId"].toString());
      setIntoDefault(UserDefault_IsLogin, "true");

      if(data["response"]["isProfileUpdated"] == false){
        Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
        Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>EditProfile(lat: position.latitude,long: position.longitude,screen:"OTP")),(Route<dynamic>route)=>false);
      }
      else{
        Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>Dashboard()),(Route<dynamic>route)=>false);
      }

      

    }

  }).catchError((e){

  });
    
  }

  String smsOTP;    
    String _verificationId;    
    String errorMessage = '';   


  

  Future<bool> loginUser(String phone, BuildContext context) async{
    
    _auth.verifyPhoneNumber(
        phoneNumber: "+91"+phone,
        timeout: Duration(seconds: 60),
        verificationCompleted: (AuthCredential credential) async{

          result = await _auth.signInWithCredential(credential);

          User user = result.user;

          print(_auth.currentUser.refreshToken);

          if(user != null){
           
          }else{
            print("Error");
          }

          //This callback would gets called when verification is done auto maticlly
        },

        verificationFailed: (FirebaseAuthException exception){
          print(exception.message);
        },
        codeSent: (String verificationId, [int forceResendingToken]){
          VerificationId = verificationId;
        },
        codeAutoRetrievalTimeout: (String verificationId){}
    );
  }

  _getSignatureCode() async {
    String signature = await SmsRetrieved.getAppSignature();
    print("signature $signature");
  }

  _onSubmitOtp() {
    setState(() {
      _isLoadingButton = !_isLoadingButton;
      _verifyOtpCode();
    });
  }

  AuthCredential _phoneAuthCredential;


  Future<void> _submitPhoneNumber(String phone, BuildContext context) async {
    /// NOTE: Either append your phone number country code or add in the code itself
    /// Since I'm in India we use "+91 " as prefix `phoneNumber`
    String phoneNumber = "+91 " + phone;
    print(phoneNumber);

    /// The below functions are the callbacks, separated so as to make code more readable
    void verificationCompleted(AuthCredential phoneAuthCredential) {
      print('verificationCompleted');
      this._phoneAuthCredential = phoneAuthCredential;
      print(phoneAuthCredential);
    }

    void verificationFailed(FirebaseAuthException error) {
      print(error);
    }

    void codeSent(String verificationId, [int code]) {
      print('codeSent');
      _verificationId = verificationId;
    }

    void codeAutoRetrievalTimeout(String verificationId) {
      print('codeAutoRetrievalTimeout');
    }

    await FirebaseAuth.instance.verifyPhoneNumber(
      /// Make sure to prefix with your country code
      phoneNumber: phoneNumber,

      /// `seconds` didn't work. The underlying implementation code only reads in `milliseconds`
      timeout: Duration(milliseconds: 110000),

      /// If the SIM (with phoneNumber) is in the current device this function is called.
      /// This function gives `AuthCredential`. Moreover `login` function can be called from this callback
      verificationCompleted: verificationCompleted,

      /// Called when the verification is failed
      verificationFailed: verificationFailed,

      /// This is called after the OTP is sent. Gives a `verificationId` and `code`
      codeSent: codeSent,

      /// After automatic code retrival `tmeout` this function is called
      codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
    ); // All the callbacks are above
  }

  void _submitOTP() {
    /// get the `smsCode` from the user
    String smsCode = _otpCode;

    /// when used different phoneNumber other than the current (running) device
    /// we need to use OTP to get `phoneAuthCredential` which is inturn used to signIn/login
    this._phoneAuthCredential = PhoneAuthProvider.credential(
        verificationId: this._verificationId, smsCode: smsCode);

    _login();
  }

  Future<void> _login() async {
    /// This method is used to login the user
    /// `AuthCredential`(`_phoneAuthCredential`) is needed for the signIn method
    /// After the signIn method from `AuthResult` we can get `FirebaserUser`(`_firebaseUser`)
    try {
      await FirebaseAuth.instance
          .signInWithCredential(this._phoneAuthCredential)
          .then((UserCredential authRes) {
        Loader.getInstance().dismissOverlay();

        authRes.user.getIdTokenResult().then((value){
                                      FBToken = value.token;
                                      print(value.token);
                                      verifyOTP(context);
                                    }); 
      });
    } catch (e) {
      Loader.getInstance().dismissOverlay();
      print(e.message);
      if(e.code == "invalid-verification-code"){
        Dialogs.getInstance().showErrorAlert(context, "", "OTP not matched. Try Again.",true);
      }
      else{
        Dialogs.getInstance().showErrorAlert(context, "", "Authentication error, kindly try login again.",true);
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getSignatureCode();
    _auth = FirebaseAuth.instance;
    Future.delayed(Duration.zero,(){
      _submitPhoneNumber(widget.Mobile, context);
    });
    // textEditingController.text = "1234";
  }


  _onOtpCallBack(String otpCode, bool isAutofill) {
    setState(() {
      this._otpCode = otpCode;
      if (otpCode.length == _otpCodeLength && isAutofill) {
        _enableButton = false;
        _isLoadingButton = true;
        _verifyOtpCode();
      } else if (otpCode.length == _otpCodeLength && !isAutofill) {
        _enableButton = true;
        _isLoadingButton = false;
      } else {
        _enableButton = false;
      }
    });
  }

  _verifyOtpCode() {
    FocusScope.of(context).requestFocus(new FocusNode());
    Timer(Duration(milliseconds: 4000), () {
      setState(() {
        _isLoadingButton = false;
        _enableButton = false;
      });

      // _scaffoldKey.currentState.showSnackBar(
      //     SnackBar(content: Text("Verification OTP Code $_otpCode Success")));
    });
  }
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
        body: new Container(
          width: double.maxFinite,
          height: double.maxFinite,
          child: SafeArea(
            child: GestureDetector(
              onTapDown: (_){
                          FocusScope.of(context).requestFocus(FocusNode());
                        },
              child: new Padding(
                padding: EdgeInsets.all(20),
                child: Center(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(height: screenAwareHeight(200, context),child: Lottie.asset(OtpLottie,repeat: true)),

                        new SizedBox(height: screenAwareHeight(50, context),),
                        
                        new Text("Verify Mobile Number",style: GoogleFonts.roboto(fontSize: screenAwareHeight(25, context),fontWeight: FontWeight.w500)),

                        new SizedBox(height: screenAwareHeight(15, context),),

                        new Text("Enter 6 digits verification code sent to ${widget.Mobile}",textAlign: TextAlign.center,style: GoogleFonts.roboto(fontSize: 16,fontWeight: FontWeight.w400)),

                        new SizedBox(height: screenAwareHeight(50, context),),

                        TextFieldPin(
                          
                  filled: true,
                  filledColor: Colors.grey[350],
                  codeLength: _otpCodeLength,
                  boxSize: 40,
                  filledAfterTextChange: true,
                  textStyle: GoogleFonts.poppins(fontSize: 14,fontWeight: FontWeight.w500 ),
                  borderStyeAfterTextChange: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      
                      ),
                  borderStyle: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      
                      ),
                  onOtpCallback: (code, isAutofill) =>
                      _onOtpCallBack(code, true),
                ),

                        /*Padding(
                          padding: EdgeInsets.fromLTRB(screenAwareHeight(20, context),0,screenAwareHeight(20, context),0),
                          child: PinCodeTextField(
                            textInputType: TextInputType.number,
                            length: 6,
                            obsecureText: false,
                            animationType: AnimationType.fade,
                            pinTheme: PinTheme(
                              shape: PinCodeFieldShape.underline,
                              borderRadius: BorderRadius.circular(5),
                              fieldHeight: 50,
                              fieldWidth: 40,
                              selectedColor: Colors.grey,
                              activeColor: Colors.grey,
                              inactiveColor: Colors.grey,
                              activeFillColor: Colors.transparent,
                              inactiveFillColor: Colors.transparent,
                              selectedFillColor: Colors.transparent,
                            ),
                            backgroundColor: Colors.transparent,
                            animationDuration: Duration(milliseconds: 300),
                            enableActiveFill: true,
                            controller: textEditingController,
                            onCompleted: (v) {
                              print("Completed");
                            },
                            onChanged: (value) {
                              print(value);
                              setState(() {
                                currentText = value;
                              });
                            },
                            beforeTextPaste: (text) {
                              print("Allowing to paste $text");
                              //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                              //but you can show anything you want here, like your pop up saying wrong paste format or etc
                              return true;
                            },
                          ),
                        ),*/

                        new SizedBox(height: screenAwareHeight(20, context),),

                        InkWell(onTap: (){
                          loginUser(widget.Mobile, context);
                        },child: new Text("Resend",style: GoogleFonts.roboto(fontSize: 16,fontWeight: FontWeight.w500,decoration: TextDecoration.underline))),

                        new SizedBox(height: screenAwareHeight(70, context),),

                        new Container(
                          width: double.maxFinite,
                          child: new Container(
                            height: 40,
                            child: new RaisedButton(
                              elevation: 15,
                              onPressed: ()async{
                                if(_otpCode.isNotEmpty){
                                  Loader.getInstance().showOverlay(context);
                                  _submitOTP();
                                  // try {
                                  //   Loader.getInstance().showOverlay(context);
                                  // // verifyOTP(context);
                                  // final code = _otpCode.trim();
                                  // AuthCredential credential = PhoneAuthProvider.credential(verificationId: VerificationId, smsCode: code);

                                  // UserCredential result = await _auth.signInWithCredential(credential);

                                  // User user = result.user;

                                  // if(user != null){
                                  //   user.getIdTokenResult().then((value){
                                  //     FBToken = value.token;
                                  //     print(value.token);
                                  //     verifyOTP(context);
                                  //   });
                                  //   print(_auth.currentUser.refreshToken);
                                  // }else{
                                  //   print("Error");
                                  //   Dialogs.getInstance().showErrorAlert(context, "", "OTP not matched. Try Again");
                                  // }
                                  // } catch (err) {
                                  //   print(err.toString());
                                  //   Loader.getInstance().dismissOverlay();
                                  //   Dialogs.getInstance().showErrorAlert(context, "", "Authentication error, kindly try login again.");
                                  // }
                                }
                                else{
                                  Dialogs.getInstance().showErrorAlert(context, "", "Please Enter OTP.",true);
                                }
                              },
                              color: AppDefault_Green,
                              child: new Text("VERIFY",style: GoogleFonts.roboto(fontSize: screenAwareHeight(18, context),fontWeight: FontWeight.w500,textStyle: TextStyle(color: Colors.white))),
                            ),
                          ),
                        ),
                        
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
  }
}