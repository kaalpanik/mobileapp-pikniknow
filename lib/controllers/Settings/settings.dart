import 'dart:convert';
import 'dart:developer';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:signalr_client/signalr_client.dart';

class Settings extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SettingsState();
  }
}

class SettingsState extends State<Settings>{
  var isDarkModeEnable = false;
  var isNotificationEnable = false;


   String _serverUrl = "http://travelappapi-dev.ap-south-1.elasticbeanstalk.com/eventhub";
    HubConnection _hubConnection;

    bool get connectionIsOpen => _connectionIsOpen;
    bool _connectionIsOpen;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    openChatConnection();
  
  }


  Future<void> openChatConnection() async {
    if (_hubConnection == null) {
      _hubConnection = HubConnectionBuilder().withUrl(_serverUrl).build();
      _hubConnection.onclose((error){
        print(error);
      });
      _hubConnection.on("StoryComment", _handleIncommingChatMessage);
    }

    if (_hubConnection.state != HubConnectionState.Connected) {
      await _hubConnection.start();
    }
  }

  void _handleIncommingChatMessage(List<Object> args){
    log(args.toString());
  }


 

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Settings",style: GoogleFonts.roboto(color: Colors.black),),
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: InkWell(
          onTap: (){
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back,color: Colors.black,)),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text("Dark Mode",style: GoogleFonts.roboto(color: Colors.black,fontWeight: FontWeight.w600,)),
                trailing: CupertinoSwitch(
                  value: isDarkModeEnable,
                  activeColor: AppDefault_Green,
                 onChanged: (value){
                  setState(() {
                    isDarkModeEnable = value;
                  });
                }),
              ),
              Divider(
                height:1
              ),
                ListTile(
                title: Text("Notifications",style: GoogleFonts.roboto(color: Colors.black,fontWeight: FontWeight.w600,)),
                trailing: CupertinoSwitch(
                  value: isNotificationEnable,
                  activeColor: AppDefault_Green,
                 onChanged: (value){
                  setState(() {
                    isNotificationEnable = value;
                  });
                }),
              ),
              Divider(
                height:1
              ),
                ListTile(
                title: Text("Share travel app",style: GoogleFonts.roboto(color: Colors.black,fontWeight: FontWeight.w600,)),
                trailing: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 8, 10, 8),
                  child: Icon(Icons.share,color: AppDefault_Green,),
                )
              ),
              SizedBox(height:30),
              Container(
                height: 40,
                width:double.maxFinite,
                child: Center(child: Text("About Us",style: GoogleFonts.roboto(color: Colors.black,fontWeight: FontWeight.w700,))),
              ),
              ListTile(
                title: Text("Privacy Policy",style: GoogleFonts.roboto(color: Colors.black,fontWeight: FontWeight.w600,)),
              ),
              Divider(
                height: 1,
              ),
               ListTile(
                title: Text("Terms & Conditions",style: GoogleFonts.roboto(color: Colors.black,fontWeight: FontWeight.w600,)),
              ),
            ],
          ),
        )
      ),
    );
  }
}