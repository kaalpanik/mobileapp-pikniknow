import 'dart:async';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/Common/utils.dart';
import 'package:newapp/controllers/auth/LoginScreen.dart';

class Onboarding extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return OnboardingState();
  }
}

class OnboardingState extends State<Onboarding> {
  int _current = 0;

  Timer _timer;

  var image;
  
                



  var i = 0;

  @override
  void initState() {
    super.initState();
    image = [Image.asset("assets/Onboarding/1.png",fit: BoxFit.contain,), Image.asset("assets/Onboarding/2.png",fit: BoxFit.contain,), Image.asset("assets/Onboarding/3.png",fit: BoxFit.contain,)];
   
    
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    precacheImage(image[0].image, context);
    precacheImage(image[1].image, context);
    precacheImage(image[2].image, context);
  }

  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.maxFinite,
        width: double.maxFinite,
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            CarouselSlider.builder(

                              itemCount: image.length,
                              itemBuilder: (BuildContext context, int itemIndex) =>
                                  GestureDetector(
                                    onTap: (){
                                      
                                    },
                                    child: Container(
                                        // margin: EdgeInsets.only(left:  ScreenConfig().screenAwareWidth(10, context),
                                        //     right:  ScreenConfig().screenAwareWidth(10, context),
                                        //     bottom:  ScreenConfig().screenAwareWidth(3, context)),
                                        decoration: new BoxDecoration(
                                          // color: Colors.white,
                                          borderRadius: BorderRadius.circular( screenAwareWidth(5, context)),
                                        ),
                                        child: Container(
                                          height: double.maxFinite,
                                          width: double.maxFinite,

                                          child: image[itemIndex],
                                        )
                                    ),
                                  ),
                              options: CarouselOptions(
                                
                                height: double.maxFinite,
                                initialPage: 0,
                                scrollPhysics: BouncingScrollPhysics(),
                                viewportFraction: 1.0,
                                enableInfiniteScroll: true,
                                // reverse: false,
                                enlargeCenterPage: false,
                                autoPlay: true,
                                autoPlayInterval: Duration(seconds: 3),
                                // autoPlayCurve: Curves.fastOutSlowIn,
                                // pauseAutoPlayOnTouch: true,
                                onPageChanged: (index,reason) {
                                  setState(() {
                                    _current = index;
                                  });
                                },
                                scrollDirection: Axis.horizontal,
                              ),
                            ),
            Positioned(
              bottom: MediaQuery.of(context).padding.bottom + 50,
              left: 0,
              right: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [1, 2, 3].map((url) {
                  int index = [1, 2, 3].indexOf(url);
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index
                          ? AppDefault_Green
                          : Color.fromRGBO(0, 0, 0, 0.4),
                    ),
                  );
                }).toList(),
              ),
            ),
            Positioned(
                bottom: MediaQuery.of(context).padding.bottom + 10,
                left: 0,
                right: 0,
                child: new Container(
                  height: 45,
                  width: double.maxFinite,
                  child: new Center(
                      child: RaisedButton(
                    color: AppDefault_Green,
                    onPressed: () {
                      setIntoDefault(UserDefault_IsOnboard, "true");
                      Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>LoginScreen()), (route) => false);
                    },
                    child: new Text(
                      "Get Started",
                      style: GoogleFonts.montserrat(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)),
                  )),
                ))
          ],
        ),
      ),
    );
  }
}


class firstWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
                      height: double.maxFinite,
                      width: double.maxFinite,
                      child: Image.asset("assets/Splash/dark-album1.jpg",fit: BoxFit.cover,),
                    );
  }
}

class secondWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
                      height: double.maxFinite,
                      width: double.maxFinite,
                      child: Image.asset("assets/Splash/dark-album2.jpg",fit: BoxFit.cover,),
                    );
  }
}

class thirdWidget extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
                      height: double.maxFinite,
                      width: double.maxFinite,
                      child: Image.asset("assets/Splash/dark-album3.jpg",fit: BoxFit.cover,),
                    );
  }
}

