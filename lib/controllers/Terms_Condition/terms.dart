import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/extention.dart';

class Terms extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return TermState();
  }
}

class TermState extends State<Terms>{

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero,(){
      apiContactUs(context);
    });
  }

  var termsDetails;

  apiContactUs(BuildContext context)async{
    var token = await getFromUserDefault(UserDefault_TOKEN);
    var webLink = ApiManager.BaseURL + "Page/GetByPageLocationId/15";

    ApiManager().callGetApi(context, webLink, true, token).then((data){
      if(data["status"] == true){
        setState(() {
          termsDetails = data["response"];
        });
      }  
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: new IconButton(icon: Icon(Icons.arrow_back_ios,color:Colors.black), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: new Text("Terms & Conditions",style:GoogleFonts.roboto(color:Colors.black))
      ),
      body: new Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: new SingleChildScrollView(
          child:new Padding(
            padding: EdgeInsets.all(20),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Text(termsDetails == null? "":termsDetails["heading"],style:GoogleFonts.roboto(color:Colors.black,fontSize: 20,fontWeight: FontWeight.bold)),
                new SizedBox(height: 20,),
                new Text(termsDetails == null? "":termsDetails["title"],style:GoogleFonts.roboto(color:Colors.black,fontSize: 17,fontWeight: FontWeight.w600)),
                new SizedBox(height: 20,),
                new Text(termsDetails == null? "":termsDetails["description"],style:GoogleFonts.roboto(color:Colors.black,fontSize: 17)),
              ],
            )
          )
        ),
      ),
    );
  }
  
}