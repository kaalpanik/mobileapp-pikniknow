import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/extention.dart';

class AboutUs extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return AboutState();
  }
}

class AboutState extends State<AboutUs>{

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero,(){
      apiContactUs(context);
    });
  }

  var aboutDetails;

  apiContactUs(BuildContext context)async{
    var token = await getFromUserDefault(UserDefault_TOKEN);
    var webLink = ApiManager.BaseURL + "Page/GetByPageLocationId/14";

    ApiManager().callGetApi(context, webLink, true, token).then((data){
      if(data["status"] == true){
        setState(() {
          aboutDetails = data["response"];
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        leading: new IconButton(icon: Icon(Icons.arrow_back_ios,color:Colors.black), onPressed: (){
          Navigator.of(context).pop();
        }),
        title: new Text("About Us",style:GoogleFonts.roboto(color:Colors.black))
      ),
      body: new Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: new SingleChildScrollView(
          child:new Padding(
            padding: EdgeInsets.all(20),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Text(aboutDetails == null? "":aboutDetails["heading"],style:GoogleFonts.roboto(color:Colors.black,fontSize: 20,fontWeight: FontWeight.bold)),
                new SizedBox(height: 20,),
                new Text(aboutDetails == null? "":aboutDetails["title"],style:GoogleFonts.roboto(color:Colors.black,fontSize: 17,fontWeight: FontWeight.w600)),
                new SizedBox(height: 20,),
                new Text(aboutDetails == null? "":aboutDetails["description"],style:GoogleFonts.roboto(color:Colors.black,fontSize: 17)),
              ],
            )
          )
        ),
      ),
    );
  }
  
}