import 'dart:async';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:newapp/Common/Constant.dart';


class SearchMap extends StatefulWidget {
  double lat;
  double long;
  SearchMap({this.lat, this.long});
  @override
  State<StatefulWidget> createState() {
    return SearchMapState();
  }
}

class SearchMapState extends State<SearchMap> {
  final homeScaffoldKey = GlobalKey<ScaffoldState>();

  Completer<GoogleMapController> _controller = Completer();

  CameraPosition _kGooglePlex;

  CameraPosition _kLake ;


  //Places Api
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: "AIzaSyB0wVEE6PvMyYd-NdTxYOr2Q7KHZnF0Xek");

  var latitude ;
  var longitude ;


  void onError(PlacesAutocompleteResponse response) {
    homeScaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

  Future<void> _handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      
      apiKey: "AIzaSyB0wVEE6PvMyYd-NdTxYOr2Q7KHZnF0Xek",
      onError: onError,
      mode: Mode.fullscreen,
      // language: "in",
      // components: [Component(Component.country, "in")],
    );

    displayPrediction(p, homeScaffoldKey.currentState);
  }

Future<Null> displayPrediction(Prediction p, ScaffoldState scaffold) async {
  if (p != null) {
    // get detail (lat/lng)
    PlacesDetailsResponse detail = await _places.getDetailsByPlaceId(p.placeId);
    final lat = detail.result.geometry.location.lat;
    final lng = detail.result.geometry.location.lng;

    latitude = lat;
    longitude = lng;

    // _kGooglePlex = CameraPosition(
    //   target: LatLng(lat, lng),
    //   zoom: 14.4746,
    // );

    _kLake = CameraPosition(
      target: LatLng(lat, lng),
      zoom: 14.151926040649414);

    _goToTheLocation();

    
  }
}

Future<void> _goToTheLocation() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }


  @override
  void initState() {
    _kGooglePlex = CameraPosition(
      target: LatLng(widget.lat, widget.long),
      zoom: 14.4746,
    );
    super.initState();

    _kLake = CameraPosition(
      target: LatLng(widget.lat, widget.long),
      zoom: 19.151926040649414);

    latitude = widget.lat;
    longitude = widget.long;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: homeScaffoldKey,
      body: new Container(
        height: double.maxFinite,
        width: double.maxFinite,
        child: Stack(
          children: [
            GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: _kGooglePlex,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
              onCameraMove: (pos){
                latitude = pos.target.latitude;
                longitude = pos.target.longitude;
              },
              myLocationEnabled:  true,
              zoomControlsEnabled: false,
              myLocationButtonEnabled: false,
            ),
            new Center(
              child: new Container(
                height: 35,width:35,
                child: Image.asset("assets/marker.png"),
              )
            ),
            new SafeArea(
              child: new Padding(
                padding: EdgeInsets.all(10),
                child: new Container(
                  height: 60,
                  width: double.maxFinite,
                  child: Row(
                    children: [
                      Expanded(
                        child: Card(
                          elevation: 20,
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children:[
                              new Padding(
                                padding: EdgeInsets.only(left:0,right: 2),
                                child: IconButton(
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                  icon: Icon(Icons.arrow_back_ios),
                                )
                              ),
                              new Expanded(
                                child: InkWell(
                                  onTap: _handlePressButton,
                                  child: new Padding(
                                    padding: EdgeInsets.fromLTRB(0, 10, 15, 10),
                                    child: Text(
                                      "Search Place",
                                      style: GoogleFonts.roboto(
                                        fontSize: 16
                                      )
                                    )
                                  ),
                                )
                              )
                            ]
                          ),
                        ),
                      ),

                      new Container(
                  height:45,
                  child:Padding(
                    padding:EdgeInsets.only(left:8,right:8),
                    child: RaisedButton(onPressed: (){
                      Navigator.of(context).pop({"latitude":latitude,"longitude": longitude});
                    },padding: EdgeInsets.all(0),child:new Text("DONE",style:GoogleFonts.roboto(color:Colors.white,fontSize:14)),color:AppDefault_Green,elevation: 10,)
                  )
                ),
                    ],
                  ),
                ),
              ),
            ),
            // new SafeArea(child: new Column(
            //   children: [
            //     new Expanded(child:Container()),
            //     new Container(
            //       height:45,
            //       width:double.maxFinite,
            //       child:Padding(
            //         padding:EdgeInsets.only(left:20,right:20),
            //         child: RaisedButton(onPressed: (){
            //           Navigator.of(context).pop({"latitude":latitude,"longitude": longitude});
            //         },child:new Text("DONE",style:GoogleFonts.roboto(color:Colors.white,fontSize:16)),color:AppDefault_Green,elevation: 10,)
            //       )
            //     ),
            //     new SizedBox(height: 15,)
            //   ],
            // ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _currentLocation,
        // label: Text('My Location'),
        backgroundColor: CupertinoColors.systemGrey6,
        child: new Icon(Icons.my_location,color: CupertinoColors.systemGrey,),
      ),
    );
  }

  void _currentLocation() async {
    Position position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
   _kLake = CameraPosition(
      target: LatLng(position.latitude, position.longitude),
      zoom: 14.151926040649414);

    _goToTheLocation();
  }
}
