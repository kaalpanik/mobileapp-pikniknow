import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:newapp/Common/ApiCallingHelper.dart';
import 'package:newapp/Common/Constant.dart';
import 'package:newapp/Common/ProgressBar.dart';
import 'package:newapp/Common/dialogs.dart';
import 'package:newapp/Common/extention.dart';
import 'package:newapp/Common/multi_manager/flick_multi_manager.dart';
import 'package:newapp/Common/multi_manager/flick_multi_player.dart';
import 'package:newapp/Common/multi_manager/flick_multi_player_file.dart';
import 'package:newapp/controllers/Dashboard/Dashboard.dart';
import 'package:newapp/controllers/Story/searchMap.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:visibility_detector/visibility_detector.dart';




class AddStory extends StatefulWidget {
  double lat;
  double long;
  AddStory({this.lat,this.long});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AddStoryState();
  }
}

class AddStoryState extends State<AddStory> {

  final homeScaffoldKey = GlobalKey<ScaffoldState>();

  var imageArray = [];
  var videoArray = [];
  var tagArray = [];
  var tagsNames = [];
  bool addTag = false;

  TextEditingController addTagCont = TextEditingController();

  TextEditingController titleCont = TextEditingController();
     TextEditingController addressCont = TextEditingController();
     TextEditingController addressCont2 = TextEditingController();
     TextEditingController locationNameCont = TextEditingController();
     TextEditingController cityCont = TextEditingController();
     TextEditingController stateCont = TextEditingController();
     TextEditingController countryCont = TextEditingController();
     TextEditingController metaDescCont = TextEditingController();
     TextEditingController spotInfoCont = TextEditingController();
     TextEditingController postalCodeCont = TextEditingController();
     TextEditingController getThereCont = TextEditingController();
     TextEditingController selectLocationCont = TextEditingController();

      DateTime createdDate = DateTime.now();

  Position position;

  Completer<GoogleMapController> _controller = Completer();

  CameraPosition _kGooglePlex;

  var token = "";
  var userId = "";

  FlickMultiManager flickMultiManager;

  var selectTagArray = [];

  var lat;
  var long;

  CameraPosition _kLake ;
  

  @override
  void initState() {
    _kGooglePlex = CameraPosition(
      target: LatLng(widget.lat, widget.long),
      zoom: 14.4746,
    );

    _kLake = CameraPosition(
      target: LatLng(widget.lat, widget.long),
      zoom: 14.4746);

    super.initState();

    lat = widget.lat;
    long = widget.long;

    flickMultiManager = FlickMultiManager();

    getFromUserDefault(UserDefault_TOKEN).then((t){
      token = t;
      apiGetGeneric(context);
    });

    getFromUserDefault(UserDefault_USERID).then((id){
      userId = id;
    });
    
    
  }

  pickImages()async{
    FilePickerResult result = await FilePicker.platform.pickFiles(
          type: FileType.image,
          allowCompression: true,
          allowMultiple: true,
        );
    setState(() {
      if(imageArray.length < 10){
        for(var image in result.files){
          imageArray.add(File(image.path));
        }
      }
      
    });
  }

  pickVideos()async{
    FilePickerResult result = await FilePicker.platform.pickFiles(
          type: FileType.video,
          allowCompression: true,
          allowMultiple: true,
        );
    setState((){
      if(videoArray.length < 2){
        for(var video in result.files){
          videoArray.add(File(video.path));
        }
      }
    });


  }

apiGetGeneric(BuildContext context){
  String webLink = ApiCallingHelper.baseUrl + "Lookups";

  ApiCallingHelper().callGetApiAuthorized(context, webLink,token).then((value){
    var data = jsonDecode(value);
    var status = data["status"];
    if(status){
      for(var tag in jsonDecode(data["response"])[0]["tags"]){
        setState(() {
          tagArray.add(tag);
          tagsNames.add(tag["Name"]);
        });
      }
      print(tagArray);
    }
    else{

    }
  }).catchError((e){
    print(e);
  });
}

//TagId
//Name
//StatusId

apiPostStory(BuildContext context)async{
  Loader.getInstance().showOverlay(context);
  String webLink = ApiCallingHelper.baseUrl + "Story";

  List<StoryMediaModel> StoryMedias = [];

  for(var image in imageArray){
    var index = imageArray.indexOf(image);
    String fileName = image.path.split('/').last;
    StoryMedias.add(StoryMediaModel(StoryMediaId: "0",StoryId: "0",StoryMediaPath: await MultipartFile.fromFile(image.path,filename: fileName),FilePath: "",FileTypeId: "6",IsPrimaryMedia: index ==0?"true":"false"));

  }

  for(var video in videoArray){
    String fileName = video.path.split('/').last;
    StoryMedias.add(StoryMediaModel(StoryMediaId: "0",StoryId: "0",StoryMediaPath: await MultipartFile.fromFile(video.path,filename: fileName),FilePath: "",FileTypeId: "7",IsPrimaryMedia:"false"));
  }



  Map<String,dynamic> other = {
    "StoryId":"0",
	    "Title":titleCont.text,
	    "Latitude":lat.toString(),
	    "Longitude":long.toString(),
	    "SpotInformation":spotInfoCont.text,
	    "CreatedByDate":"12-01-2020",
	    "CreatedById":userId,
	    "StatusId":"1",
	    "TotalLike":"0",
	    "TotalShare":"0",
      // "AddressLine1": addressCont.text,
      // "AddressLine2":addressCont2.text,
      "City":cityCont.text,
      "State":stateCont.text,
      "Country":countryCont.text,
      "PostalCode":postalCodeCont.text,
      "LocationName":locationNameCont.text,
      "MetaDescription":metaDescCont.text,
	    
	    "TotalView":"0",
  };

  

  for(var m in StoryMedias){
    var index = StoryMedias.indexOf(m);
    other["StoryMedias[$index].StoryMediaPath"] = m.StoryMediaPath;
    other["StoryMedias[$index].StoryMediaId"] = m.StoryMediaId;
    other["StoryMedias[$index].StoryId"] = m.StoryId;
    other["StoryMedias[$index].FilePath"] = m.FilePath;
    other["StoryMedias[$index].FileTypeId"] = m.FileTypeId;
    other["StoryMedias[$index].IsPrimaryMedia"] = m.IsPrimaryMedia;
  }

  for(var tag in selectTagArray){
    var index = selectTagArray.indexOf(tag);
    var tagId = "0";
    var tagName = tag;
    for(var alltag in tagArray){
      if(tag == alltag["Name"]){
        tagId = alltag["TagId"].toString();
        tagName = tag;
      }
    }
    other["StoryTags[$index].StoryTagId"] = "0";
    other["StoryTags[$index].TagId"] = tagId;
    other["StoryTags[$index].StoryId"] = "0";
    other["StoryTags[$index].Name"] = tagName;
  }

  


  print(other);
  FormData params = FormData.fromMap(other);

  Dio dio = new Dio();

  var resp = await dio.post(webLink, data: params,options: Options(headers: {"Authorization": token})).catchError((e){
    print(e);
  });
  Loader.getInstance().dismissOverlay();

    var data =  resp.data;

    var status = data["status"];
    var message = data["message"];
    if(status == true){

      Alert(
      context: context,
      type: AlertType.success,
      title: "Successful",
      desc: "Thank you for submitting story. Your story will be approved and posted within 24 hours",
      style: AlertStyle(
        isOverlayTapDismiss: false,
      ),
      closeFunction: (){},
      closeIcon: Icon(Icons.close,size:0,color:Colors.transparent),
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: (){
            Navigator.of(context).pop();
                   Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>Dashboard()),(Route<dynamic> route)=>false);
          },
          color: AppDefault_Green,
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();


      // showDialog(
      //   barrierDismissible: false,
      //   context: context,
      //   builder: (BuildContext context){

      //     if(Platform.isIOS){
      //                       return CupertinoAlertDialog(
                              
      //                       content: new Text("CONGRATULATIONS - Your story will be approved soon",textScaleFactor: 1, style: GoogleFonts.roboto(),),
      //                       actions: <Widget>[
      //                         new CupertinoButton(
      //                           child: new Text("OK",textScaleFactor: 1, style: GoogleFonts.roboto(),),
      //                           onPressed: (){
      //              Navigator.of(context).pop();
      //              Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>Dashboard()),(Route<dynamic> route)=>false);
      //           },
      //                         )
      //                       ],
      //                     );
      //                    }
      //                    else{
      //                      return AlertDialog(
      //                        content: new Text("CONGRATULATIONS - Your story will be approved soon",textScaleFactor: 1, style: GoogleFonts.roboto(),),
      //                        actions: [
      //                          new RaisedButton(
      //                           child: new Text("OK",textScaleFactor: 1, style: GoogleFonts.roboto(color: Colors.white),),
      //                           onPressed: (){
      //              Navigator.of(context).pop();
      //              Navigator.of(context).pushAndRemoveUntil(CupertinoPageRoute(builder: (context)=>Dashboard()),(Route<dynamic> route)=>false);
      //           },
      //                           color: AppDefault_Green,
      //                         )
      //                        ],
      //                      );
      //                    }

         
      //   }
      // );
    }    
  }

  Future<bool> validation(BuildContext context) async{
    if(titleCont.text.isEmpty){
      Dialogs.getInstance().showErrorAlert(context, "", "Please enter title for story",true);
      return false;
    }
    else if(locationNameCont.text.isEmpty){

      Dialogs.getInstance().showErrorAlert(context, "", "Please enter location name for story",true);
      return false;
      
    }
    else if(spotInfoCont.text.isEmpty){

      Dialogs.getInstance().showErrorAlert(context, "", "Please enter spot information for story",true);
      return false;
      
    }
    else if(spotInfoCont.text.length>1000){

      Dialogs.getInstance().showErrorAlert(context, "", "Please enter 1000 or less characters.",true);
      return false;
      
    }
    else if(metaDescCont.text.isEmpty){

      Dialogs.getInstance().showErrorAlert(context, "", "Please tell how to get there.",true);
      return false;
      
    }
    else if(metaDescCont.text.length>500){

      Dialogs.getInstance().showErrorAlert(context, "", "Please enter 500 or less characters.",true);
      return false;
      
    }
    else if(imageArray.isEmpty){
      Dialogs.getInstance().showErrorAlert(context, "", "Please select atleast one image for story",true);
      return false;
    }
    else if(selectTagArray.isEmpty){
      Dialogs.getInstance().showErrorAlert(context, "", "Please select atleast one tag for story",true);
      return false;
    }
    else{
      return true;
    }
  }

  String validateSpot(String value) {
    if (spotInfoCont.text.length>1000) {
    return "Please enter 1000 or less characters.";
    }
    return null;
  }

  String validateHowToGetThere(String value) {
    if (metaDescCont.text.length>500) {
    return "Please enter 500 or less characters.";
    }
    return null;
  }

  getLocationName(double storyLat, double storyLong) async{
    final coordinates = new Coordinates(storyLat, storyLong);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;

    
     var placeName = "${first.featureName} - ${first.subAdminArea == null? "":first.subAdminArea} - ${first.countryName}";

    selectLocationCont.text = placeName;

  }

  

  navigateToMap(BuildContext context,Position position)async{
    var result = await Navigator.of(context).push(CupertinoPageRoute(builder: (context)=>SearchMap(lat: position.latitude,long: position.longitude,)));

    print(result);

    lat = result["latitude"];
    long = result["longitude"];

     

    _kLake = CameraPosition(
      target: LatLng(lat, long),
      zoom: 14.4746);

      getLocationName(double.parse(lat.toString()), double.parse(long.toString()));
    

  }

  Future<void> _goToTheLocation() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }

  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: homeScaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text(
          "Your Story",
          style: GoogleFonts.roboto(color: Colors.black),
        ),
        leading: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.black,
            )),
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Center(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 8, 10, 5),
                  child: ListTile(
                    // trailing:Icon(Icons.add_circle,size:30,color: Colors.green[800],),
                    // RaisedButton(
                    //   shape: RoundedRectangleBorder(
                    //     borderRadius: BorderRadius.circular(10)
                    //   ),
                    //   child: Text("Add",style: GoogleFonts.roboto(color:Colors.white),),
                    //   color: Colors.green[800],
                    //   onPressed: (){}),
                    title: Text(
                      "Story Title",
                      style: GoogleFonts.roboto(
                        fontSize:16,
                          color: Colors.black, fontWeight: FontWeight.w500),
                    ),
                    subtitle: TextField(
                    cursorColor: Colors.black,
                    controller: titleCont,
                    style: GoogleFonts.roboto(
                            fontSize:16
                          ),
                      decoration: InputDecoration(
                      hintText: "ex. The trek of ladhak",
                      ),
                    ),
                  ),
                ),
                /*Padding(
                 padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: ListTile(
                  //  trailing:Icon(Icons.add_circle,size:30,color: Colors.green[800],),
                    title: Text(
                      "Add Address Line 1",
                      style: GoogleFonts.roboto(
                          color: Colors.black, fontWeight: FontWeight.w500),
                    ),
                    subtitle: TextField(
                      controller: addressCont,
                      decoration: InputDecoration(
                        hintText: "Area Name or Spot Name"
                      ),
                    ),
                  ),
                ),
               Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                 child: ListTile(
                  //  trailing:Icon(Icons.add_circle,size:30,color: Colors.green[800],),
                    title: Text(
                      "Add Address Line 2",
                      style: GoogleFonts.roboto(
                          color: Colors.black, fontWeight: FontWeight.w500),
                    ),
                    subtitle: TextField(
                      controller: addressCont2,
                      decoration: InputDecoration(
                        hintText: "Area Name or Spot Name"
                      ),
                    ),
                  ),
               ),*/
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                 child: ListTile(
                  //  trailing:Icon(Icons.add_circle,size:30,color: Colors.green[800],),
                    title: Text(
                      "Enter Location",
                      style: GoogleFonts.roboto(
                          color: Colors.black, fontWeight: FontWeight.w500,fontSize:16),
                    ),
                    subtitle: TextField(
                      controller: locationNameCont,
                      style: GoogleFonts.roboto(
                            fontSize:16
                          ),
                      decoration: InputDecoration(
                        hintText: "ex. Landmark etc."
                      ),
                    ),
                  ),
               ),
               /*Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                 child: ListTile(
                  //  trailing:Icon(Icons.add_circle,size:30,color: Colors.green[800],),
                    title: Text(
                      "Add postal code",
                      style: GoogleFonts.roboto(
                          color: Colors.black, fontWeight: FontWeight.w500,fontSize:16),
                    ),
                    subtitle: TextField(
                      controller: postalCodeCont,
                      style: GoogleFonts.roboto(
                            fontSize:16
                          ),
                      decoration: InputDecoration(
                        hintText: "Enter your area pincode"
                      ),
                    ),
                  ),
               ),*/
               
               
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Add Image/Video",
                  style: GoogleFonts.roboto(fontWeight: FontWeight.w500,fontSize: 16),
                ),
                SizedBox(
                  height: 5,
                ),
                VisibilityDetector(
      key: ObjectKey(flickMultiManager),
      onVisibilityChanged: (visibility) {
        if (visibility.visibleFraction == 0 && this.mounted) {
          flickMultiManager.pause();
        }
      },
                  child: new Container(
                    height: 140,
                    width: double.maxFinite,
                    child: new Padding(
                      padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                      child: new SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: new Row(
                          children:[
                            InkWell(
                              onTap: (){

                                Alert(
      context: context,
      title: "",
      desc: "Select Option",
      image: Icon(Icons.image,size:70),
      
      style: AlertStyle(
        titleStyle: TextStyle(fontSize: 2),
        isOverlayTapDismiss: false,
      ),
      // closeFunction: (){},
      // closeIcon: Icon(Icons.close,size:0,color:Colors.transparent),
      buttons: [
        DialogButton(
          child: Text(
            "Image",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: (){
            if(imageArray.length < 10){
                                              pickImages();
                                            }
                                            else{
                                              homeScaffoldKey.currentState.showSnackBar(
                                                SnackBar(content: Text("You can select images upto 10")),
                                              );
                                            }
                                            Navigator.of(context).pop();
          },
          color: AppDefault_Green,
          radius: BorderRadius.circular(0.0),
        ),

        DialogButton(
          child: Text(
            "Video",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: (){
           if(videoArray.length < 2){
                                              pickVideos();
                                            }
                                            else{
                                              homeScaffoldKey.currentState.showSnackBar(
                                                SnackBar(content: Text("You can select videos upto 2")),
                                              );
                                            }
                                            Navigator.of(context).pop();
          },
          color: AppDefault_Green,
          radius: BorderRadius.circular(0.0),
        ),
      ],
    ).show();
                               
                              },
                              child: Card(
                                elevation: 5,
                                child: Container(
                                  height: 100,
                                  width: 150,
                                  color: Colors.grey[300],
                                  child: Icon(
                                    Icons.add,
                                    size: 30,
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                            ),
                            new SizedBox(width: 15,),
                            new ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: imageArray.length,
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (context,imgIndex){
                                return new Row(
                                  children: [
                                    Card(
                                      elevation: 5,
                                      child: Stack(
                                        children: [
                                          Container(
                                            height: 100,
                                            width: 150,
                                            child: Image.file(imageArray[imgIndex],fit: BoxFit.cover,),
                                          ),
                                          new Positioned(
                                              right: 5,
                                              top: 5,
                                              child: InkWell(onTap: (){
                                                setState(() {
                                                  imageArray.removeAt(imgIndex);
                                                });
                                              },child: Container(height: 30,width: 30,child: CircleAvatar(backgroundColor: Colors.white,child: Center(child: Icon(Icons.delete,color:Colors.red,size:20)))))
                                            ),
                                          
                                        ],
                                      ),
                                    ),
                                    new SizedBox(width: 15,),
                                  ],
                                );
                              },
                            ),
                            new SizedBox(width: 15,),
                            new ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: videoArray.length,
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (context,videoIndex){
                                
                                return new Row(
                                  children: [
                                    Card(
                                      elevation: 5,
                                      child: Stack(
                                        children: [
                                          Container(
                                            height: 100,
                                            width: 150,
                                            child: FlickMultiPlayerFile(
                                              url: videoArray[videoIndex].path,
                                              flickMultiManager: flickMultiManager,
                                              image: "assets/PikNikTransparent.png",
                                            ),
                                          ),
                                          new Positioned(
                                              right: 5,
                                              top: 5,
                                              child: InkWell(onTap: (){
                                                setState(() {
                                                  videoArray.removeAt(videoIndex);
                                                });
                                              },child: Container(height: 30,width: 30,child: CircleAvatar(backgroundColor: Colors.white,child: Center(child: Icon(Icons.delete,color:Colors.red,size:20)))))
                                            ),

                                           
                                            
                                        ],
                                      ),
                                    ),
                                    new SizedBox(width: 15,),
                                  ],
                                );
                              },
                            )
                          ]
                        ),
                      ),
                    ),
                  ),
                ),
                
                SizedBox(
                  height: 20,
                ),
                // Text("Select Location",
                //     style: GoogleFonts.roboto(fontWeight: FontWeight.w500,fontSize: 16)),
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                  child: Container(width: double.maxFinite,child: Text("Select Location",textAlign: TextAlign.left,style: GoogleFonts.roboto(fontSize:16),)),),
                
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
                  child: InkWell(
                    onTap: ()async{
                      position = await getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
                      print(position.longitude);
                      navigateToMap(context,position);
                    },
                    child: AbsorbPointer(
                      child: TextField(
                        controller: selectLocationCont,
                        style: GoogleFonts.roboto(
                              fontSize:16
                            ),
                        minLines: 1,
                        maxLines: 4,
                        decoration: InputDecoration(
                          hintText: "Select Location",
                          suffixIcon: Icon(Icons.arrow_forward_ios)
                        ),
                      )
                    ),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: ListTile(
                    title: Text("Enter spot info",style: GoogleFonts.roboto(fontSize:16),),
               
                  //
               subtitle: 
                  Container(
                        width: double.maxFinite,
                        child: TextField(
                          minLines: 3,
                          maxLines: 10,
                          controller: spotInfoCont,
                          style: GoogleFonts.roboto(
                            fontSize:16,
                            fontWeight: FontWeight.bold
                          ),
                          onChanged: (text){
                            setState(() {
                              
                            });
                          },
                          decoration: InputDecoration(
                          hintText: "Enter spot information",
                          hintStyle: GoogleFonts.roboto(
                            fontSize:16
                          ),
                          errorText: validateSpot(spotInfoCont.text),
                          
                            counterText: "${spotInfoCont.text.length}/1000"),
                        )),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                  child: ListTile(
                    title: Text("How to get there?",style: GoogleFonts.roboto(fontSize:16),),
               
                  //
               subtitle: 
                  Container(
                        width: double.maxFinite,
                        child: TextField(
                          controller: metaDescCont,
                          minLines: 3,
                          maxLines: 10,
                          style: GoogleFonts.roboto(
                            fontSize:16,
                            fontWeight: FontWeight.bold
                          ),
                          onChanged: (text){
                            setState(() {
                              
                            });
                          },
                          decoration: InputDecoration(
                          hintText: "E.g. Train, Bus, Route Information",
                          hintStyle: GoogleFonts.roboto(
                            fontSize:16
                          ),
                          errorText: validateHowToGetThere(metaDescCont.text),
                            counterText: "${metaDescCont.text.length}/500"),
                        )),
                  ),
                ),
                InkWell(
                  onTap: (){
                    setState(() {
                      addTag = true;
                    });
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ImageIcon(AssetImage("assets/IconTag.png"),size: 20,color: AppDefault_Green,),
                      SizedBox(width:5),
                      Text("Choose Tags",
                          style: GoogleFonts.roboto(fontWeight: FontWeight.w500,fontSize:16)),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                 child: Row(
                   children: [
                     Expanded(
                       child: ListTile(
                         trailing:InkWell(onTap:(){
                           Dialogs.getInstance().showInfoAlert(context, "", "Type a tag and just hit space or enter to create a tag",false);
                         },child: Icon(Icons.help,size:30,color: AppDefault_Green,)),
                          title: Text(
                            "Add Tag",
                            style: GoogleFonts.roboto(
                                color: Colors.black, fontWeight: FontWeight.w500,fontSize:16),
                          ),
                          subtitle: TypeAheadFormField(
                            
                          textFieldConfiguration: TextFieldConfiguration(
                            textCapitalization: TextCapitalization.words,
                            controller: this.addTagCont,
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(RegExp(r"^\s+"),replacementString: "")
                            ],
                            onChanged: (t){
                              if((t as String).length>3){
                                if((t as String).contains(" ")){
                                  setState(() {
                                    selectTagArray.add(addTagCont.text);
                                  });
                                  addTagCont.clear();
                                  FocusScope.of(context).requestFocus(FocusNode());
                                }
                              }
                            },
                            decoration: InputDecoration(
                              labelText: 'Enter Tag Name'
                            )
                          ),          
                          suggestionsCallback: (pattern) {
                            return getSuggestions(pattern);
                          },
                          itemBuilder: (context, suggestion) {
                            if(addTagCont.text.length >=2){
                              return ListTile(
                                title: Text(suggestion),
                              );
                            }
                            else{
                              return new Container();
                            }

                          },
                          transitionBuilder: (context, suggestionsBox, controller) {
                            return suggestionsBox;
                          },
                          onSuggestionSelected: (suggestion) {
                            // this.addTagCont.text = suggestion;
                            setState(() {
                              if(!selectTagArray.contains(suggestion)){
                                selectTagArray.add(suggestion);
                              }
                            });
                            this.addTagCont.clear();
                            FocusScope.of(context).requestFocus(FocusNode());
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please select a tag';
                            }
                          },

                          hideOnEmpty: true,
                          hideSuggestionsOnKeyboardHide: true,
                          getImmediateSuggestions: false,
                          onSaved: (value){
                            print(value);
                          },
                        ),
                        ),
                     ),
                    //  new RaisedButton(child: new Text("Add",style:GoogleFonts.roboto(color:Colors.white)),color:AppDefault_Green, onPressed: (){
                    //    if(addTagCont.text.isNotEmpty){
                    //      setState(() {
                    //        selectTagArray.add(addTagCont.text);
                    //      });
                    //      addTagCont.clear();
                    //    }
                    //  })
                   ],
                 ),
               ),
                SizedBox(height: 10),
                Tags(  
                   
      itemCount: selectTagArray.length, 
      itemBuilder: (int index){ 
          return Tooltip(
          message: selectTagArray[index],
          child:ItemTags(
            active: false,
            pressEnabled: false,
            padding: EdgeInsets.all(13),
            index: index,
            color: Colors.white,
            activeColor: Colors.blueGrey,
            removeButton: ItemTagsRemoveButton(
              onRemoved: (){
                setState(() {
                            // required
                            selectTagArray.removeAt(index);
                        });
                return true;
              }
            ),
            title:selectTagArray[index],
            onPressed: (item){
              if(selectTagArray.isNotEmpty){
                if(item.active){
                  selectTagArray.add(item.title);
                }
                else{
                  List.from(selectTagArray).forEach((element) {
                      if(element.contains(item.title)){
                        var index = selectTagArray.indexOf(element);
                        selectTagArray.removeAt(index);
                      }
                    });
                }
              }
              else{
                selectTagArray.add(item.title);
              }

              print(selectTagArray);
            },
          )
          );
      },
    ),
                /*Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    SizedBox(
                      width: 10,
                    ),
                    RaisedButton(
                      color: Colors.grey[200],
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      onPressed: () {},
                      child: Text(
                        "Cultural",
                        style: GoogleFonts.roboto(),
                      ),
                    ),
                    RaisedButton(
                      color: Colors.grey[200],
                      elevation: 0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      onPressed: () {},
                      child: Text("Mountains", style: GoogleFonts.roboto()),
                    ),
                    RaisedButton(
                        color: Colors.blue[800],
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        onPressed: () {},
                        child: Text("Snowy",
                            style: GoogleFonts.roboto(color: Colors.white))),
                    SizedBox(
                      width: 10,
                    ),
                  ],
                ),*/
                SizedBox(
                  height: 25,
                ),
                Container(
                  width: 250,
                  height: 35,
                  child: RaisedButton(
                    color:  AppDefault_Green,
                    onPressed: () {
                      validation(context).then((value){
                        if(value){
                          apiPostStory(context);
                        }
                      });
                      
                    },
                    child: Text(
                      "Submit",
                      style: GoogleFonts.roboto(color: Colors.white,fontSize:16),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  getSuggestions(String query) {
    List matches = List();
    matches.addAll(tagsNames);

    matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class StoryMediaModel{
  String StoryMediaId;
  String StoryId;
  var StoryMediaPath;
  String FilePath;
  String FileTypeId;
  String IsPrimaryMedia;

  StoryMediaModel({this.StoryMediaId,this.StoryId,this.StoryMediaPath,this.FilePath,this.FileTypeId,this.IsPrimaryMedia});
}


