import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StoryDetailScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return StoryDetailScreenState();
  }
}

class StoryDetailScreenState extends State<StoryDetailScreen>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text("Story Detail",style: GoogleFonts.roboto(color: Colors.black),),
        actions: <Widget>[
        
        ],
        leading: InkWell(
                  onTap: (){
                   Navigator.of(context).pop();
                  },
                  child: Icon(Icons.arrow_back,color: Colors.black,),
        ),
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Center(
          child: SingleChildScrollView(
            // physics: NeverScrollableScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                 Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        height: 35,
                                        width: 35,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                              width: 1.0,
                                              color: Colors.black,
                                            ),
                                            image: DecorationImage(
                                                image: AssetImage("assets/lonvala.jpg"),
                                                fit: BoxFit.fill)),
                                      ),

                                      Padding(
                                    padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                    child: Text(
                                      "Bruce Wayne",
                                      style: GoogleFonts.roboto(fontWeight: FontWeight.w600)
                                    ),
                                  ),
                                    ],
                                  ),
                                  SizedBox(
                                    width:80,
                                  ),
                                  
                                   Container(
                                     width: 80,
                                     height: 30,
                                     decoration: BoxDecoration(
                                       border: Border.all(
                                         width: 1,
                                         color: Colors.indigo
                                       ),
                                       borderRadius: BorderRadius.circular(50)
                                     ),
                                     child: Padding(
                                       padding: const EdgeInsets.all(5.0),
                                       child: Center(child:Text("Following", style: GoogleFonts.roboto(fontWeight: FontWeight.w500,color: Colors.indigo))),
                                     ))
                                ],
                              ),
                            ),
                            ListTile(
                              title:  Padding(
                                padding: const EdgeInsets.fromLTRB(5, 0, 8, 0),
                                child: Text("Santa Catalina Islands",style:GoogleFonts.roboto(fontWeight: FontWeight.w500)),
                              ),
                              subtitle:Row(
                                children: <Widget>[
                                 
                          
                          Icon(Icons.location_on,size: 20,color: Colors.grey,),
                                  Text("Santa Catalina Islands",style:GoogleFonts.roboto(fontSize:12,fontWeight: FontWeight.w500),
                                  ),
                        Padding(
                           padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                           child: Text("12.5km",style:GoogleFonts.roboto(color: Colors.grey,fontSize: 12,fontWeight: FontWeight.bold),),
                         ),
                                ],
                              ), 
                            ),
                            Stack(
                              children: <Widget>[
                                Container(
                                  height: 200,
                                  width: 800,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(image: AssetImage("assets/catalina.jpg"))
                                  ),
                                ),
                                
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(18, 80, 18, 40),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.indigo
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: Center(child: Icon(Icons.arrow_back_ios,color: Colors.white,)),
                                        )),
                                       Container(
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.indigo
                                        ),
                                        child: Padding(
                                          padding: const EdgeInsets.all(5.0),
                                          child: Center(child: Icon(Icons.arrow_forward_ios,color: Colors.white,)),
                                        )),
                                    ],
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height:5),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(25, 8, 20, 8),
                              child: Text("215 Views\r200 Likes"),
                            ),
                            SizedBox(height:10),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                              child: ListTile(
                                title:Text("Spot Information",style:GoogleFonts.roboto(fontWeight: FontWeight.w600)),
                                subtitle: TextField(
                                  maxLines: 3,
                                  decoration: InputDecoration(
                                    hintText: "Amazing place in California,USA",
                                  ),
                                ),
                                                          ),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                              child: ListTile(
                                title:Text("Spot Location",style:GoogleFonts.roboto(fontWeight: FontWeight.w600)),
                                subtitle: TextField(
                                  // maxLines: 3,
                                  decoration: InputDecoration(
                                    hintText: " California,USA",
                                  ),
                                ),
                              ),
                            ),
                            // Padding(
                            //   padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                            //   child: ListTile(
                            //     leading: 
                                
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width: 30,
                                        height: 30,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.indigo
                                      ),
                                      child: Center(child: Text("8",style: GoogleFonts.roboto(color: Colors.white),))),
                                      SizedBox(width:5),
                                       Text("Comments",style:GoogleFonts.roboto(fontWeight: FontWeight.w600))
                                    ],
                                  ),
                                ),
                                SizedBox(height:10),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(15, 8, 15, 8),
                                  child: Container(
                                    child: Row(
                                      children: <Widget>[
                                         Container(
                                          height: 35,
                                          width: 35,
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                width: 1.0,
                                                color: Colors.black,
                                              ),
                                              image: DecorationImage(
                                                  image: AssetImage("assets/profile.jpg"),
                                                  fit: BoxFit.fill)),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text("travel_hippie",style: GoogleFonts.roboto(fontSize:12,fontWeight: FontWeight.w600)),
                                              Text("Where is this place?",style: GoogleFonts.roboto(fontWeight: FontWeight.w600)),
                                              Row(
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  Text("sunday August 31,10:22",style: GoogleFonts.roboto(fontSize: 11)),
                                                  SizedBox(width:100),
                                                    Text("Reply",style: GoogleFonts.roboto(fontSize:12,fontWeight: FontWeight.w600)),
                                                ],
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height:10),
                                 Padding(
                                  padding: const EdgeInsets.fromLTRB(15, 8, 15, 8),
                                  child: Container(
                                    child: Row(
                                      children: <Widget>[
                                         Container(
                                          height: 35,
                                          width: 35,
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(
                                                width: 1.0,
                                                color: Colors.black,
                                              ),
                                              image: DecorationImage(
                                                  image: AssetImage("assets/profile.jpg"),
                                                  fit: BoxFit.fill)),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(8, 8, 8, 0),
                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text("travel_hippie",style: GoogleFonts.roboto(fontSize:12,fontWeight: FontWeight.w600)),
                                              Text("Where is this place?",style: GoogleFonts.roboto(fontWeight: FontWeight.w600)),
                                              Row(
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  Text("sunday August 31,10:22",style: GoogleFonts.roboto(fontSize: 11)),
                                                  SizedBox(width:100),
                                                    Text("Reply",style: GoogleFonts.roboto(fontSize:12,fontWeight: FontWeight.w600)),
                                                ],
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                // Padding(
                                //   padding: const EdgeInsets.all(8.0),
                                //   child: ListTile(
                                //     leading: Container(
                                //       decoration: BoxDecoration(
                                //         image: DecorationImage(image: AssetImage("assets/profile.jpg"))
                                //       ),
                                //     ),
                                //     title: Text("travel_hippie"),
                                //   ),
                                // ),
                                SizedBox(height:50),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                width: double.maxFinite,
                                height: 50,
                                child: Card(
                                 
                                  
                                  elevation: 10,
                                  child:TextField(
                                    decoration: InputDecoration(
                                      
                                      hintText: "Add Comments",
                                    contentPadding: EdgeInsets.fromLTRB(12, 8, 15, 8),
                                      hintStyle: GoogleFonts.roboto(fontSize:12,fontWeight: FontWeight.w600)
                                    ),
                                  )
                                ),
                                ),
                              )
                            

              ],
            ),
          ),
        ),
      ),
    );
  }
}