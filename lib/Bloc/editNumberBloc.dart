import 'dart:async';

class EditNumberBloc{

  StreamController EditNumberBlocController = StreamController.broadcast();
  Stream get EditNumberBlocState => EditNumberBlocController.stream;
  Function get changeEditNumberBlocState => EditNumberBlocController.sink.add;

  
  
  dispose(){
    EditNumberBlocController.close();
  }
}

final editNumberBloc = EditNumberBloc();