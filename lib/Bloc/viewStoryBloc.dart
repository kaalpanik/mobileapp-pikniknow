import 'dart:async';

class ViewStoryBloc{

  StreamController ViewStoryBlocController = StreamController.broadcast();
  Stream get ViewStoryState => ViewStoryBlocController.stream;
  Function get changeViewStoryBlocState => ViewStoryBlocController.sink.add;

  
  
  dispose(){
    ViewStoryBlocController.close();
  }
}

final viewStoryBloc = ViewStoryBloc();


class FollowBloc{

  StreamController FollowBlocController = StreamController.broadcast();
  Stream get FollowBlocState => FollowBlocController.stream;
  Function get changeFollowBlocState => FollowBlocController.sink.add;

  
  
  dispose(){
    FollowBlocController.close();
  }
}

final followBloc = FollowBloc();