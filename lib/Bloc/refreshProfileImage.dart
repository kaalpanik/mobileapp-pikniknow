import 'dart:async';

class RefreshProfileBloc{

  StreamController refreshProfileBlocController = StreamController.broadcast();
  Stream get refreshProfileState => refreshProfileBlocController.stream;
  Function get changeRefreshProfileState => refreshProfileBlocController.sink.add;

  
  
  dispose(){
    refreshProfileBlocController.close();
  }
}

final refreshProfileBloc = RefreshProfileBloc();